# Defense systems wiki

## Introduction

Bacteriophages, or phages for short, are viruses that infect bacteria and hijack bacterial cellular machinery to reproduce themselves. Phages are extremely abundant entities and could be responsible for up to 20-40% of bacterial mortality daily (Hampton et al., 2020). Therefore, phage infection constitutes a very strong evolutionary pressure for bacteria.

In response to this evolutionary pressure, bacteria have developed an arsenal of anti-phage defense systems. The term "defense system" here designates either a single gene or a set of genes, which expression provides the bacteria with some level of resistance against phage infection.

## Defense system wiki objectives

The objective of this wiki is to gather synthetic information on all the different known [defense systems](/All_defense_systems/Liste_defense_systems.md) and [key concepts](/General_concepts/) of the anti-phage defense systems field.

The objective is to describe all defense systems in regard to different aspects:

- A short description.

- The molecular mechanism if it has been elucidated.

- Example of genomic architectures of the system or the different subsystems.

- Distribution of the systems in different prokaryotic phyla, and the distribution of the different subsystems.

- Experimental validation (source organism, host organism, and phages against which the system is active).

- A list of relevant abstracts.

- References.


## History of defense systems

The first anti-phage defense system was discovered in the early 1950s by two separate teams of researchers (Luria and Human, 1952; Bertani and  Wiegle 1952). Their work was in fact the first report of what would later be named Restriction-Modification ([RM](/All_defense_systems/RM/RM.md)) system, which is considered to be the first anti-phage defense system discovered.

The sighting of a second defense system occurred more than 40 years later, in the late 1980s when several teams around the world observed arrays containing short, palindromic DNA repeats clustered together on the bacterial genome (Barrangou et al., 2017). Yet, the biological function of these repeats was only elucidated in 2007, when a team of researchers demonstrated that these repeats were part of a new anti-phage defense system (Barrangou et al., 2007), known as [CRISPR-Cas system](https://en.wikipedia.org/wiki/CRISPR). 

Following these two major breakthroughs, knowledge of anti-phage systems remained scarce for some years. Yet, in 2011, Makarova and colleagues revealed that anti-phage systems tend to colocalize on the bacterial genome in [defense islands](/General_concepts/Defense_islands/Defense_islands.md). This led to a guilt-by-association hypothesis: if a gene or a set of genes is frequently found in bacterial genomes in close proximity to known defense systems, such as RM or CRISPR-Cas systems, then it might constitute a new defense system. This concept had a large role in the discovery of an impressive diversity of defense systems in a very short amount of time. To date, more than 130 defense systems have been described.

## List of known defense systems

To date, more than 130 anti-phage defense systems have been described. An exhaustive list of the systems with experimentally validated anti-phage activity can be found [here](/All_defense_systems/Liste_defense_systems.md).

## DefenseFinder

[DefenseFinder](http://defensefinder.mdmlab.fr/) is a detection tool to systematically detect all known defense systems in prokaryotic genomes (Tesson et al, 2022).

This tool is available as a [webservice](http://defensefinder.mdmlab.fr/) or as a [standalone version](https://github.com/mdmparis/defense-finder) on linux 


## Molecular mechanisms

This section is empty. You can help by adding to it.

## Application

This section is empty. You can help by adding to it.


## References 

Barrangou, R. et al. CRISPR provides acquired resistance against viruses in
prokaryotes. Science 315, 1709–1712 (2007)

Barrangou R, Horvath P. A decade of discovery: CRISPR functions and applications. Nat Microbiol. 2017 Jun 5;2:17092. doi: 10.1038/nmicrobiol.2017.92. PMID: 28581505

BERTANI, G, and J J WEIGLE. “Host controlled variation in bacterial viruses.” Journal of bacteriology vol. 65,2 (1953): 113-21. doi:10.1128/jb.65.2.113-121.1953

Doron S, Melamed S, Ofir G, Leavitt A, Lopatina A, Keren M, Amitai G, Sorek R. Systematic discovery of antiphage defense systems in the microbial pangenome. Science. 2018 Mar 2;359(6379):eaar4120. doi: 10.1126/science.aar4120. Epub 2018 Jan 25. PMID: 29371424; PMCID: PMC6387622.

Gao L, Altae-Tran H, Böhning F, et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science. 2020;369(6507):1077-1084. doi:10.1126/science.aba0372

Hampton, H. G., Watson, B. N. J. & Fineran, P. C. The arms race between bacteria and their phage foes. Nature 577, 327–336 (2020)
Tesson, F., Hervé, A., Mordret, E., Touchon, M., d’Humières, C., Cury, J., Bernheim, A., 2022. Systematic and quantitative view of the antiviral arsenal of prokaryotes. Nat Commun 13, 2561. https://doi.org/10.1038/s41467-022-30269-9

LURIA SE, HUMAN ML. A nonhereditary, host-induced variation of bacterial viruses. J Bacteriol. 1952;64(4):557-569. doi:10.1128/jb.64.4.557-569.1952

Makarova KS, Wolf YI, Snir S, Koonin EV. Defense islands in bacterial and archaeal genomes and prediction of novel defense systems. J Bacteriol. 2011 Nov;193(21):6039-56. doi: 10.1128/JB.05535-11. Epub 2011 Sep 9. PMID: 21908672; PMCID: PMC3194920.

Tal N, Sorek R. SnapShot: Bacterial immunity. Cell. 2022 Feb 3;185(3):578-578.e1. doi: 10.1016/j.cell.2021.12.029. PMID: 35120666.

Tesson, F., Hervé, A., Mordret, E., Touchon, M., d’Humières, C., Cury, J., Bernheim, A., 2022. Systematic and quantitative view of the antiviral arsenal of prokaryotes. Nat Commun 13, 2561. https://doi.org/10.1038/s41467-022-30269-9

## Contact

If you want to help to create new pages or have ideas on how to improve the wiki, please contact us at: defensefinder@mdmlab.fr

## To contribute

Log on with an external account if your not from pasteur, and send us (mailto:defensefinder@mdmlab.fr) your handle.
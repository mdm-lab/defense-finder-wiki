# Rst_Hydrolase-3Tm

## Example of genomic structure

The Rst_Hydrolase-3Tm system is composed of 2 proteins: Hydrolase and, Hydrolase-Tm.

Here is an example found in the RefSeq database: 

<img src="./data/Rst_Hydrolase-Tm.svg">

Rst\_Hydrolase-Tm subsystem in the genome of *Escherichia coli* (GCF\_004792495.1) is composed of 2 proteins: Hydrolase-Tm (WP\_000998640.1)and, Hydrolase (WP\_000754434.1).

## Distribution of the system among prokaryotes

The Rst_Hydrolase-3Tm system is present in a total of 34 different species.

Among the 22k complete genomes of RefSeq, this system is present in 42 genomes (0.2 %).

<img src="./data/Distribution_Rst_Hydrolase-3Tm.svg" width=800px>

*Proportion of genome encoding the Rst_Hydrolase-3Tm system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Rst_Hydrolase-3Tm systems were experimentally validated using:

A system from *Escherichia coli (P4 loci)* in *Escherichia coli* has an anti-phage effect against T7 (Rousset et al., 2022)

## Relevant abstracts


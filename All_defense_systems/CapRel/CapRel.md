# CapRel

## Description

CapRel is a fused toxin–antitoxin system that is active against diverse phages when expressed in *Escherichia coli*. CapRel belongs to the family of toxSAS toxin–antitoxin systems. CapRel is an Abortive infection system which is found in Cyanobacteria, Actinobacteria, and Proteobacteria, Spirochetes, Bacteroidetes, and Firmicutes, as well as in some temperate phages.

## Molecular mechanism

The CapRel system of Salmonella temperate phage SJ46 is normally found in a closed conformation, which is thought to maintain CapRel in an auto-inhibited state. However during phage SECPhi27 infection, binding of the major phage capsid protein (Gp57) to CapRel releases it from is inhibited state, allowing pyrophosphorylation of tRNAs by the toxin domain and resulting in translation inhibition. Other phage capsid proteins can be recognized by CapRel, as observed during infection by phage Bas8.


Different CapRel homologues confer defense against different phages, suggesting variable phage specificity of CapRel system which seems to be mediated by the C-terminal region of CapRel. 


## Example of genomic structure

The CapRel system is composed of one protein: CapRel.

Here is an example found in the RefSeq database: 

<img src="./data/CapRel.svg">

CapRel system in the genome of *Escherichia coli* (GCF\_003856995.1) is composed of 1 protein: CapRel (WP\_000526244.1).

## Distribution of the system among prokaryotes

The CapRel system is present in a total of 202 different species.

Among the 22k complete genomes of RefSeq, this system is present in 407 genomes (1.8 %).

<img src="./data/Distribution_CapRel.svg" width=800px>

*Proportion of genome encoding the CapRel system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

CapRel systems were experimentally validated using:

A system from *Salmonella phage SJ46* in *Escherichia coli* has an anti-phage effect against T2, T4, T6, RB69, SECphi27 (Zhang et al., 2022)

A system from *Enterobacter chengduensis* in *Escherichia coli* has an anti-phage effect against T7 (Zhang et al., 2022)

A system from *Klebsiella pneumoniae* in *Escherichia coli* has an anti-phage effect against SECphi18 (Zhang et al., 2022)

## Relevant abstracts

**Zhang, T. et al. Direct activation of a bacterial innate immune system by a viral capsid protein. Nature 612, 132-140 (2022).**
Bacteria have evolved diverse immunity mechanisms to protect themselves against the constant onslaught of bacteriophages1-3. Similar to how eukaryotic innate immune systems sense foreign invaders through pathogen-associated molecular patterns4 (PAMPs), many bacterial immune systems that respond to bacteriophage infection require phage-specific triggers to be activated. However, the identities of such triggers and the sensing mechanisms remain largely unknown. Here we identify and investigate the anti-phage function of CapRelSJ46, a fused toxin-antitoxin system that protects Escherichia coli against diverse phages. Using genetic, biochemical and structural analyses, we demonstrate that the C-terminal domain of CapRelSJ46 regulates the toxic N-terminal region, serving as both antitoxin and phage infection sensor. Following infection by certain phages, newly synthesized major capsid protein binds directly to the C-terminal domain of CapRelSJ46 to relieve autoinhibition, enabling the toxin domain to pyrophosphorylate tRNAs, which blocks translation to restrict viral infection. Collectively, our results reveal the molecular mechanism by which a bacterial immune system directly senses a conserved, essential component of phages, suggesting a PAMP-like sensing model for toxin-antitoxin-mediated innate immunity in bacteria. We provide evidence that CapRels and their phage-encoded triggers are engaged in a Red Queen conflict5, revealing a new front in the intense coevolutionary battle between phages and bacteria. Given that capsid proteins of some eukaryotic viruses are known to stimulate innate immune signalling in mammalian hosts6-10, our results reveal a deeply conserved facet of immunity.

## References
Zhang T, Tamman H, Coppieters 't Wallant K, Kurata T, LeRoux M, Srikant S, Brodiazhenko T, Cepauskas A, Talavera A, Martens C, Atkinson GC, Hauryliuk V, Garcia-Pino A, Laub MT. Direct activation of a bacterial innate immune system by a viral capsid protein. Nature. 2022 Dec;612(7938):132-140. doi: 10.1038/s41586-022-05444-z. Epub 2022 Nov 16. PMID: 36385533.
# Dsr

## Example of genomic structure

The Dsr system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Dsr_I.svg">

Dsr\_I subsystem in the genome of *Escherichia coli* (GCF\_016904235.1) is composed of 1 protein: Dsr1 (WP\_204608492.1).

<img src="./data/Dsr_II.svg">

Dsr\_II subsystem in the genome of *Escherichia coli* (GCF\_009950125.1) is composed of 1 protein: Dsr2 (WP\_178103017.1).

## Distribution of the system among prokaryotes

The Dsr system is present in a total of 246 different species.

Among the 22k complete genomes of RefSeq, this system is present in 641 genomes (2.8 %).

<img src="./data/Distribution_Dsr.svg" width=800px>

*Proportion of genome encoding the Dsr system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Dsr systems were experimentally validated using:

Subsystem DSR1 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T3, T7, PhiV-1 (Gao et al., 2020)

Subsystem DSR2 with a system from *Cronobacter sakazakii* in *Escherichia coli*  has an anti-phage effect against  Lambda (Gao et al., 2020)

Subsystem DSR2 with a system from *Bacillus subtilis* in *Bacillus subtilis *  has an anti-phage effect against  SPR (Garb et al., 2022)

Subsystem DSR1 with a system from *Bacillus subtilis* in *Bacillus subtilis *  has an anti-phage effect against  phi29 (Garb et al., 2022)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.

**Garb, J. et al. Multiple phage resistance systems inhibit infection via SIR2-dependent NAD+ depletion. Nat Microbiol 7, 1849-1856 (2022).**
Defence-associated sirtuins (DSRs) comprise a family of proteins that defend bacteria from phage infection via an unknown mechanism. These proteins are common in bacteria and harbour an N-terminal sirtuin (SIR2) domain. In this study we report that DSR proteins degrade nicotinamide adenine dinucleotide (NAD+) during infection, depleting the cell of this essential molecule and aborting phage propagation. Our data show that one of these proteins, DSR2, directly identifies phage tail tube proteins and then becomes an active NADase in Bacillus subtilis. Using a phage mating methodology that promotes genetic exchange between pairs of DSR2-sensitive and DSR2-resistant phages, we further show that some phages express anti-DSR2 proteins that bind and repress DSR2. Finally, we demonstrate that the SIR2 domain serves as an effector NADase in a diverse set of phage defence systems outside the DSR family. Our results establish the general role of SIR2 domains in bacterial immunity against phages.


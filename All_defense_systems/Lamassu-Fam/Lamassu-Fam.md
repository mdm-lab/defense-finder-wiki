# Lamassu-Fam

## Description

The original types of Lamassu systems are Lamassu Type 1 and 2. They both necessarily comprise two genes *lmuA* and *lmuB*, to which a third gene (*lmuC*) is added in the case of Lamassu Type 2.  

More recently, Lamassu has been suggested to be a large family of defense systems, that can be classified into multiple subtypes. 

These systems all encode the *lmuB* gene, and in most cases also comprise *lmuC.* In addition to these two core genes, Lamassu systems of various subtypes encode a third protein, hypothesized to be the Abi effector protein (3). This effector  can not only be LmuA (Lamassu Type1 and 2) but also proteins encoding endonuclease domains, SIR2-domains, or even hydrolase domains (3). Systems of the extended Lamassu-family can be found in 10% of prokaryotic genomes (3).

## Molecular mechanism

Lamassu systems function through abortive infection (Abi), but their molecular mechanism remains to be described.

## Example of genomic structure

The majority of the Lamassu-Fam systems are composed of 3 proteins: LmuA, LmuB and, an accessory LmuC proteins.

Here is an example of a Lamassu-Fam\_Cap4\_nuclease found in the RefSeq database: 

<img src="./data/Lamassu-Fam_Cap4_nuclease.svg">

Lamassu-Fam\_Cap4\_nuclease subsystem in the genome of *Pseudomonas sp.* (GCF\_016925675.1) is composed of 3 proteins: LmuB\_SMC\_Hydrolase\_protease (WP\_205519025.1), LmuC\_acc\_Cap4\_nuclease (WP\_205478326.1)and, LmuA\_effector\_Cap4\_nuclease\_II (WP\_205478325.1).

<img src="./data/Lamassu-Fam_Mrr.svg">

Lamassu-Fam\_Mrr subsystem in the genome of *Escherichia coli* (GCF\_011404895.1) is composed of 2 proteins: LmuA\_effector\_Mrr (WP\_044864610.1)and, LmuB\_SMC\_Cap4\_nuclease\_II (WP\_226199836.1).

<img src="./data/Lamassu-Fam_Hydrolase.svg">

Lamassu-Fam\_Hydrolase subsystem in the genome of *Caldisphaera lagunensis* (GCF\_000317795.1) is composed of 2 proteins: LmuA\_effector\_Hydrolase (WP\_015232255.1)and, LmuB\_SMC\_Hydrolase\_protease (WP\_015232260.1).

<img src="./data/Lamassu-Fam_Lipase.svg">

Lamassu-Fam\_Lipase subsystem in the genome of *Bradyrhizobium elkanii* (GCF\_012871055.1) is composed of 2 proteins: LmuA\_effector\_Lipase (WP\_172647146.1)and, LmuB\_SMC\_Lipase (WP\_172647148.1).

<img src="./data/Lamassu-Fam_Hydrolase_protease.svg">

Lamassu-Fam\_Hydrolase\_protease subsystem in the genome of *Klebsiella pneumoniae* (GCF\_022453565.1) is composed of 3 proteins: LmuB\_SMC\_Cap4\_nuclease\_II (WP\_023301569.1), LmuA\_effector\_Protease (WP\_023301563.1)and, LmuA\_effector\_Hydrolase (WP\_023301562.1).

<img src="./data/Lamassu-Fam_Hypothetical.svg">

Lamassu-Fam\_Hypothetical subsystem in the genome of *Streptococcus constellatus* (GCF\_016127875.1) is composed of 2 proteins: LmuB\_SMC\_Cap4\_nuclease\_II (WP\_198458038.1)and, LmuA\_effector\_hypothetical (WP\_198458040.1).

<img src="./data/Lamassu-Fam_Protease.svg">

Lamassu-Fam\_Protease subsystem in the genome of *Azospirillum brasilense* (GCF\_022023855.1) is composed of 2 proteins: LmuA\_effector\_Protease (WP\_237905456.1)and, LmuB\_SMC\_Cap4\_nuclease\_II (WP\_237905457.1).

<img src="./data/Lamassu-Fam_PDDEXK.svg">

Lamassu-Fam\_PDDEXK subsystem in the genome of *Janthinobacterium sp.* (GCF\_000013625.1) is composed of 2 proteins: LmuA\_effector\_PDDEXK (WP\_012078862.1)and, LmuB\_SMC\_Cap4\_nuclease\_II (WP\_012078864.1).

<img src="./data/Lamassu-Fam_Sir2.svg">

Lamassu-Fam\_Sir2 subsystem in the genome of *Paenibacillus polymyxa* (GCF\_022492955.1) is composed of 4 proteins: LmuB\_SMC\_Cap4\_nuclease\_II (WP\_240753063.1), LmuB\_SMC\_Sir2 (WP\_240753064.1), LmuC\_acc\_Sir2 (WP\_240753066.1)and, LmuA\_effector\_Sir2 (WP\_240753072.1).


<img src="./data/Lamassu-Fam_FMO.svg">

Lamassu-Fam\_FMO subsystem in the genome of *Acinetobacter johnsonii* (GCF\_021496365.1) is composed of 2 proteins: LmuA\_effector\_FMO (WP\_234965678.1)and, LmuB\_SMC\_FMO (WP\_234965680.1).

<img src="./data/Lamassu-Fam_Amidase.svg">

Lamassu-Fam\_Amidase subsystem in the genome of *Bradyrhizobium arachidis* (GCF\_015291705.1) is composed of 2 proteins: LmuA\_effector\_Amidase (WP\_143130692.1)and, LmuB\_SMC\_Amidase (WP\_092217687.1).

## Distribution of the system among prokaryotes

The Lamassu-Fam system is present in a total of 1189 different species.

Among the 22k complete genomes of RefSeq, this system is present in 3939 genomes (17.3 %).

<img src="./data/Distribution_Lamassu-Fam.svg" width=800px>

*Proportion of genome encoding the Lamassu-Fam system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Lamassu-Fam systems were experimentally validated using:

A system from *Bacillus sp. NIO-1130* in *Bacillus subtilis* has an anti-phage effect against phi3T, SpBeta, SPR (Doron et al., 2018)

A system from *Bacillus cereus* in *Bacillus subtilis* has an anti-phage effect against SpBeta (Doron et al., 2018)

Subsystem LmuB+LmuC+Hydrolase+ Protease with a system from *Bacillus cereus* in *Escherichia coli*  has an anti-phage effect against  T4 (Millman et al., 2022)

Subsystem LmuB+LmuC+Hydrolase+ Protease with a system from *Bacillus cereus* in *Bacillus subtilis*  has an anti-phage effect against  SpBeta, phi105, Rho14, SPP1, phi29 (Millman et al., 2022)

Subsystem LmuB+LmuC+Mrr endonuclease with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  LambdaVir, SECphi27 (Millman et al., 2022)

Subsystem LmuB+LmuC+PDDEXK nuclease with a system from *Bacillus cereus* in *Escherichia coli*  has an anti-phage effect against  LambdaVir (Millman et al., 2022)

Subsystem LmuB+LmuC+PDDEXK nuclease with a system from *Bacillus sp. UNCCL81* in *Escherichia coli*  has an anti-phage effect against  LambdaVir (Millman et al., 2022)

Subsystem LmuA+LmuC+LmuB with a system from *Janthinobacterium agaricidamnosum* in *Escherichia coli*  has an anti-phage effect against  T1, T3, T7, LambdaVir, PVP-SE1 (Payne et al., 2021)

Subsystem DdmABC with a system from *Vibrio cholerae* in *Escherichia coli*  has an anti-phage effect against  P1, Lambda (Jaskólska et al., 2022)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.

**Jaskólska, M., Adams, D. W. & Blokesch, M. Two defence systems eliminate plasmids from seventh pandemic Vibrio cholerae. Nature 604, 323-329 (2022).**
Horizontal gene transfer can trigger rapid shifts in bacterial evolution. Driven by a variety of mobile genetic elementsin particular bacteriophages and plasmidsthe ability to share genes within and across species underpins the exceptional adaptability of bacteria. Nevertheless, invasive mobile genetic elements can also present grave risks to the host; bacteria have therefore evolved a vast array of defences against these elements1. Here we identify two plasmid defence systems conserved in the Vibrio cholerae El Tor strains responsible for the ongoing seventh cholera pandemic2-4. These systems, termed DdmABC and DdmDE, are encoded on two major pathogenicity islands that are a hallmark of current pandemic strains. We show that the modules cooperate to rapidly eliminate small multicopy plasmids by degradation. Moreover, the DdmABC system is widespread and can defend against bacteriophage infection by triggering cell suicide (abortive infection, or Abi). Notably, we go on to show that, through an Abi-like mechanism, DdmABC increases the burden of large low-copy-number conjugative plasmids, including a broad-host IncC multidrug resistance plasmid, which creates a fitness disadvantage that counterselects against plasmid-carrying cells. Our results answer the long-standing question of why plasmids, although abundant in environmental strains, are rare in pandemic strains; have implications for understanding the dissemination of antibiotic resistance plasmids; and provide insights into how the interplay between two defence systems has shaped the evolution of the most successful lineage of pandemic V. cholerae.

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.

## References

1\. Doron S, Melamed S, Ofir G, et al. Systematic discovery of antiphage defense systems in the microbial pangenome. *Science*. 2018;359(6379):eaar4120. doi:10.1126/science.aar4120

2\. Payne LJ, Todeschini TC, Wu Y, et al. Identification and classification of antiviral defence systems in bacteria and archaea with PADLOC reveals new system types. *Nucleic Acids Res*. 2021;49(19):10868-10878. doi:10.1093/nar/gkab883

3\. Millman, A., Melamed, S., Leavitt, A., Doron, S., Bernheim, A., Hör, J., Lopatina, A., Ofir, G., Hochhauser, D., Stokar-Avihail, A., Tal, N., Sharir, S., Voichek, M., Erez, Z., Ferrer, J.L.M., Dar, D., Kacen, A., Amitai, G., Sorek, R., 2022. An expanding arsenal of immune systems that protect bacteria from phages. bioRxiv. https://doi.org/10.1101/2022.05.11.491447

## References

1\. Doron S, Melamed S, Ofir G, et al. Systematic discovery of antiphage defense systems in the microbial pangenome. *Science*. 2018;359(6379):eaar4120. doi:10.1126/science.aar4120

2\. Payne LJ, Todeschini TC, Wu Y, et al. Identification and classification of antiviral defence systems in bacteria and archaea with PADLOC reveals new system types. *Nucleic Acids Res*. 2021;49(19):10868-10878. doi:10.1093/nar/gkab883

3\. Millman, A., Melamed, S., Leavitt, A., Doron, S., Bernheim, A., Hör, J., Lopatina, A., Ofir, G., Hochhauser, D., Stokar-Avihail, A., Tal, N., Sharir, S., Voichek, M., Erez, Z., Ferrer, J.L.M., Dar, D., Kacen, A., Amitai, G., Sorek, R., 2022. An expanding arsenal of immune systems that protect bacteria from phages. bioRxiv. https://doi.org/10.1101/2022.05.11.491447

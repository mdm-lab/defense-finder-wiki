# Dpd

## Example of genomic structure

The Dpd system is composed of 15 proteins: FolE, QueD, DpdC, DpdA, DpdB, QueC, DpdD, DpdK, DpdJ, DpdI, DpdH, DpdG, DpdF, DpdE and, QueE.

Here is an example found in the RefSeq database: 

<img src="./data/Dpd.svg">

Dpd system in the genome of *Thalassotalea crassostreae* (GCF\_001831495.1) is composed of 15 proteins: QueE (WP\_068546614.1), DpdE (WP\_068546526.1), DpdF (WP\_068546528.1), DpdG (WP\_068546530.1), DpdH (WP\_070795901.1), DpdI (WP\_068546533.1), DpdJ (WP\_068546534.1), DpdK (WP\_082897170.1), DpdD (WP\_068546535.1), QueC (WP\_068546536.1), DpdB (WP\_068546537.1), DpdA (WP\_068546538.1), DpdC (WP\_157726628.1), QueD (WP\_068546540.1)and, FolE (WP\_068546542.1).

## Distribution of the system among prokaryotes

The Dpd system is present in a total of 100 different species.

Among the 22k complete genomes of RefSeq, this system is present in 226 genomes (1.0 %).

<img src="./data/Distribution_Dpd.svg" width=800px>

*Proportion of genome encoding the Dpd system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Relevant abstracts

**Thiaville, J. J. et al. Novel genomic island modifies DNA with 7-deazaguanine derivatives. Proceedings of the National Academy of Sciences 113, E1452-E1459 (2016).**
The discovery of ?20-kb gene clusters containing a family of paralogs of tRNA guanosine transglycosylase genes, called tgtA5, alongside 7-cyano-7-deazaguanine (preQ0) synthesis and DNA metabolism genes, led to the hypothesis that 7-deazaguanine derivatives are inserted in DNA. This was established by detecting 2-deoxy-preQ0 and 2-deoxy-7-amido-7-deazaguanosine in enzymatic hydrolysates of DNA extracted from the pathogenic, Gram-negative bacteria Salmonella enterica serovar Montevideo. These modifications were absent in the closely related S. enterica serovar Typhimurium LT2 and from a mutant of S. Montevideo, each lacking the gene cluster. This led us to rename the genes of the S. Montevideo cluster as dpdA-K for 7-deazapurine in DNA. Similar gene clusters were analyzed in ?150 phylogenetically diverse bacteria, and the modifications were detected in DNA from other organisms containing these clusters, including Kineococcus radiotolerans, Comamonas testosteroni, and Sphingopyxis alaskensis. Comparative genomic analysis shows that, in Enterobacteriaceae, the cluster is a genomic island integrated at the leuX locus, and the phylogenetic analysis of the TgtA5 family is consistent with widespread horizontal gene transfer. Comparison of transformation efficiencies of modified or unmodified plasmids into isogenic S. Montevideo strains containing or lacking the cluster strongly suggests a restriction-modification role for the cluster in Enterobacteriaceae. Another preQ0 derivative, 2-deoxy-7-formamidino-7-deazaguanosine, was found in the Escherichia coli bacteriophage 9g, as predicted from the presence of homologs of genes involved in the synthesis of the archaeosine tRNA modification. These results illustrate a deep and unexpected evolutionary connection between DNA and tRNA metabolism.


# CBASS

## Example of genomic structure

The CBASS system have been describe in a total of 5 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/CBASS_I.svg">

CBASS\_I subsystem in the genome of *Rhizobium leguminosarum* (GCF\_002243365.1) is composed of 2 proteins: 4TM\_new (WP\_094230678.1)and, Cyclase\_SMODS (WP\_094230679.1).

<img src="./data/CBASS_II.svg">

CBASS\_II subsystem in the genome of *Parvularcula bermudensis* (GCF\_000152825.2) is composed of 3 proteins: 4TM\_new (WP\_013299178.1), Cyclase\_II (WP\_148235131.1)and, AG\_E2\_Prok-E2\_B (WP\_013299180.1).

<img src="./data/CBASS_III.svg">

CBASS\_III subsystem in the genome of *Methylocella tundrae* (GCF\_900749825.1) is composed of 5 proteins: Endonuc\_small (WP\_134490779.1), Cyclase\_SMODS (WP\_134490781.1), bacHORMA\_2 (WP\_134490783.1), HORMA (WP\_134490785.1)and, TRIP13 (WP\_134490787.1).

<img src="./data/CBASS_IV.svg">

CBASS\_IV subsystem in the genome of *Bacillus sp.* (GCF\_022809835.1) is composed of 4 proteins: 2TM\_type\_IV (WP\_243501124.1), QueC (WP\_206906219.1), TGT (WP\_243501126.1)and, Cyclase\_SMODS (WP\_243501127.1).

## Distribution of the system among prokaryotes

The CBASS system is present in a total of 1062 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2938 genomes (12.9 %).

<img src="./data/Distribution_CBASS.svg" width=800px>

*Proportion of genome encoding the CBASS system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

CBASS systems were experimentally validated using:

A system from *Vibrio cholerae* in *Escherichia coli* has an anti-phage effect against P1, T2 (Cohen et al., 2019)

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against P1, T2, T4, T5, T6, LambdaVir (Cohen et al., 2019)

A system from *Enterobacter cloacae* in *Escherichia coli* has an anti-phage effect against T2, T7 (Lowey et al., 2020)

A system from *Pseudomonas aeruginosa* in *Pseudomonas aeruginosa* has an anti-phage effect against PaMx41, PaMx33, PaMx35, PaMx43 (Huiting et al., 2022)

## Relevant abstracts

**Cohen, D. et al. Cyclic GMP-AMP signalling protects bacteria against viral infection. Nature 574, 691-695 (2019).**
The cyclic GMP-AMP synthase (cGAS)-STING pathway is a central component of the cell-autonomous innate immune system in animals1,2. The cGAS protein is a sensor of cytosolic viral DNA and, upon sensing DNA, it produces a cyclic GMP-AMP (cGAMP) signalling molecule that binds to the STING protein and activates the immune response3-5. The production of cGAMP has also been detected in bacteria6, and has been shown, in Vibrio cholerae, to activate a phospholipase that degrades the inner bacterial membrane7. However, the biological role of cGAMP signalling in bacteria remains unknown. Here we show that cGAMP signalling is part of an antiphage defence system that is common in bacteria. This system is composed of a four-gene operon that encodes the bacterial cGAS and the associated phospholipase, as well as two enzymes with the eukaryotic-like domains E1, E2 and JAB. We show that this operon confers resistance against a wide variety of phages. Phage infection triggers the production of cGAMP, which-in turn-activates the phospholipase, leading to a loss of membrane integrity and to cell death before completion of phage reproduction. Diverged versions of this system appear in more than 10% of prokaryotic genomes, and we show that variants with effectors other than phospholipase also protect against phage infection. Our results suggest that the eukaryotic cGAS-STING antiviral pathway has ancient evolutionary roots that stem from microbial defences against phages.

**Duncan-Lowey, B., McNamara-Bordewick, N. K., Tal, N., Sorek, R. & Kranzusch, P. J. Effector-mediated membrane disruption controls cell death in CBASS antiphage defense. Molecular Cell 81, 5039-5051.e5 (2021).**
Cyclic oligonucleotide-based antiphage signaling systems (CBASS) are antiviral defense operons that protect bacteria from phage replication. Here, we discover a widespread class of CBASS transmembrane (TM) effector proteins that respond to antiviral nucleotide signals and limit phage propagation through direct membrane disruption. Crystal structures of the Yersinia TM effector Cap15 reveal a compact 8-stranded ?-barrel scaffold that forms a cyclic dinucleotide receptor domain that oligomerizes upon activation. We demonstrate that activated Cap15 relocalizes throughout the cell and specifically induces rupture of the inner membrane. Screening for active effectors, we identify the function of distinct families of CBASS TM effectors and demonstrate that cell death via disruption of inner-membrane integrity is a common mechanism of defense. Our results reveal the function of the most prominent class of effector protein in CBASS immunity and define disruption of the inner membrane as a widespread strategy of abortive infection in bacterial phage defense.

**Millman, A., Melamed, S., Amitai, G. & Sorek, R. Diversity and classification of cyclic-oligonucleotide-based anti-phage signalling systems. Nat Microbiol 5, 1608-1615 (2020).**
Cyclic-oligonucleotide-based anti-phage signalling systems (CBASS) are a family of defence systems against bacteriophages (hereafter phages) that share ancestry with the cGAS-STING innate immune pathway in animals. CBASS systems are composed of an oligonucleotide cyclase, which generates signalling cyclic oligonucleotides in response to phage infection, and an effector that is activated by the cyclic oligonucleotides and promotes cell death. Cell death occurs before phage replication is completed, therefore preventing the spread of phages to nearby cells. Here, we analysed 38,000 bacterial and archaeal genomes and identified more than 5,000 CBASS systems, which have diverse architectures with multiple signalling molecules, effectors and ancillary genes. We propose a classification system for CBASS that groups systems according to their operon organization, signalling molecules and effector function. Four major CBASS types were identified, sharing at least six effector subtypes that promote cell death by membrane impairment, DNA degradation or other means. We observed evidence of extensive gain and loss of CBASS systems, as well as shuffling of effector genes between systems. We expect that our classification and nomenclature scheme will guide future research in the developing CBASS field.

**Morehouse, B. R. et al. STING cyclic dinucleotide sensing originated in bacteria. Nature 586, 429-433 (2020).**
Stimulator of interferon genes (STING) is a receptor in human cells that senses foreign cyclic dinucleotides that are released during bacterial infection and in endogenous cyclic GMP-AMP signalling during viral infection and anti-tumour immunity1-5. STING shares no structural homology with other known signalling proteins6-9, which has limited attempts at functional analysis and prevented explanation of the origin of cyclic dinucleotide signalling in mammalian innate immunity. Here we reveal functional STING homologues encoded within prokaryotic defence islands, as well as a conserved mechanism of signal activation. Crystal structures of bacterial STING define a minimal homodimeric scaffold that selectively responds to cyclic di-GMP synthesized by a neighbouring cGAS/DncV-like nucleotidyltransferase (CD-NTase) enzyme. Bacterial STING domains couple the recognition of cyclic dinucleotides with the formation of protein filaments to drive oligomerization of TIR effector domains and rapid NAD+ cleavage. We reconstruct the evolutionary events that followed the acquisition of STING into metazoan innate immunity, and determine the structure of a full-length TIR-STING fusion from the Pacific oyster Crassostrea gigas. Comparative structural analysis demonstrates how metazoan-specific additions to the core STING scaffold enabled a switch from direct effector function to regulation of antiviral transcription. Together, our results explain the mechanism of STING-dependent signalling and reveal the conservation of a functional cGAS-STING pathway in prokaryotic defence against bacteriophages.

**Ye, Q. et al. HORMA Domain Proteins and a Trip13-like ATPase Regulate Bacterial cGAS-like Enzymes to Mediate Bacteriophage Immunity. Mol Cell 77, 709-722.e7 (2020).**
Bacteria are continually challenged by foreign invaders, including bacteriophages, and have evolved a variety of defenses against these invaders. Here, we describe the structural and biochemical mechanisms of a bacteriophage immunity pathway found in a broad array of bacteria, including E. coli and Pseudomonas aeruginosa. This pathway uses eukaryotic-like HORMA domain proteins that recognize specific peptides, then bind and activate a cGAS/DncV-like nucleotidyltransferase (CD-NTase) to generate a cyclic triadenylate (cAAA) second messenger; cAAA in turn activates an endonuclease effector, NucC. Signaling is attenuated by a homolog of the AAA+ ATPase Pch2/TRIP13, which binds and disassembles the active HORMA-CD-NTase complex. When expressed in non-pathogenic E. coli, this pathway confers immunity against bacteriophage ? through an abortive infection mechanism. Our findings reveal the molecular mechanisms of a bacterial defense pathway integrating a cGAS-like nucleotidyltransferase with HORMA domain proteins for threat sensing through protein detection and negative regulation by a Trip13 ATPase.


# Druantia

## Example of genomic structure

The Druantia system have been describe in a total of 3 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Druantia_I.svg">

Druantia\_I subsystem in the genome of *Escherichia coli* (GCF\_002220215.1) is composed of 5 proteins: DruA (WP\_000549798.1), DruB (WP\_001315973.1), DruC (WP\_021520530.1), DruD (WP\_000455180.1)and, DruE\_1 (WP\_089180326.1).

<img src="./data/Druantia_II.svg">

Druantia\_II subsystem in the genome of *Collimonas pratensis* (GCF\_001584185.1) is composed of 4 proteins: DruM (WP\_082793204.1), DruE\_2 (WP\_061945149.1), DruG (WP\_061945151.1)and, DruF (WP\_150119800.1).

<img src="./data/Druantia_III.svg">

Druantia\_III subsystem in the genome of *Acinetobacter baumannii* (GCF\_012935125.1) is composed of 2 proteins: DruH (WP\_005120035.1)and, DruE\_3 (WP\_002036795.1).

## Distribution of the system among prokaryotes

The Druantia system is present in a total of 284 different species.

Among the 22k complete genomes of RefSeq, this system is present in 827 genomes (3.6 %).

<img src="./data/Distribution_Druantia.svg" width=800px>

*Proportion of genome encoding the Druantia system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Druantia systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T2, T4, T5, P1, Lambda, T3, T7, PhiV-1, Lambdavir, SECphi18, SECphi27 (Gao et al., 2020; Doron et al., 2018)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.


# DISARM

## Description

DISARM (Defense Island System Associated with Restriction-Modification) is a defense system widespread in prokaryotes, encoded by a 5-gene cassette. DISARM provides broad protection against double-stranded DNA phages, including siphophages, myophages, and podophages (1,3).

 It was reported to restrict incoming phage DNA and methylate the bacterial host DNA, which could be responsible for self from non-self discrimination (1). This suggests a [Restriction-Modification](/list_defense_systems/RM)\-like (RM-like) mechanism, yet some pieces of experimental evidence hint that DISARM actually acts through a novel and uncharacterized molecular mechanism (1,2).

## Molecular mechanism

DISARM allows phage adsorption but prevents phage replication. DISARM is thought to cause intracellular phage DNA decay (1), but the molecular of this potential DNA degradation remains unknown.

The *drmMII* gene of DISARM system from *Bacillus paralicheniformis* was shown to methylate bacterial DNA at CCWGG motifs when expressed in Bacillus subtilis, and in the absence of *drmMII,* this DISARM system appears toxic to the cells (1). These observations are consistent with an RM-like mechanism, where nucleic acid degradation targets specific DNA motifs, that are methylated in the bacterial chromosome to prevent auto-immunity. 

Yet this system was also shown to protect against phages whose genomes are exempt of CCWGG motifs (1). Moreover, a recent study reports that the absence of methylases (DrmMI or DrmMII) of the DISARM system from a *Serratia sp.* does not result in autoimmunity (3). Both these results suggest additional phage DNA recognition mechanisms. 

Hints of these additional mechanisms can be found in recent structural studies, which show that DrmA and DrmB form a complex that can bind single-stranded DNA (2). Moreover, the DrmAB complex seems to exhibit strong ATPase activity in presence of unmethylated DNA, and  reduced ATPase activity in the presence of a methylated DNA substrate (2). Finally, binding of unmethylated single-stranded DNA appears to mediate major conformational change of the complex, which was hypothesized to be responsible for downstream DISARM activation (2).


## Example of genomic structure
DISARM is encoded by three core genes: *drmA* (encoding for a protein containing a putative helicase domain)*,* *drmB* (encoding for a protein containing a putative helicase-associated domain), and *drmC* (encoding for a protein containing a phospholipase D/nuclease domain) (1)

These three core genes are accompanied by a methyltransferase, which can be either an adenine methylase (*drmMI*) for class 1 DISARM systems or a cytosine methylase (*drmMII*) for DISARM class 2. Both classes also encode an additional gene (*drmD* for class 1, and *drmE* for class 2). 

Here is some example found in the RefSeq database:

<img src="./data/DISARM_1.svg">

DISARM\_1 subsystem in the genome of *Pseudomonas aeruginosa* (GCF\_009676885.1) is composed of 6 proteins: drmD (WP\_023093122.1), drmMI (WP\_023115027.1), drmD (WP\_023093126.1), drmA (WP\_033993408.1), drmB (WP\_023093129.1)and, drmC (WP\_031637507.1).

<img src="./data/DISARM_2.svg">

DISARM\_2 subsystem in the genome of *Bacillus paralicheniformis* (GCF\_009497935.1) is composed of 5 proteins: drmMII (WP\_020450482.1), drmC (WP\_020450481.1), drmB (WP\_025810358.1), drmA (WP\_020450479.1)and, drmE (WP\_020450478.1).

## Distribution of the system among prokaryotes

The DISARM system is present in a total of 214 different species.

Among the 22k complete genomes of RefSeq, this system is present in 341 genomes (1.5 %).

<img src="./data/Distribution_DISARM.svg" width=800px>

*Proportion of genome encoding the DISARM system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

DISARM systems were experimentally validated using:

A system from *Bacillus paralicheniformis* in *Bacillus subtilis* has an anti-phage effect against SPO1, phi3T, SpBeta, SPR, phi105, rho14, SPP1, phi29 , Nf (Doron et al., 2018; Ofir et al., 2017)

A system from *Serratia sp. SCBI* in *Escherichia coli* has an anti-phage effect against T1, Nami, T7, M13 (Aparicio-Maldonado et al., 2021)

## Relevant abstracts

**Bravo, J. P. K., Aparicio-Maldonado, C., Nobrega, F. L., Brouns, S. J. J. & Taylor, D. W. Structural basis for broad anti-phage immunity by DISARM. Nat Commun 13, 2987 (2022).**
In the evolutionary arms race against phage, bacteria have assembled a diverse arsenal of antiviral immune strategies. While the recently discovered DISARM (Defense Island System Associated with Restriction-Modification) systems can provide protection against a wide range of phage, the molecular mechanisms that underpin broad antiviral targeting but avoiding autoimmunity remain enigmatic. Here, we report cryo-EM structures of the core DISARM complex, DrmAB, both alone and in complex with an unmethylated phage DNA mimetic. These structures reveal that DrmAB core complex is autoinhibited by a trigger loop (TL) within DrmA and binding to DNA substrates containing a 5? overhang dislodges the TL, initiating a long-range structural rearrangement for DrmAB activation. Together with structure-guided in vivo studies, our work provides insights into the mechanism of phage DNA recognition and specific activation of this widespread antiviral defense system.

**Ofir, G. et al. DISARM is a widespread bacterial defence system with broad anti-phage activities. Nat Microbiol 3, 90-98 (2018).**
The evolutionary pressure imposed by phage predation on bacteria and archaea has resulted in the development of effective anti-phage defence mechanisms, including restriction-modification and CRISPR-Cas systems. Here, we report on a new defence system, DISARM (defence island system associated with restriction-modification), which is widespread in bacteria and archaea. DISARM is composed of five genes, including a DNA methylase and four other genes annotated as a helicase domain, a phospholipase D (PLD) domain, a DUF1998 domain and a gene of unknown function. Engineering the Bacillus paralicheniformis 9945a DISARM system into Bacillus subtilis has rendered the engineered bacteria protected against phages from all three major families of tailed double-stranded DNA phages. Using a series of gene deletions, we show that four of the five genes are essential for DISARM-mediated defence, with the fifth (PLD) being redundant for defence against some of the phages. We further show that DISARM restricts incoming phage DNA and that the B. paralicheniformis DISARM methylase modifies host CCWGG motifs as a marker of self DNA akin to restriction-modification systems. Our results suggest that DISARM is a new type of multi-gene restriction-modification module, expanding the arsenal of defence systems known to be at the disposal of prokaryotes against their viruses.


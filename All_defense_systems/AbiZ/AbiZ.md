# AbiZ

## Example of genomic structure

The AbiZ system is composed of one protein: AbiZ.

Here is an example found in the RefSeq database: 

<img src="./data/AbiZ.svg">

AbiZ system in the genome of *Streptococcus oralis* (GCF\_019334565.1) is composed of 1 protein: AbiZ (WP\_215804505.1).

## Distribution of the system among prokaryotes

The AbiZ system is present in a total of 191 different species.

Among the 22k complete genomes of RefSeq, this system is present in 831 genomes (3.6 %).

<img src="./data/Distribution_AbiZ.svg" width=800px>

*Proportion of genome encoding the AbiZ system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

AbiZ systems were experimentally validated using:

A system from *Lactococcus lactis* in *Lactococcus lactis* has an anti-phage effect against Phi31.2, ul36, phi31, phi48, phi31.1, Q30, Q36, Q33, phi50, phi48 (Durmaz et al., 2007)

## Relevant abstracts

**Durmaz, E. & Klaenhammer, T. R. Abortive Phage Resistance Mechanism AbiZ Speeds the Lysis Clock To Cause Premature Lysis of Phage-Infected Lactococcus lactis. J Bacteriol 189, 1417-1425 (2007).**
The conjugative plasmid pTR2030 has been used extensively to confer phage resistance in commercial Lactococcus starter cultures. The plasmid harbors a 16-kb region, flanked by insertion sequence (IS) elements, that encodes the restriction/modification system LlaI and carries an abortive infection gene, abiA. The AbiA system inhibits both prolate and small isometric phages by interfering with the early stages of phage DNA replication. However, abiA alone does not account for the full abortive activity reported for pTR2030. In this study, a 7.5-kb region positioned within the IS elements and downstream of abiA was sequenced to reveal seven additional open reading frames (ORFs). A single ORF, designated abiZ, was found to be responsible for a significant reduction in plaque size and an efficiency of plaquing (EOP) of 10?6, without affecting phage adsorption. AbiZ causes phage ?31-infected Lactococcus lactis NCK203 to lyse 15 min early, reducing the burst size of ?31 100-fold. Thirteen of 14 phages of the P335 group were sensitive to AbiZ, through reduction in either plaque size, EOP, or both. The predicted AbiZ protein contains two predicted transmembrane helices but shows no significant DNA homologies. When the phage ?31 lysin and holin genes were cloned into the nisin-inducible shuttle vector pMSP3545, nisin induction of holin and lysin caused partial lysis of NCK203. In the presence of AbiZ, lysis occurred 30 min earlier. In holin-induced cells, membrane permeability as measured using propidium iodide was greater in the presence of AbiZ. These results suggest that AbiZ may interact cooperatively with holin to cause premature lysis.

**Forde, A. & Fitzgerald, G. F. Bacteriophage defence systems in lactic acid bacteria. Antonie Van Leeuwenhoek 76, 89-113 (1999).**
The study of the interactions between lactic acid bacteria and their bacteriophages has been a vibrant and rewarding research activity for a considerable number of years. In the more recent past, the application of molecular genetics for the analysis of phage-host relationships has contributed enormously to the unravelling of specific events which dictate insensitivity to bacteriophage infection and has revealed that while they are complex and intricate in nature, they are also extremely effective. In addition, the strategy has laid solid foundations for the construction of phage resistant strains for use in commercial applications and has provided a sound basis for continued investigations into existing, naturally-derived and novel, genetically-engineered defence systems. Of course, it has also become clear that phage particles are highly dynamic in their response to those defence systems which they do encounter and that they can readily adapt to them as a consequence of their genetic flexibility and plasticity. This paper reviews the exciting developments that have been described in the literature regarding the study of phage-host interactions in lactic acid bacteria and the innovative approaches that can be taken to exploit this basic information for curtailing phage infection.


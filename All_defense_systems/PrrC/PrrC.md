# PrrC

## Example of genomic structure

The PrrC system is composed of 4 proteins: EcoprrI, Type_I_S, PrrC and, Type_I_REases.

Here is an example found in the RefSeq database: 

<img src="./data/PrrC.svg">

PrrC system in the genome of *Streptococcus canis* (GCF\_900636575.1) is composed of 4 proteins: Type\_I\_REases (WP\_003046543.1), PrrC (WP\_003046540.1), Type\_I\_S (WP\_129544911.1)and, Type\_I\_MTases (WP\_003046534.1).

## Distribution of the system among prokaryotes

The PrrC system is present in a total of 285 different species.

Among the 22k complete genomes of RefSeq, this system is present in 705 genomes (3.1 %).

<img src="./data/Distribution_PrrC.svg" width=800px>

*Proportion of genome encoding the PrrC system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

PrrC systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against Lambda, T4 Dec8 (Jabbar and Snyder, 1984)

## Relevant abstracts

**Penner, M., Morad, I., Snyder, L. & Kaufmann, G. Phage T4-coded Stp: double-edged effector of coupled DNA and tRNA-restriction systems. J Mol Biol 249, 857-868 (1995).**
The optional Escherichia coli prr locus encodes two physically associated restriction systems: the type IC DNA restriction-modification enzyme EcoprrI and the tRNA(Lys)-specific anticodon nuclease, specified by the PrrC polypeptide. Anticodon nuclease is kept latent as a result of this interaction. The activation of anticodon nuclease, upon infection by phage T4, may cause depletion of tRNA(Lys) and, consequently, abolition of T4 protein synthesis. However, this effect is counteracted by the repair of tRNA(Lys) in consecutive reactions catalysed by the phage enzymes polynucleotide kinase and RNA ligase. Stp, a short polypeptide encoded by phage T4, has been implicated with activation of the anticodon nuclease. Here we confirm this notion and also demonstrate a second function of Stp: inhibition of EcoprrI restriction. Both effects depend, in general, on the same residues within the N-proximal 18 residue region of Stp. We propose that Stp alters the conformation of EcoprrI and, consequently, of PrrC, allowing activation of the latent anticodon nuclease. Presumably, Stp evolved to offset a DNA restriction system of the host cell but was turned, eventually, against the phage as an activator of the appended tRNA restriction enzyme.

**Uzan, M. & Miller, E. S. Post-transcriptional control by bacteriophage T4: mRNA decay and inhibition of translation initiation. Virology Journal 7, 360 (2010).**
Over 50 years of biological research with bacteriophage T4 includes notable discoveries in post-transcriptional control, including the genetic code, mRNA, and tRNA; the very foundations of molecular biology. In this review we compile the past 10 - 15 year literature on RNA-protein interactions with T4 and some of its related phages, with particular focus on advances in mRNA decay and processing, and on translational repression. Binding of T4 proteins RegB, RegA, gp32 and gp43 to their cognate target RNAs has been characterized. For several of these, further study is needed for an atomic-level perspective, where resolved structures of RNA-protein complexes are awaiting investigation. Other features of post-transcriptional control are also summarized. These include: RNA structure at translation initiation regions that either inhibit or promote translation initiation; programmed translational bypassing, where T4 orchestrates ribosome bypass of a 50 nucleotide mRNA sequence; phage exclusion systems that involve T4-mediated activation of a latent endoribonuclease (PrrC) and cofactor-assisted activation of EF-Tu proteolysis (Gol-Lit); and potentially important findings on ADP-ribosylation (by Alt and Mod enzymes) of ribosome-associated proteins that might broadly impact protein synthesis in the infected cell. Many of these problems can continue to be addressed with T4, whereas the growing database of T4-related phage genome sequences provides new resources and potentially new phage-host systems to extend the work into a broader biological, evolutionary context.


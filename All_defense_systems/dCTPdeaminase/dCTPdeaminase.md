# dCTPdeaminase

## Description
dCTPdeaminase is a family of systems. dCTPdeaminase from Escherichia coli has been shown to provide resistance against various lytic phages when express heterologously in another Escherichia coli.
This system is mostly found in Proteobacteria but a few examples also exist in Acidobacteria, Actinobacteria, Bacteroidetes, Cyanobacteria, Firmicutes, Planctomyces, and Verrucomicrobia.
Those systems can be found in plasmids (around 8%).

## Mechanism
When activated by a phage infection, dCTPdeaminase, will convert deoxycytidine (dCTP/dCDP/dCMP) into deoxyuridine.
This action will deplete the pool of CTP nucleotide necessary for the phage replication and will stop the infection.
The trigger for dCTPdeaminase may be linked to the shutoff of RNAP (σS-dependent host RNA polymerase) that occur during phage infections.

## Example of genomic structure

The dCTPdeaminase system is composed of one protein: dCTPdeaminase.

Here is an example found in the RefSeq database: 

<img src="./data/dCTPdeaminase.svg">

dCTPdeaminase system in the genome of *Vibrio parahaemolyticus* (GCF\_009883855.1) is composed of 1 protein: dCTPdeaminase (WP\_029845369.1).

## Distribution of the system among prokaryotes

The dCTPdeaminase system is present in a total of 269 different species.

Among the 22k complete genomes of RefSeq, this system is present in 501 genomes (2.2 %).

<img src="./data/Distribution_dCTPdeaminase.svg" width=800px>

*Proportion of genome encoding the dCTPdeaminase system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

dCTPdeaminase systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T5, SECphi4, SECphi6, SECphi18, T2, T4, T6, T7 (Tal et al., 2022)

Subsystem AvcID with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T3, SECphi17, SECphi18, SECphi27 (Hsueh et al., 2022)

Subsystem AvcID with a system from *Proteus mirabilis* in *Escherichia coli*  has an anti-phage effect against  T4 (Hsueh et al., 2022)

Subsystem AvcID with a system from *Vibrio parahaemolyticus* in *Escherichia coli*  has an anti-phage effect against  T3, T5, T6, SECphi18 (Hsueh et al., 2022)

Subsystem AvcID with a system from *Vibrio cholerae* in *Escherichia coli*  has an anti-phage effect against  T2, T3 (Hsueh et al., 2022)

## Relevant abstracts

**Hsueh, B. Y. et al. Phage defence by deaminase-mediated depletion of deoxynucleotides in bacteria. Nat Microbiol 7, 1210-1220 (2022).**
Vibrio cholerae biotype El Tor is perpetuating the longest cholera pandemic in recorded history. The genomic islands VSP-1 and VSP-2 distinguish El Tor from previous pandemic V. cholerae strains. Using a co-occurrence analysis of VSP genes in >200,000 bacterial genomes we built gene networks to infer biological functions encoded in these islands. This revealed that dncV, a component of the cyclic-oligonucleotide-based anti-phage signalling system (CBASS) anti-phage defence system, co-occurs with an uncharacterized gene vc0175 that we rename avcD for anti-viral cytodine deaminase. We show that AvcD is a deoxycytidylate deaminase and that its activity is post-translationally inhibited by a non-coding RNA named AvcI. AvcID and bacterial homologues protect bacterial populations against phage invasion by depleting free deoxycytidine nucleotides during infection, thereby decreasing phage replication. Homologues of avcD exist in all three domains of life, and bacterial AvcID defends against phage infection by combining traits of two eukaryotic innate viral immunity proteins, APOBEC and SAMHD1.

**Hsueh, B. Y. et al. Phage defence by deaminase-mediated depletion of deoxynucleotides in bacteria. Nat Microbiol 7, 1210-1220 (2022).**
Vibrio cholerae biotype El Tor is perpetuating the longest cholera pandemic in recorded history. The genomic islands VSP-1 and VSP-2 distinguish El Tor from previous pandemic V. cholerae strains. Using a co-occurrence analysis of VSP genes in >200,000 bacterial genomes we built gene networks to infer biological functions encoded in these islands. This revealed that dncV, a component of the cyclic-oligonucleotide-based anti-phage signalling system (CBASS) anti-phage defence system, co-occurs with an uncharacterized gene vc0175 that we rename avcD for anti-viral cytodine deaminase. We show that AvcD is a deoxycytidylate deaminase and that its activity is post-translationally inhibited by a non-coding RNA named AvcI. AvcID and bacterial homologues protect bacterial populations against phage invasion by depleting free deoxycytidine nucleotides during infection, thereby decreasing phage replication. Homologues of avcD exist in all three domains of life, and bacterial AvcID defends against phage infection by combining traits of two eukaryotic innate viral immunity proteins, APOBEC and SAMHD1.


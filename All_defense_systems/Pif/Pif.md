# Pif

## Example of genomic structure

The Pif system is composed of 2 proteins: PifC and, PifA.

Here is an example found in the RefSeq database: 

<img src="./data/Pif.svg">

Pif system in the genome of *Escherichia coli* (GCF\_018628815.1) is composed of 2 proteins: PifA (WP\_000698737.1)and, PifC (WP\_000952217.1).

## Distribution of the system among prokaryotes

The Pif system is present in a total of 28 different species.

Among the 22k complete genomes of RefSeq, this system is present in 143 genomes (0.6 %).

<img src="./data/Distribution_Pif.svg" width=800px>

*Proportion of genome encoding the Pif system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Pif systems were experimentally validated using:

A system from *Escherichia coli F-plasmid* in *Escherichia coli* has an anti-phage effect against T7 (Cheng et al., 2004)

## Relevant abstracts

**Cheng, X., Wang, W. & Molineux, I. J. F exclusion of bacteriophage T7 occurs at the cell membrane. Virology 326, 340-352 (2004).**
The F plasmid PifA protein, known to be the cause of F exclusion of bacteriophage T7, is shown to be a membrane-associated protein. No transmembrane domains of PifA were located. In contrast, T7 gp1.2 and gp10, the two phage proteins that trigger phage exclusion, are both soluble cytoplasmic proteins. The Escherichia coli FxsA protein, which, at higher concentrations than found in wild-type cells, protects T7 from exclusion, is shown to interact with PifA. FxsA is a polytopic membrane protein with four transmembrane segments and a long cytoplasmic C-terminal tail. This tail is not important in alleviating F exclusion and can be deleted; in contrast, the fourth transmembrane segment of FxsA is critical in allowing wild-type T7 to grow in the presence of F PifA. These data suggest that the primary event that triggers the exclusion process occurs at the cytoplasmic membrane and that FxsA sequesters PifA so that membrane damage is minimized.

**Cram, D., Ray, A. & Skurray, R. Molecular analysis of F plasmid pif region specifying abortive infection of T7 phage. Mol Gen Genet 197, 137-142 (1984).**
We report the molecular cloning of the pif region of the F plasmid and its physical dissection by subcloning and deletion analysis. Examination of the polypeptide products synthesized in maxicells by plasmids carrying defined pif sequences has shown that the region specifies at least two proteins of molecular weights 80,000 and 40,000, the genes for which appear to lie in the same transcriptional unit. In addition, analysis of pif-lacZ fusion plasmids has detected a pif promoter and determined the direction of transcription across the pif region.

**Schmitt, C. K., Kemp, P. & Molineux, I. J. Genes 1.2 and 10 of bacteriophages T3 and T7 determine the permeability lesions observed in infected cells of Escherichia coli expressing the F plasmid gene pifA. J Bacteriol 173, 6507-6514 (1991).**
Infections of F plasmid-containing strains of Escherichia coli by bacteriophage T7 result in membrane damage that allows nucleotides to exude from the infected cell into the culture medium. Only pifA of the F pif operon is necessary for "leakiness" of the T7-infected cell. Expression of either T7 gene 1.2 or gene 10 is sufficient to cause leakiness, since infections by phage containing null mutations in both of these genes do not result in permeability changes of the F-containing cell. Even in the absence of phage infection, expression from plasmids of either gene 1.2 or 10 can cause permeability changes, particularly of F plasmid-containing cells. In contrast, gene 1.2 of the related bacteriophage T3 prevents leakiness of the infected cell. In the absence of T3 gene 1.2 function, expression of gene 10 causes membrane damage that allows nucleotides to leak from the cell. Genes 1.2 and 10 of both T3 and T7 are the two genes involved in determining resistance or sensitivity to F exclusion; F exclusion and leakiness of the phage-infected cell are therefore closely related phenomena. However, since leakiness of the infected cell does not necessarily result in phage exclusion, it cannot be used as a predictor of an abortive infection.


# PsyrTA

## Example of genomic structure

The PsyrTA system is composed of 2 proteins: PsyrT and, PsyrA.

Here is an example found in the RefSeq database: 

<img src="./data/PsyrTA.svg">

PsyrTA system in the genome of *Photorhabdus laumondii* (GCF\_003343225.1) is composed of 2 proteins: PsyrT (WP\_109791883.1)and, PsyrA (WP\_113049635.1).

## Distribution of the system among prokaryotes

The PsyrTA system is present in a total of 281 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1435 genomes (6.3 %).

<img src="./data/Distribution_PsyrTA.svg" width=800px>

*Proportion of genome encoding the PsyrTA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

PsyrTA systems were experimentally validated using:

A system from *Bacillus sp. FJAT-29814* in *Escherichia coli* has an anti-phage effect against T2, T4, T6 (Millman et al., 2022)

## Relevant abstracts

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.

**Sberro, H. et al. Discovery of functional toxin/antitoxin systems in bacteria by shotgun cloning. Mol Cell 50, 136-148 (2013).**
Toxin-antitoxin (TA) modules, composed of a toxic protein and a counteracting antitoxin, play important roles in bacterial physiology. We examined the experimental insertion of 1.5 million genes from 388 microbial genomes into an Escherichia coli host using more than 8.5 million random clones. This revealed hundreds of genes (toxins) that could only be cloned when the neighboring gene (antitoxin) was present on the same clone. Clustering of these genes revealed TA families widespread in bacterial genomes, some of which deviate from the classical characteristics previously described for such modules. Introduction of these genes into E. coli validated that the toxin toxicity is mitigated by the antitoxin. Infection experiments with T7 phage showed that two of the new modules can provide resistance against phage. Moreover, our experiments revealed an "antidefense" protein in phage T7 that neutralizes phage resistance. Our results expose active fronts in the arms race between bacteria and phage.


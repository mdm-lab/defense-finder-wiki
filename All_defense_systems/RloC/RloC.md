# RloC

## Example of genomic structure

The RloC system is composed of one protein: RloC.

Here is an example found in the RefSeq database: 

<img src="./data/RloC.svg">

RloC system in the genome of *Flavobacterium arcticum* (GCF\_003344925.1) is composed of 1 protein: RloC (WP\_114676820.1).

## Distribution of the system among prokaryotes

The RloC system is present in a total of 803 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1961 genomes (8.6 %).

<img src="./data/Distribution_RloC.svg" width=800px>

*Proportion of genome encoding the RloC system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

RloC systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T4 (Penner et al., 1995)

## Relevant abstracts

**Bitton, L., Klaiman, D. & Kaufmann, G. Phage T4-induced DNA breaks activate a tRNA repair-defying anticodon nuclease. Mol Microbiol 97, 898-910 (2015).**
The natural role of the conserved bacterial anticodon nuclease (ACNase) RloC is not known, but traits that set it apart from the homologous phage T4-excluding ACNase PrrC could provide relevant clues. PrrC is silenced by a genetically linked DNA restriction-modification (RM) protein and turned on by a phage-encoded DNA restriction inhibitor. In contrast, RloC is rarely linked to an RM protein, and its ACNase is regulated by an internal switch responsive to double-stranded DNA breaks. Moreover, PrrC nicks the tRNA substrate, whereas RloC excises the wobble nucleotide. These distinctions suggested that (i) T4 and related phage that degrade their host DNA will activate RloC and (ii) the tRNA species consequently disrupted will not be restored by phage tRNA repair enzymes that counteract PrrC. Consistent with these predictions we show that Acinetobacter baylyi?RloC expressed in Escherichia coli is activated by wild-type phage T4 but not by a mutant impaired in host DNA degradation. Moreover, host and T4 tRNA species disrupted by the activated ACNase were not restored by T4's tRNA repair system. Nonetheless, T4's plating efficiency was inefficiently impaired by AbaRloC, presumably due to a decoy function of the phage encoded tRNA target, the absence of which exacerbated the restriction.

**Davidov, E. & Kaufmann, G. RloC: a wobble nucleotide-excising and zinc-responsive bacterial tRNase. Mol Microbiol 69, 1560-1574 (2008).**
The conserved bacterial protein RloC, a distant homologue of the tRNA(Lys) anticodon nuclease (ACNase) PrrC, is shown here to act as a wobble nucleotide-excising and Zn(++)-responsive tRNase. The more familiar PrrC is silenced by a genetically linked type I DNA restriction-modification (R-M) enzyme, activated by a phage anti-DNA restriction factor and counteracted by phage tRNA repair enzymes. RloC shares PrrC's ABC ATPase motifs and catalytic ACNase triad but features a distinct zinc-hook/coiled-coil insert that renders its ATPase domain similar to Rad50 and related DNA repair proteins. Geobacillus kaustophilus RloC expressed in Escherichia coli exhibited ACNase activity that differed from PrrC's in substrate preference and ability to excise the wobble nucleotide. The latter specificity could impede reversal by phage tRNA repair enzymes and account perhaps for RloC's more frequent occurrence. Mutagenesis and functional assays confirmed RloC's catalytic triad assignment and implicated its zinc hook in regulating the ACNase function. Unlike PrrC, RloC is rarely linked to a type I R-M system but other genomic attributes suggest their possible interaction in trans. As DNA damage alleviates type I DNA restriction, we further propose that these related perturbations prompt RloC to disable translation and thus ward off phage escaping DNA restriction during the recovery from DNA damage.


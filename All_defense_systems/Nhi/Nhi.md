# Nhi

## Example of genomic structure

The Nhi system is composed of one protein: Nhi.

Here is an example found in the RefSeq database: 

<img src="./data/Nhi.svg">

Nhi system in the genome of *Enterococcus avium* (GCF\_003711125.1) is composed of 1 protein: Nhi (WP\_148712513.1).

## Distribution of the system among prokaryotes

The Nhi system is present in a total of 56 different species.

Among the 22k complete genomes of RefSeq, this system is present in 202 genomes (0.9 %).

<img src="./data/Distribution_Nhi.svg" width=800px>

*Proportion of genome encoding the Nhi system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Nhi systems were experimentally validated using:

Subsystem Nhi-like with a system from *Bacillus cereus* in *Bacillus subtilis*  has an anti-phage effect against  phi3T, SpBeta, SPR (Millman et al., 2022)

A system from *Staphylococcus epidermidis* in *Staphylococcus epidermidis* has an anti-phage effect against JBug18, Pike, CNPx (Bari et al., 2022)

A system from *Staphylococcus epidermidis* in *Staphylococcus aureus* has an anti-phage effect against Lorac (Bari et al., 2022)

A system from *Staphylococcus aureus* in *Staphylococcus aureus* has an anti-phage effect against Lorac (Bari et al., 2022)

A system from *Vibrio vulnificus* in *Staphylococcus aureus* has an anti-phage effect against Lorac (Bari et al., 2022)

## Relevant abstracts

**Bari, S. M. N. et al. A unique mode of nucleic acid immunity performed by a multifunctional bacterial enzyme. Cell Host Microbe 30, 570-582.e7 (2022).**
The perpetual arms race between bacteria and their viruses (phages) has given rise to diverse immune systems, including restriction-modification and CRISPR-Cas, which sense and degrade phage-derived nucleic acids. These complex systems rely upon production and maintenance of multiple components to achieve antiphage defense. However, the prevalence and effectiveness of minimal, single-component systems that cleave DNA remain unknown. Here, we describe a unique mode of nucleic acid immunity mediated by a single enzyme with nuclease and helicase activities, herein referred to as Nhi (nuclease-helicase immunity). This enzyme provides robust protection against diverse staphylococcal phages and prevents phage DNA accumulation in cells stripped of all other known defenses. Our observations support a model in which Nhi targets and degrades phage-specific replication intermediates. Importantly, Nhi homologs are distributed in diverse bacteria and exhibit functional conservation, highlighting the versatility of such compact weapons as major players in antiphage defense.


# Gao_Her

## Example of genomic structure

The Gao_Her system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Gao_Her_DUF.svg">

Gao\_Her\_DUF subsystem in the genome of *Enterobacter roggenkampii* (GCF\_014524505.1) is composed of 2 proteins: DUF4297 (WP\_188074283.1)and, HerA\_DUF (WP\_063614829.1).

<img src="./data/Gao_Her_SIR.svg">

Gao\_Her\_SIR subsystem in the genome of *Escherichia coli* (GCF\_012221565.1) is composed of 2 proteins: SIR2 (WP\_167839366.1)and, HerA\_SIR2 (WP\_021577682.1).

## Distribution of the system among prokaryotes

The Gao_Her system is present in a total of 127 different species.

Among the 22k complete genomes of RefSeq, this system is present in 233 genomes (1.0 %).

<img src="./data/Distribution_Gao_Her.svg" width=800px>

*Proportion of genome encoding the Gao_Her system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Gao_Her systems were experimentally validated using:

Subsystem SIR2 + HerA with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  Lambda, T3, T7, PhiV-1 (Gao et al., 2020)

Subsystem DUF4297 + HerA with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T4, P1, Lambda, T3, T7 (Gao et al., 2020)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.


# BstA

## Description

BstA is a family of defense systems. BtsA systems from *Salmonella enterica subsp. enterica*, *Klebsiella pneumoniae* and *Escherichia coli* have been shown to provide resistance against a large diversity of phages when expressed in a *S. enterica* or *E.coli* host (1).

The majority of BstA systems appear to be prophage-encoded, as 79% of BstA homologs found in a set of Gram-negative bacterial genomes were associted with phage genes (1).

The defense mechanism encoded by BstA remains to be elucidated. Experimental observation suggest that BtsA could act through an abortive infection mechanism. Fluorescence microscopy experiments suggest that the BstA protein colocalizes with phage DNA. The BstA protein appears to inhibit phage DNA replication during lytic phage infection cycles (1).

Interestingly, part of the BstA locus appears to encode an anti-BstA genetic element (*aba*), which prevents auto-immunity for prophages encoding the BstA locus. The aba element appears to be specific to a given BstA locus, as replacing the aba element from a BstA locus with the aba element from an other BstA system does not prevent auto-immunity (1). 

## Example of genomic structure

The BstA system is composed of one protein: BstA.

Here is an example found in the RefSeq database: 

<img src="./data/BstA.svg">

BstA system in the genome of *Providencia rustigianii* (GCF\_900635875.1) is composed of 1 protein: BstA (WP\_126437212.1).

## Distribution of the system among prokaryotes

The BstA system is present in a total of 81 different species.

Among the 22k complete genomes of RefSeq, this system is present in 236 genomes (1.0 %).

<img src="./data/Distribution_BstA.svg" width=800px>

*Proportion of genome encoding the BstA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

BstA systems were experimentally validated using:

A system from *Salmonella Typhimurium's BTP1 prophage* in *Salmonella Typhimurium* has an anti-phage effect against P22, ES18, 9NA (Owen et al., 2021)

A system from *Salmonella Typhimurium's BTP1 prophage* in *Escherichia coli* has an anti-phage effect against P22, ES18, 9NA (Owen et al., 2021)

A system from *Klebsiella pneumoniae* in *Salmonella Typhimurium* has an anti-phage effect against BTP1, P22, ES18, P22 HT, 9NA, Felix O1 (Owen et al., 2021)

A system from *Escherichia coli* in *Salmonella Typhimurium* has an anti-phage effect against BTP1, P22, ES18, P22 HT, 9NA (Owen et al., 2021)

A system from *Salmonella Typhimurium's BTP1* in *Escherichia coli* has an anti-phage effect against Lambda, Phi80, P1vir, T7 (Owen et al., 2021)

## Relevant abstracts

**Owen, S. V. et al. Prophages encode phage-defense systems with cognate self-immunity. Cell Host Microbe 29, 1620-1633.e8 (2021).**
Temperate phages are pervasive in bacterial genomes, existing as vertically inherited islands termed prophages. Prophages are vulnerable to predation of their host bacterium by exogenous phages. Here, we identify BstA, a family of prophage-encoded phage-defense proteins in diverse Gram-negative bacteria. BstA localizes to sites of exogenous phage DNA replication and mediates abortive infection, suppressing the competing phage epidemic. During lytic replication, the BstA-encoding prophage is not itself inhibited by BstA due to self-immunity conferred by the anti-BstA (aba) element, a short stretch of DNA within the bstA locus. Inhibition of phage replication by distinct BstA proteins from Salmonella, Klebsiella, and Escherichia prophages is generally interchangeable, but each possesses a cognate aba element. The specificity of the aba element ensures that immunity is exclusive to the replicating prophage, preventing exploitation by variant BstA-encoding phages. The BstA protein allows prophages to defend host cells against exogenous phage attack without sacrificing the ability to replicate lytically.

## References

1\. Owen SV, Wenner N, Dulberger CL, Rodwell EV, Bowers-Barnard A, Quinones-Olvera N, Rigden DJ, Rubin EJ, Garner EC, Baym M, Hinton JCD. Prophages encode phage-defense systems with cognate self-immunity. Cell Host Microbe. 2021 Nov 10;29(11):1620-1633.e8. doi: 10.1016/j.chom.2021.09.002. Epub 2021 Sep 30. PMID: 34597593; PMCID: PMC8585504.

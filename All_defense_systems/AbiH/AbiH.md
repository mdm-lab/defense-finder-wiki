# AbiH

## Example of genomic structure

The AbiH system is composed of one protein: AbiH.

Here is an example found in the RefSeq database: 

<img src="./data/AbiH.svg">

AbiH system in the genome of *Agrobacterium tumefaciens* (GCF\_005221405.1) is composed of 1 protein: AbiH (WP\_045021548.1).

## Distribution of the system among prokaryotes

The AbiH system is present in a total of 408 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1277 genomes (5.6 %).

<img src="./data/Distribution_AbiH.svg" width=800px>

*Proportion of genome encoding the AbiH system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

AbiH systems were experimentally validated using:

A system from *lactococci* in *lactococci* has an anti-phage effect against 936, c2 (Chopin et al., 2005)

## Relevant abstracts

**Chopin, M.-C., Chopin, A. & Bidnenko, E. Phage abortive infection in lactococci: variations on a theme. Curr Opin Microbiol 8, 473-479 (2005).**
Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.

**Forde, A. & Fitzgerald, G. F. Bacteriophage defence systems in lactic acid bacteria. Antonie Van Leeuwenhoek 76, 89-113 (1999).**
The study of the interactions between lactic acid bacteria and their bacteriophages has been a vibrant and rewarding research activity for a considerable number of years. In the more recent past, the application of molecular genetics for the analysis of phage-host relationships has contributed enormously to the unravelling of specific events which dictate insensitivity to bacteriophage infection and has revealed that while they are complex and intricate in nature, they are also extremely effective. In addition, the strategy has laid solid foundations for the construction of phage resistant strains for use in commercial applications and has provided a sound basis for continued investigations into existing, naturally-derived and novel, genetically-engineered defence systems. Of course, it has also become clear that phage particles are highly dynamic in their response to those defence systems which they do encounter and that they can readily adapt to them as a consequence of their genetic flexibility and plasticity. This paper reviews the exciting developments that have been described in the literature regarding the study of phage-host interactions in lactic acid bacteria and the innovative approaches that can be taken to exploit this basic information for curtailing phage infection.

**Prévots, F., Daloyau, M., Bonin, O., Dumont, X. & Tolou, S. Cloning and sequencing of the novel abortive infection gene abiH of Lactococcus lactis ssp. lactis biovar. diacetylactis S94. FEMS Microbiol Lett 142, 295-299 (1996).**
A gene which encodes resistance by abortive infection (Abi+) to bacteriophage was cloned from Lactococcus lactis ssp. lactis biovar. diacetylactis S94. This gene was found to confer a reduction in efficiency of plating and plaque size for prolate-headed bacteriophage phi 53 (group I of homology) and total resistance to the small isometric-headed bacteriophage phi 59 (group III of homology). The cloned gene is predicted to encode a polypeptide of 346 amino acid residues with a deduced molecular mass of 41 455 Da. No homology with any previously described genes was found. A probe was used to determine the presence of this gene in two strains on 31 tested.


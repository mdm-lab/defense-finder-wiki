# Rst_3HP

## Example of genomic structure

The Rst_3HP system is composed of 3 proteins: Hp1, Hp2 and, Hp3.

Here is an example found in the RefSeq database: 

<img src="./data/Rst_3HP.svg">

Rst\_3HP system in the genome of *Klebsiella pneumoniae* (GCF\_016403065.1) is composed of 3 proteins: Hp3 (WP\_004151009.1), Hp2 (WP\_004151008.1)and, Hp1 (WP\_004151007.1).

## Distribution of the system among prokaryotes

The Rst_3HP system is present in a total of 83 different species.

Among the 22k complete genomes of RefSeq, this system is present in 420 genomes (1.8 %).

<img src="./data/Distribution_Rst_3HP.svg" width=800px>

*Proportion of genome encoding the Rst_3HP system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Rst_3HP systems were experimentally validated using:

A system from *Escherichia coli (P2 loci)* in *Escherichia coli* has an anti-phage effect against P1 (Rousset et al., 2022)

## Relevant abstracts

**Rousset, F. et al. Phages and their satellites encode hotspots of antiviral systems. Cell Host & Microbe 30, 740-753.e5 (2022).**
Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.


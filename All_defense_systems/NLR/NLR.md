# NLR

## Example of genomic structure

The NLR system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/NLR_like_bNACHT01.svg">

NLR\_like\_bNACHT01 subsystem in the genome of *Pseudomonas psychrotolerans* (GCF\_001913135.1) is composed of 1 protein: NLR\_like\_bNACHT01 (WP\_074528296.1).

<img src="./data/NLR_like_bNACHT09.svg">

NLR\_like\_bNACHT09 subsystem in the genome of *Escherichia coli* (GCF\_900636105.1) is composed of 1 protein: NLR\_like\_bNACHT09 (WP\_089572057.1).

## Distribution of the system among prokaryotes

The NLR system is present in a total of 186 different species.

Among the 22k complete genomes of RefSeq, this system is present in 453 genomes (2.0 %).

<img src="./data/Distribution_NLR.svg" width=800px>

*Proportion of genome encoding the NLR system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

NLR systems were experimentally validated using:

Subsystem bNACHT01 with a system from *Klebsiella pneumoniae* in *Escherichia coli*  has an anti-phage effect against  T4, T5, T6 (Kibby et al., 2022)

Subsystem bNACHT02 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T7, MS2 (Kibby et al., 2022)

Subsystem bNACHT11 with a system from *Klebsiella pneumoniae* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T6 (Kibby et al., 2022)

Subsystem bNACHT12 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T4, T6, MS2 (Kibby et al., 2022)

Subsystem bNACHT23 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T6, T5 (Kibby et al., 2022)

Subsystem bNACHT25 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T6, LambdaVir, MS2 (Kibby et al., 2022)

Subsystem bNACHT32 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T6, LambdaVir, MS2 (Kibby et al., 2022)

Subsystem bNACHT67 with a system from *Klebsiella michiganensis* in *Escherichia coli*  has an anti-phage effect against  T2, T4 (Kibby et al., 2022)

Subsystem bNACHT09 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T5, LambdaVir, T3, T7 (Kibby et al., 2022)

## Relevant abstracts

**Kibby, E. M. et al. Bacterial NLR-related proteins protect against phage. 2022.07.19.500537 Preprint at https://doi.org/10.1101/2022.07.19.500537 (2022).**
Bacteria use a wide range of immune systems to counter phage infection. A subset of these genes share homology with components of eukaryotic immune systems, suggesting that eukaryotes horizontally acquired certain innate immune genes from bacteria. Here we show that proteins containing a NACHT module, the central feature of the animal nucleotide-binding domain and leucine-rich repeat containing gene family (NLRs), are found in bacteria and defend against phages. NACHT proteins are widespread in bacteria, provide immunity against both DNA and RNA phages, and display the characteristic C-terminal sensor, central NACHT, and N-terminal effector modules. Some bacterial NACHT proteins have domain architectures similar to human NLRs that are critical components of inflammasomes. Human disease-associated NLR mutations that cause stimulus-independent activation of the inflammasome also activate bacterial NACHT proteins, supporting a shared signaling mechanism. This work establishes that NACHT module-containing proteins are ancient mediators of innate immunity across the tree of life.


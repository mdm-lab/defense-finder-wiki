# DRT

## Example of genomic structure

The DRT system have been describe in a total of 9 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/DRT6.svg">

DRT6 subsystem in the genome of *Methylobacterium sp.* (GCF\_003254375.1) is composed of 1 protein: DRT6 (WP\_111474389.1).

<img src="./data/DRT8.svg">

DRT8 subsystem in the genome of *Undibacterium sp.* (GCF\_009937955.1) is composed of 2 proteins: DRT8b (WP\_162060770.1)and, DRT8 (WP\_162060771.1).

<img src="./data/DRT9.svg">

DRT9 subsystem in the genome of *Pseudomonas aeruginosa* (GCF\_016864415.1) is composed of 1 protein: DRT9 (WP\_071567741.1).

<img src="./data/DRT_1.svg">

DRT\_1 subsystem in the genome of *Vibrio parahaemolyticus* (GCF\_000430405.1) is composed of 2 proteins: drt1a (WP\_020841728.1)and, drt1b (WP\_020841729.1).

<img src="./data/DRT_2.svg">

DRT\_2 subsystem in the genome of *Klebsiella variicola* (GCF\_018324045.1) is composed of 1 protein: drt2 (WP\_020244644.1).

<img src="./data/DRT_3.svg">

DRT\_3 subsystem in the genome of *Vibrio mimicus* (GCF\_019048845.1) is composed of 2 proteins: drt3a (WP\_217011272.1)and, drt3b (WP\_217011273.1).

<img src="./data/DRT_4.svg">

DRT\_4 subsystem in the genome of *Escherichia albertii* (GCF\_003316815.1) is composed of 1 protein: drt4 (WP\_103054060.1).

<img src="./data/DRT_5.svg">

DRT\_5 subsystem in the genome of *Escherichia coli* (GCF\_016904115.1) is composed of 1 protein: drt5 (WP\_001524904.1).

## Distribution of the system among prokaryotes

The DRT system is present in a total of 573 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1365 genomes (6.0 %).

<img src="./data/Distribution_DRT.svg" width=800px>

*Proportion of genome encoding the DRT system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

DRT systems were experimentally validated using:

Subsystem RT-nitrilase (UG1) (Type 1) with a system from *Klebsiella pneumoniae* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T5 (Gao et al., 2020)

Subsystem RT (UG2) (Type 2) with a system from *Salmonella enterica* in *Escherichia coli*  has an anti-phage effect against  T5, T2 (Gao et al., 2020; Mestre et al., 2022)

Subsystem RT (UG3) + RT (UG8) (Type 3) with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T5, Lambda (Gao et al., 2020)

Subsystem RT (UG15) (Type 4) with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T5, T3, T7, Phi-V1, ZL19 (Gao et al., 2020; Mestre et al., 2022)

Subsystem RT (UG16) (Type 5) with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2 (Gao et al., 2020)

Subsystem RT (UG10) (Type 7) with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T5, ZL-19 (Mestre et al., 2022)

Subsystem RT(UG7) (Type 8) with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T5 (Mestre et al., 2022)

Subsystem RT (UG28) (Type 9) with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T5, ZL-19 (Mestre et al., 2022)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.

**Mestre, M. R. et al. UG/Abi: a highly diverse family of prokaryotic reverse transcriptases associated with defense functions. Nucleic Acids Research 50, 6084-6101 (2022).**
Reverse transcriptases (RTs) are enzymes capable of synthesizing DNA using RNA as a template. Within the last few years, a burst of research has led to the discovery of novel prokaryotic RTs with diverse antiviral properties, such as DRTs (Defense-associated RTs), which belong to the so-called group of unknown RTs (UG) and are closely related to the Abortive Infection system (Abi) RTs. In this work, we performed a systematic analysis of UG and Abi RTs, increasing the number of UG/Abi members up to 42 highly diverse groups, most of which are predicted to be functionally associated with other gene(s) or domain(s). Based on this information, we classified these systems into three major classes. In addition, we reveal that most of these groups are associated with defense functions and/or mobile genetic elements, and demonstrate the antiphage role of four novel groups. Besides, we highlight the presence of one of these systems in novel families of human gut viruses infecting members of the Bacteroidetes and Firmicutes phyla. This work lays the foundation for a comprehensive and unified understanding of these highly diverse RTs with enormous biotechnological potential.


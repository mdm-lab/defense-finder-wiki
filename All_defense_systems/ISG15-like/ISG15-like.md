# ISG15-like

## Example of genomic structure

The ISG15-like system is composed of 4 proteins: BilD, BilC, BilB and, BilA.

Here is an example found in the RefSeq database: 

<img src="./data/ISG15-like.svg">

ISG15-like system in the genome of *Rhizobium phaseoli* (GCF\_001664285.1) is composed of 4 proteins: BilA (WP\_064823699.1), BilB (WP\_150124924.1), BilC (WP\_150124925.1)and, BilD (WP\_190304495.1).

## Distribution of the system among prokaryotes

The ISG15-like system is present in a total of 28 different species.

Among the 22k complete genomes of RefSeq, this system is present in 43 genomes (0.2 %).

<img src="./data/Distribution_ISG15-like.svg" width=800px>

*Proportion of genome encoding the ISG15-like system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

ISG15-like systems were experimentally validated using:

A system from *Collimonas sp. OK412* in *Escherichia coli* has an anti-phage effect against T2, T4, T6, T5, SECphi4, SECphi6, SECphi18, SECphi27, T7, SECphi17 (Millman et al., 2022)

A system from *Caulobacter sp. Root343* in *Escherichia coli* has an anti-phage effect against T4, T6, T5, SECphi4, SECphi6, SECphi18, SECphi27, T7, SECphi17 (Millman et al., 2022)

A system from *Cupriavidus sp. SHE* in *Escherichia coli* has an anti-phage effect against T2, T4, T6, T5, SECphi4, SECphi6, SECphi18, SECphi27 (Millman et al., 2022)

A system from *Paraburkholderia caffeinilytica* in *Escherichia coli* has an anti-phage effect against T6, SECphi27 (Millman et al., 2022)

A system from *Thiomonas sp. FB-6* in *Escherichia coli* has an anti-phage effect against SECphi27 (Millman et al., 2022)

## Relevant abstracts

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.


# Gao_Qat

## Example of genomic structure

The Gao_Qat system is composed of 4 proteins: QatA, QatB, QatC and, QatD.

Here is an example found in the RefSeq database: 

<img src="./data/Gao_Qat.svg">

Gao\_Qat system in the genome of *Raoultella ornithinolytica* (GCF\_002214825.1) is composed of 4 proteins: QatA (WP\_088883811.1), QatB (WP\_127146083.1), QatC (WP\_088883813.1)and, QatD (WP\_088883814.1).

## Distribution of the system among prokaryotes

The Gao_Qat system is present in a total of 246 different species.

Among the 22k complete genomes of RefSeq, this system is present in 645 genomes (2.8 %).

<img src="./data/Distribution_Gao_Qat.svg" width=800px>

*Proportion of genome encoding the Gao_Qat system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Gao_Qat systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against P1, Lambda (Gao et al., 2020)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.


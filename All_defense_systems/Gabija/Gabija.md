# Gabija

## Description

According to recent studies, GajA is a sequence-specific DNA nicking endonuclease, whose activity is inhibited by nucleotide concentration. Accordingly, GajA would be fully inhibited at cellular nucleotides concentrations. It was hypothesized that upon nucleotide depletion during phage infection, GajA would become activated (2). 

Another study suggests that the *gajB* gene could encode for an NTPase, which would form a complex with GajA to achieve anti-phage defense (3).

## Molecular mechanism

The precise mechanism of the Gabija system remains to be fully described, yet studies suggest that it could act either as a nucleic acid degrading system or as an abortive infection system.

## Example of genomic structure

The Gabija system is composed of 2 proteins: GajA and, GajB_2.

Here is an example found in the RefSeq database: 

<img src="./data/Gabija.svg">

Gabija system in the genome of *Vibrio parahaemolyticus* (GCF\_009883895.1) is composed of 2 proteins: GajA (WP\_085576823.1)and, GajB\_1 (WP\_031856308.1).

## Distribution of the system among prokaryotes

The Gabija system is present in a total of 1200 different species.

Among the 22k complete genomes of RefSeq, this system is present in 3762 genomes (16.5 %).

<img src="./data/Distribution_Gabija.svg" width=800px>

*Proportion of genome encoding the Gabija system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Gabija systems were experimentally validated using:

A system from *Bacillus cereus* in *Bacillus subtilis* has an anti-phage effect against SBSphiC, SpBeta, phi105, rho14, phi29 (Doron et al., 2018)

A system from *Bacillus cereus* in *Bacillus subtilis* has an anti-phage effect against SpBeta, phi105 (Doron et al., 2018)

A system from *Bacillus cereus* in *Escherichia coli* has an anti-phage effect against T7 (Cheng et al., 2021)

## Relevant abstracts

**Cheng, R. et al. A nucleotide-sensing endonuclease from the Gabija bacterial defense system. Nucleic Acids Res 49, 5216-5229 (2021).**
The arms race between bacteria and phages has led to the development of exquisite bacterial defense systems including a number of uncharacterized systems distinct from the well-known restriction-modification and CRISPR/Cas systems. Here, we report functional analyses of the GajA protein from the newly predicted Gabija system. The GajA protein is revealed as a sequence-specific DNA nicking endonuclease unique in that its activity is strictly regulated by nucleotide concentration. NTP and dNTP at physiological concentrations can fully inhibit the robust DNA cleavage activity of GajA. Interestingly, the nucleotide inhibition is mediated by an ATPase-like domain, which usually hydrolyzes ATP to stimulate the DNA cleavage when associated with other nucleases. These features suggest a mechanism of the Gabija defense in which an endonuclease activity is suppressed under normal conditions, while it is activated by the depletion of NTP and dNTP upon the replication and transcription of invading phages. This work highlights a concise strategy to utilize a DNA nicking endonuclease for phage resistance via nucleotide regulation.

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.


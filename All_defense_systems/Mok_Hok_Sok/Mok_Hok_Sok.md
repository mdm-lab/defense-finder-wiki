# Mok_Hok_Sok

## Example of genomic structure

The Mok_Hok_Sok system is composed of 2 proteins: Mok and, Hok.

Here is an example found in the RefSeq database: 

<img src="./data/Mok_Hok_Sok.svg">

Mok\_Hok\_Sok system in the genome of *Raoultella terrigena* (GCF\_015571975.1) is composed of 2 proteins: Hok (WP\_227629320.1)and, Mok (WP\_227699927.1).

## Distribution of the system among prokaryotes

The Mok_Hok_Sok system is present in a total of 57 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1687 genomes (7.4 %).

<img src="./data/Distribution_Mok_Hok_Sok.svg" width=800px>

*Proportion of genome encoding the Mok_Hok_Sok system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Mok_Hok_Sok systems were experimentally validated using:

A system from *R1 plasmid of Salmonella paratyphi* in *Escherichia coli* has an anti-phage effect against T4, LambdaVir (Pecota and Wood, 1996)

## Relevant abstracts

**Pecota, D. C. & Wood, T. K. Exclusion of T4 phage by the hok/sok killer locus from plasmid R1. Journal of Bacteriology 178, 2044 (1996).**
The hok (host killing) and sok (suppressor of killing) genes (hok/sok) efficiently maintain the low-copy-number plasmid R1. To investigate whether the hok/sok locus evolved as a phage-exclusion mechanism, Escherichia coli cells that contain hok/sok on ...


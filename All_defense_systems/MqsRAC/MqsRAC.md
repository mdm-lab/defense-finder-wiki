# MqsRAC

## Example of genomic structure

The MqsRAC system is composed of 2 proteins: mqsR and, mqsC.

Here is an example found in the RefSeq database: 

<img src="./data/MqsRAC.svg">

MqsRAC system in the genome of *Escherichia coli* (GCF\_900636115.1) is composed of 2 proteins: mqsR (WP\_024222007.1)and, mqsC (WP\_021568458.1).

## Distribution of the system among prokaryotes

The MqsRAC system is present in a total of 18 different species.

Among the 22k complete genomes of RefSeq, this system is present in 26 genomes (0.1 %).

<img src="./data/Distribution_MqsRAC.svg" width=800px>

*Proportion of genome encoding the MqsRAC system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Relevant abstracts


# SEFIR

## Description
The SEFIR defense system is composed of a single bacterial SEFIR (bSEFIR)-domain protein. bSEFIR-domain genes were identified in bacterial genomes, were shown to be enriched in defense islands and the activity of the defense system was first experimentally validated in *Bacillus sp.* NIO-1130 against phage phi29 [1]. 

Bacterial SEFIR domains were named after their eukaryotic homologs which were already known to be part of several eukayrotic immune proteins (e.g. SEFs and Interleukin-17 Receptors) [2].

## Molecular mechanism
SEFIR was shown to protect against phage infection through an abortive infection mechanism *via* NAD+ depletion.  This is similar to what can be observed in other defense systems containing a TIR domain which shares homology with the SEFIR domain (in eukaryotes, both domains are part of the STIR super family) [1].

## Example of genomic structure

The SEFIR system is composed of one protein: bSEFIR.

Here is an example found in the RefSeq database: 

<img src="./data/SEFIR.svg">

SEFIR system in the genome of *Lactiplantibacillus plantarum* (GCF\_003020005.1) is composed of 1 protein: bSEFIR (WP\_106904862.1).

## Distribution of the system among prokaryotes

The SEFIR system is present in a total of 226 different species.

Among the 22k complete genomes of RefSeq, this system is present in 377 genomes (1.7 %).

<img src="./data/Distribution_SEFIR.svg" width=800px>

*Proportion of genome encoding the SEFIR system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

SEFIR systems were experimentally validated using:

A system from *Bacillus sp. NIO-1130* in *Bacillus subtilis* has an anti-phage effect against phi29 (Millman et al., 2022)

## Relevant abstracts

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.

## References
[1] Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).
[2] Novatchkova, M., Leibbrandt, A., Werzowa, J., Neubüser, A., & Eisenhaber, F. (2003). The STIR-domain superfamily in signal transduction, development and immunity. _Trends in biochemical sciences_, _28_(5), 226-229.

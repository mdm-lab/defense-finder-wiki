# BREX

## Description

BREX (for Bacteriophage Exclusion) is a family of anti-phage defense systems. BREX systems are active against both lytic and lysogenic phages. They allow phage adsorption but block phage DNA replication, and are considered to be [RM](/list_defense_systems/RM)\-like systems (1,2). BREX systems are found in around 10% of sequenced microbial genomes (1).

BREX systems can be divided into six subtypes, and are encoded by 4 to 8 genes, some of these genes being mandatory while others are subtype-specific (1).

## Molecular mechanism

*B. cereus* BREX Type 1 system was reported to methylate target motifs in the bacterial genome (1). The methylation activity of this system has been hypothesized to allow for self from non-self discrimination, as it is the case for Restriction-Modification ([RM)](/list_defense_systems/RM) systems. 

However, the mechanism through which BREX Type 1 systems defend against phages is distinct from RM systems, and does not seem to degrade phage nucleic acids (1). 

To date, BREX molecular mechanism remains to be described.


## Example of genomic structure

The BREX system have been describe in a total of 6 subsystems.

BREX systems necessarily include the pglZ gene (encoding for a putative alkaline phosphatase), which is accompanied by either brxC or pglY. These two genes share only a distant homology but have been hypothesized to fulfill the same function among the different BREX subtypes (1).

Goldfarb and colleagues reported a 6-gene cassette from *Bacillus cereus* as being the model for BREX Type 1. BREX Type 1 are the most widespread BREX systems, and present two core genes (pglZ and brxC).  Four other genes  are associated with BREX Type 1 : *pglX (*encoding for a putative methyltransferase),  *brxA (*encoding an RNA-binding anti-termination protein)*, brxB (*unknown functio*n), brxC (*encoding for a protein with ATP-binding domain) and *brxL* (encoding for a putative protease) (1,2).

Type 2 BREX systems include the system formerly known as Pgl , which is comprised of four genes  (pglW, X, Y, and Z) (3), to which Goldfarb and colleagues found often associated two additional genes (brxD, and brxHI).

Although 4 additional BREX subtypes have been proposed, BREX Type 1 and Type 2 remain the only ones to be experimentally validated. A detailed description of the other subtypes can be found in Goldfarb *et al*., 2015.

Here is some example found in the RefSeq database:

<img src="./data/BREX_I.svg">

BREX\_I subsystem in the genome of *Kaistella sp.* (GCF\_020410745.1) is composed of 6 proteins: brxL (WP\_226063319.1), pglZA (WP\_226063320.1), pglX1 (WP\_226063321.1), brxC (WP\_226063322.1), brxB\_DUF1788 (WP\_226063323.1)and, brxA\_DUF1819 (WP\_226063324.1).

<img src="./data/BREX_II.svg">

BREX\_II subsystem in the genome of *Streptomyces hygroscopicus* (GCF\_001447075.1) is composed of 5 proteins: brxD (WP\_058082289.1), pglZ2 (WP\_058082290.1), pglY (WP\_058082291.1), pglX2 (WP\_058082292.1)and, pglW (WP\_237280966.1).

## Distribution of the system among prokaryotes

The BREX system is present in a total of 732 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1612 genomes (7.1 %).

<img src="./data/Distribution_BREX.svg" width=800px>

*Proportion of genome encoding the BREX system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

BREX systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against Lambda (Gao et al., 2020 ; Gordeeva et al., 2017)

A system from *Bacillus cereus* in *Bacillus subtilis* has an anti-phage effect against SPbeta, SP16, Zeta, phi3T, SPO2, SPO1, SP82G (Goldfarb et al., 2015)

## Relevant abstracts

**Goldfarb, T. et al. BREX is a novel phage resistance system widespread in microbial genomes. EMBO J 34, 169-183 (2015).**
The perpetual arms race between bacteria and phage has resulted in the evolution of efficient resistance systems that protect bacteria from phage infection. Such systems, which include the CRISPR-Cas and restriction-modification systems, have proven to be invaluable in the biotechnology and dairy industries. Here, we report on a six-gene cassette in Bacillus cereus which, when integrated into the Bacillus subtilis genome, confers resistance to a broad range of phages, including both virulent and temperate ones. This cassette includes a putative Lon-like protease, an alkaline phosphatase domain protein, a putative RNA-binding protein, a DNA methylase, an ATPase-domain protein, and a protein of unknown function. We denote this novel defense system BREX (Bacteriophage Exclusion) and show that it allows phage adsorption but blocks phage DNA replication. Furthermore, our results suggest that methylation on non-palindromic TAGGAG motifs in the bacterial genome guides self/non-self discrimination and is essential for the defensive function of the BREX system. However, unlike restriction-modification systems, phage DNA does not appear to be cleaved or degraded by BREX, suggesting a novel mechanism of defense. Pan genomic analysis revealed that BREX and BREX-like systems, including the distantly related Pgl system described in Streptomyces coelicolor, are widely distributed in ~10% of all sequenced microbial genomes and can be divided into six coherent subtypes in which the gene composition and order is conserved. Finally, we detected a phage family that evades the BREX defense, implying that anti-BREX mechanisms may have evolved in some phages as part of their arms race with bacteria.

**Gordeeva, J. et al. BREX system of Escherichia coli distinguishes self from non-self by methylation of a specific DNA site. Nucleic Acids Res 47, 253-265 (2019).**
Prokaryotes evolved numerous systems that defend against predation by bacteriophages. In addition to well-known restriction-modification and CRISPR-Cas immunity systems, many poorly characterized systems exist. One class of such systems, named BREX, consists of a putative phosphatase, a methyltransferase and four other proteins. A Bacillus cereus BREX system provides resistance to several unrelated phages and leads to modification of specific motif in host DNA. Here, we study the action of BREX system from a natural Escherichia coli isolate. We show that while it makes cells resistant to phage ? infection, induction of ? prophage from cells carrying BREX leads to production of viruses that overcome the defense. The induced phage DNA contains a methylated adenine residue in a specific motif. The same modification is found in the genome of BREX-carrying cells. The results establish, for the first time, that immunity to BREX system defense is provided by an epigenetic modification.

**Isaev, A. et al. Phage T7 DNA mimic protein Ocr is a potent inhibitor of BREX defence. Nucleic Acids Research 48, 5397-5406 (2020).**
BREX (for BacteRiophage EXclusion) is a superfamily of common bacterial and archaeal defence systems active against diverse bacteriophages. While the mechanism of BREX defence is currently unknown, self versus non-self differentiation requires methylation of specific asymmetric sites in host DNA by BrxX (PglX) methyltransferase. Here, we report that T7 bacteriophage Ocr, a DNA mimic protein that protects the phage from the defensive action of type I restriction-modification systems, is also active against BREX. In contrast to the wild-type phage, which is resistant to BREX defence, T7 lacking Ocr is strongly inhibited by BREX, and its ability to overcome the defence could be complemented by Ocr provided in trans. We further show that Ocr physically associates with BrxX methyltransferase. Although BREX+ cells overproducing Ocr have partially methylated BREX sites, their viability is unaffected. The result suggests that, similar to its action against type I R-M systems, Ocr associates with as yet unidentified BREX system complexes containing BrxX and neutralizes their ability to both methylate and exclude incoming phage DNA.

## References

**1\. Goldfarb T, Sberro H, Weinstock E, Cohen O, Doron S, Charpak-Amikam Y, Afik S, Ofir G, Sorek R. BREX is a novel phage resistance system widespread in microbial genomes. EMBO J. 2015 Jan 13;34(2):169-83. doi: 10.15252/embj.201489455. Epub 2014 Dec 1. PMID: 25452498; PMCID: PMC4337064.**

**2\. Nunes-Alves C. Bacterial physiology: putting the 'BREX' on phage replication. Nat Rev Microbiol. 2015 Mar;13(3):129. doi: 10.1038/nrmicro3437. Epub 2015 Feb 2. PMID: 25639679.**

**3\. Sumby P, Smith MC. Phase variation in the phage growth limitation system of Streptomyces coelicolor A3(2). J Bacteriol. 2003;185(15):4558-4563. doi:10.1128/JB.185.15.4558-4563.2003**

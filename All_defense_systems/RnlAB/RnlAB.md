# RnlAB

## Example of genomic structure

The RnlAB system is composed of 2 proteins: RnlA and, RnlB.

Here is an example found in the RefSeq database: 

<img src="./data/RnlAB.svg">

RnlAB system in the genome of *Escherichia coli* (GCF\_014338505.1) is composed of 2 proteins: RnlA (WP\_000155570.1)and, RnlB (WP\_000461704.1).

## Distribution of the system among prokaryotes

The RnlAB system is present in a total of 71 different species.

Among the 22k complete genomes of RefSeq, this system is present in 284 genomes (1.2 %).

<img src="./data/Distribution_RnlAB.svg" width=800px>

*Proportion of genome encoding the RnlAB system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

RnlAB systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T4 (Koga et al., 2011)

## Relevant abstracts

**Koga, M., Otsuka, Y., Lemire, S. & Yonesaki, T. Escherichia coli rnlA and rnlB Compose a Novel Toxin-Antitoxin System. Genetics 187, 123-130 (2011).**
RNase LS was originally identified as a potential antagonist of bacteriophage T4 infection. When T4 dmd is defective, RNase LS activity rapidly increases after T4 infection and cleaves T4 mRNAs to antagonize T4 reproduction. Here we show that rnlA, a structural gene of RNase LS, encodes a novel toxin, and that rnlB (formally yfjO), located immediately downstream of rnlA, encodes an antitoxin against RnlA. Ectopic expression of RnlA caused inhibition of cell growth and rapid degradation of mRNAs in ?rnlAB cells. On the other hand, RnlB neutralized these RnlA effects. Furthermore, overexpression of RnlB in wild-type cells could completely suppress the growth defect of a T4 dmd mutant, that is, excess RnlB inhibited RNase LS activity. Pull-down analysis showed a specific interaction between RnlA and RnlB. Compared to RnlA, RnlB was extremely unstable, being degraded by ClpXP and Lon proteases, and this instability may increase RNase LS activity after T4 infection. All of these results suggested that rnlA-rnlB define a new toxin-antitoxin (TA) system.


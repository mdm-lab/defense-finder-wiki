# PD-T4-1

## Example of genomic structure

The PD-T4-1 system is composed of one protein: PD-T4-1.

Here is an example found in the RefSeq database: 

<img src="./data/PD-T4-1.svg">

PD-T4-1 system in the genome of *Bradyrhizobium diazoefficiens* (GCF\_016616885.1) is composed of 1 protein: PD-T4-1 (WP\_200471933.1).

## Distribution of the system among prokaryotes

The PD-T4-1 system is present in a total of 38 different species.

Among the 22k complete genomes of RefSeq, this system is present in 209 genomes (0.9 %).

<img src="./data/Distribution_PD-T4-1.svg" width=800px>

*Proportion of genome encoding the PD-T4-1 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

PD-T4-1 systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T2, T4, T6 (Vassallo et al., 2022)

## Relevant abstracts

**Johnson, C. M., Harden, M. M. & Grossman, A. D. Interactions between mobile genetic elements: An anti-phage gene in an integrative and conjugative element protects host cells from predation by a temperate bacteriophage. PLOS Genetics 18, e1010065 (2022).**
Most bacterial genomes contain horizontally acquired and transmissible mobile genetic elements, including temperate bacteriophages and integrative and conjugative elements. Little is known about how these elements interact and co-evolved as parts of their host genomes. In many cases, it is not known what advantages, if any, these elements provide to their bacterial hosts. Most strains of Bacillus subtilis contain the temperate phage SPß and the integrative and conjugative element ICEBs1. Here we show that the presence of ICEBs1 in cells protects populations of B. subtilis from predation by SPß, likely providing selective pressure for the maintenance of ICEBs1 in B. subtilis. A single gene in ICEBs1 (yddK, now called spbK for SPß killing) was both necessary and sufficient for this protection. spbK inhibited production of SPß, during both activation of a lysogen and following de novo infection. We found that expression spbK, together with the SPß gene yonE constitutes an abortive infection system that leads to cell death. spbK encodes a TIR (Toll-interleukin-1 receptor)-domain protein with similarity to some plant antiviral proteins and animal innate immune signaling proteins. We postulate that many uncharacterized cargo genes in ICEs may confer selective advantage to cells by protecting against other mobile elements.


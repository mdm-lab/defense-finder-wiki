# Rst_RT-nitrilase-Tm

## Example of genomic structure

The Rst_RT-nitrilase-Tm system is composed of 2 proteins: RT-Tm and, RT.

Here is an example found in the RefSeq database: 

<img src="./data/Rst_RT-Tm.svg">

Rst\_RT-Tm subsystem in the genome of *Morganella morganii* (GCF\_900478755.1) is composed of 2 proteins: RT (WP\_061057569.1)and, RT-Tm (WP\_004234654.1).

## Distribution of the system among prokaryotes

The Rst_RT-nitrilase-Tm system is present in a total of 5 different species.

Among the 22k complete genomes of RefSeq, this system is present in 25 genomes (0.1 %).

<img src="./data/Distribution_Rst_RT-nitrilase-Tm.svg" width=800px>

*Proportion of genome encoding the Rst_RT-nitrilase-Tm system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Rst_RT-nitrilase-Tm systems were experimentally validated using:

A system from *Escherichia coli (P4 loci)* in *Escherichia coli* has an anti-phage effect against Al505_P2 (Rousset et al., 2022)

## Relevant abstracts


# NixI

## Example of genomic structure

The NixI system is composed of 2 proteins: NixI and, Stix.

Here is an example found in the RefSeq database: 

<img src="./data/NixI.svg">

NixI system in the genome of *Vibrio cholerae* (GCF\_009646135.1) is composed of 2 proteins: NixI (WP\_001147214.1)and, Stix (WP\_000628297.1).

## Distribution of the system among prokaryotes

The NixI system is present in a total of 8 different species.

Among the 22k complete genomes of RefSeq, this system is present in 19 genomes (0.1 %).

<img src="./data/Distribution_NixI.svg" width=800px>

*Proportion of genome encoding the NixI system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

NixI systems were experimentally validated using:

A system from *Vibrio cholerae* in *Vibrio cholerae* has an anti-phage effect against ICP1 (Legault et al., 2022)

## Relevant abstracts

**LeGault, K. N., Barth, Z. K., DePaola, P. & Seed, K. D. A phage parasite deploys a nicking nuclease effector to inhibit replication of its viral host. 2021.07.12.452122 Preprint at https://doi.org/10.1101/2021.07.12.452122 (2021).**
PLEs are phage parasites integrated into the chromosome of epidemic Vibrio cholerae. In response to infection by its viral host ICP1, PLE excises, replicates and hijacks ICP1 structural components for transduction. Through an unknown mechanism PLE prevents ICP1 from transitioning to rolling circle replication (RCR), a prerequisite for efficient packaging of the viral genome. Here, we characterize a PLE-encoded nuclease, NixI, that blocks phage development likely by nicking ICP1s genome as it transitions to RCR. NixI-dependent cleavage sites appear in ICP1s genome during infection of PLE(+) V. cholerae. Purified NixI demonstrates in vitro specificity for sites in ICP1s genome and NixI activity is enhanced by a putative specificity determinant co-expressed with NixI during phage infection. Importantly, NixI is sufficient to limit ICP1 genome replication and eliminate progeny production. We identify distant NixI homologs in an expanded family of putative phage satellites in Vibrios that lack nucleotide homology to PLEs but nonetheless share genomic synteny with PLEs. More generally, our results reveal a previously unknown mechanism deployed by phage parasites to limit packaging of their viral hosts genome and highlight the prominent role of nuclease effectors as weapons in the arms race between antagonizing genomes.


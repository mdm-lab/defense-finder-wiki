# DarTG

## Example of genomic structure

The DarTG system is composed of 2 proteins: DarT and, DarG.

Here is an example found in the RefSeq database: 

<img src="./data/DarTG.svg">

DarTG system in the genome of *Mycobacterium tuberculosis* (GCF\_904810345.1) is composed of 2 proteins: DarT (WP\_003400548.1)and, DarG (WP\_003400551.1).

## Distribution of the system among prokaryotes

The DarTG system is present in a total of 356 different species.

Among the 22k complete genomes of RefSeq, this system is present in 955 genomes (4.2 %).

<img src="./data/Distribution_DarTG.svg" width=800px>

*Proportion of genome encoding the DarTG system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

DarTG systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against RB69, T5, SECphi18, Lust (Leroux et al., 2022)

## Relevant abstracts

**LeRoux, M. et al. The DarTG toxin-antitoxin system provides phage defence by ADP-ribosylating viral DNA. Nat Microbiol 7, 1028-1040 (2022).**
Toxin-antitoxin (TA) systems are broadly distributed, yet poorly conserved, genetic elements whose biological functions are unclear and controversial. Some TA systems may provide bacteria with immunity to infection by their ubiquitous viral predators, bacteriophages. To identify such TA systems, we searched bioinformatically for those frequently encoded near known phage defence genes in bacterial genomes. This search identified homologues of DarTG, a recently discovered family of TA systems whose biological functions and natural activating conditions were unclear. Representatives from two different subfamilies, DarTG1 and DarTG2, strongly protected E. coli MG1655 against different phages. We demonstrate that for each system, infection with either RB69 or T5 phage, respectively, triggers release of the DarT toxin, a DNA ADP-ribosyltransferase, that then modifies viral DNA and prevents replication, thereby blocking the production of mature virions. Further, we isolated phages that have evolved to overcome DarTG defence either through mutations to their DNA polymerase or to an anti-DarT factor, gp61.2, encoded by many T-even phages. Collectively, our results indicate that phage defence may be a common function for TA systems and reveal the mechanism by which DarTG systems inhibit phage infection.


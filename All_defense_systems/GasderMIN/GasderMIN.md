# GasderMIN

## Example of genomic structure

The GasderMIN system is composed of one protein: bGSDM.

Here is an example found in the RefSeq database: 

<img src="./data/GasderMIN.svg">

GasderMIN system in the genome of *Rhodoplanes sp.* (GCF\_001579845.1) is composed of 1 protein: bGSDM (WP\_068019379.1).

## Distribution of the system among prokaryotes

The GasderMIN system is present in a total of 25 different species.

Among the 22k complete genomes of RefSeq, this system is present in 29 genomes (0.1 %).

<img src="./data/Distribution_GasderMIN.svg" width=800px>

*Proportion of genome encoding the GasderMIN system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

GasderMIN systems were experimentally validated using:

A system from *Lysobacter enzymogenes* in *Escherichia coli* has an anti-phage effect against T5, T4, T6 (Johnson et al., 2022)

## Relevant abstracts

**Johnson, A. G. et al. Bacterial gasdermins reveal an ancient mechanism of cell death. Science 375, 221-225 (2022).**
Gasdermin proteins form large membrane pores in human cells that release immune cytokines and induce lytic cell death. Gasdermin pore formation is triggered by caspase-mediated cleavage during inflammasome signaling and is critical for defense against pathogens and cancer. We discovered gasdermin homologs encoded in bacteria that defended against phages and executed cell death. Structures of bacterial gasdermins revealed a conserved pore-forming domain that was stabilized in the inactive state with a buried lipid modification. Bacterial gasdermins were activated by dedicated caspase-like proteases that catalyzed site-specific cleavage and the removal of an inhibitory C-terminal peptide. Release of autoinhibition induced the assembly of large and heterogeneous pores that disrupted membrane integrity. Thus, pyroptosis is an ancient form of regulated cell death shared between bacteria and animals.


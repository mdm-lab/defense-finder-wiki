# PD-Lambda-6

## Example of genomic structure

The PD-Lambda-6 system is composed of one protein: PD-Lambda-6.

Here is an example found in the RefSeq database: 

<img src="./data/PD-Lambda-6.svg">

PD-Lambda-6 system in the genome of *Escherichia albertii* (GCF\_003864095.1) is composed of 1 protein: PD-Lambda-6 (WP\_125059614.1).

## Distribution of the system among prokaryotes

The PD-Lambda-6 system is present in a total of 12 different species.

Among the 22k complete genomes of RefSeq, this system is present in 21 genomes (0.1 %).

<img src="./data/Distribution_PD-Lambda-6.svg" width=800px>

*Proportion of genome encoding the PD-Lambda-6 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

PD-Lambda-6 systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against LambdaVir, T5 (Vassallo et al., 2022)

## Relevant abstracts

**Vassallo, C. N., Doering, C. R., Littlehale, M. L., Teodoro, G. I. C. & Laub, M. T. A functional selection reveals previously undetected anti-phage defence systems in the E. coli pangenome. Nat Microbiol 7, 1568-1579 (2022).**
The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.


# Shedu

## Example of genomic structure

The Shedu system is composed of one protein: SduA.

Here is an example found in the RefSeq database: 

<img src="./data/Shedu.svg">

Shedu system in the genome of *Mycolicibacterium psychrotolerans* (GCF\_010729305.1) is composed of 1 protein: SduA (WP\_246228780.1).

## Distribution of the system among prokaryotes

The Shedu system is present in a total of 483 different species.

Among the 22k complete genomes of RefSeq, this system is present in 899 genomes (3.9 %).

<img src="./data/Distribution_Shedu.svg" width=800px>

*Proportion of genome encoding the Shedu system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Shedu systems were experimentally validated using:

A system from *Bacillus cereus* in *Bacillus subtilis* has an anti-phage effect against phi105, rho14, SPP1, phi29 (Doron et al., 2018)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.


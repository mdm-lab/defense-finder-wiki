# AbiV

## Example of genomic structure

The AbiV system is composed of one protein: AbiV.

Here is an example found in the RefSeq database: 

<img src="./data/AbiV.svg">

AbiV system in the genome of *Lactococcus cremoris* (GCF\_017376415.1) is composed of 1 protein: AbiV (WP\_011834704.1).

## Distribution of the system among prokaryotes

The AbiV system is present in a total of 76 different species.

Among the 22k complete genomes of RefSeq, this system is present in 126 genomes (0.6 %).

<img src="./data/Distribution_AbiV.svg" width=800px>

*Proportion of genome encoding the AbiV system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

AbiV systems were experimentally validated using:

A system from *Lactococcus lactis* in *Lactococcus lactis* has an anti-phage effect against sk1, p2, jj50, P008, bIL170, c2, bIL67, ml3, eb1 (Haaber et al., 2008)

## Relevant abstracts

**Forde, A. & Fitzgerald, G. F. Bacteriophage defence systems in lactic acid bacteria. Antonie Van Leeuwenhoek 76, 89-113 (1999).**
The study of the interactions between lactic acid bacteria and their bacteriophages has been a vibrant and rewarding research activity for a considerable number of years. In the more recent past, the application of molecular genetics for the analysis of phage-host relationships has contributed enormously to the unravelling of specific events which dictate insensitivity to bacteriophage infection and has revealed that while they are complex and intricate in nature, they are also extremely effective. In addition, the strategy has laid solid foundations for the construction of phage resistant strains for use in commercial applications and has provided a sound basis for continued investigations into existing, naturally-derived and novel, genetically-engineered defence systems. Of course, it has also become clear that phage particles are highly dynamic in their response to those defence systems which they do encounter and that they can readily adapt to them as a consequence of their genetic flexibility and plasticity. This paper reviews the exciting developments that have been described in the literature regarding the study of phage-host interactions in lactic acid bacteria and the innovative approaches that can be taken to exploit this basic information for curtailing phage infection.

**Haaber, J., Moineau, S., Fortier, L.-C. & Hammer, K. AbiV, a Novel Antiphage Abortive Infection Mechanism on the Chromosome of Lactococcus lactis subsp. cremoris MG1363. Appl Environ Microbiol 74, 6528-6537 (2008).**
Insertional mutagenesis with pGhost9::ISS1 resulted in independent insertions in a 350-bp region of the chromosome of Lactococcus lactis subsp. cremoris MG1363 that conferred phage resistance to the integrants. The orientation and location of the insertions suggested that the phage resistance phenotype was caused by a chromosomal gene turned on by a promoter from the inserted construct. Reverse transcription-PCR analysis confirmed that there were higher levels of transcription of a downstream open reading frame (ORF) in the phage-resistant integrants than in the phage-sensitive strain L. lactis MG1363. This gene was also found to confer phage resistance to L. lactis MG1363 when it was cloned into an expression vector. A subsequent frameshift mutation in the ORF completely eliminated the phage resistance phenotype, confirming that the ORF was necessary for phage resistance. This ORF provided resistance against virulent lactococcal phages belonging to the 936 and c2 species with an efficiency of plaquing of 10?4, but it did not protect against members of the P335 species. A high level of expression of the ORF did not affect the cellular growth rate. Assays for phage adsorption, DNA ejection, restriction/modification activity, plaque size, phage DNA replication, and cell survival showed that the ORF encoded an abortive infection (Abi) mechanism. Sequence analysis revealed a deduced protein consisting of 201 amino acids which, in its native state, probably forms a dimer in the cytosol. Similarity searches revealed no homology to other phage resistance mechanisms, and thus, this novel Abi mechanism was designated AbiV. The mode of action of AbiV is unknown, but the activity of AbiV prevented cleavage of the replicated phage DNA of 936-like phages.


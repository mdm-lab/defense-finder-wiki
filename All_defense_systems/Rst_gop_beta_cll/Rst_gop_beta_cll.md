# Rst_gop_beta_cll

## Example of genomic structure

The Rst_gop_beta_cll system is composed of 3 proteins: gop, beta and, cll.

Here is an example found in the RefSeq database: 

<img src="./data/Rst_gop_beta_cll.svg">

Rst\_gop\_beta\_cll system in the genome of *Escherichia coli* (GCF\_003018615.1) is composed of 3 proteins: cll (WP\_001357997.1), beta (WP\_001357996.1)and, gop (WP\_000931915.1).

## Distribution of the system among prokaryotes

The Rst_gop_beta_cll system is present in a total of 14 different species.

Among the 22k complete genomes of RefSeq, this system is present in 37 genomes (0.2 %).

<img src="./data/Distribution_Rst_gop_beta_cll.svg" width=800px>

*Proportion of genome encoding the Rst_gop_beta_cll system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Rst_gop_beta_cll systems were experimentally validated using:

A system from *Enterobacteria phage P4* in *Escherichia coli* has an anti-phage effect against Lambda, P1 (Rousset et al., 2022)

## Relevant abstracts

**Rousset, F. et al. Phages and their satellites encode hotspots of antiviral systems. Cell Host & Microbe 30, 740-753.e5 (2022).**
Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.


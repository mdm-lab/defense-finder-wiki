# PD-T7-3

## Example of genomic structure

The PD-T7-3 system is composed of one protein: PD-T7-3.

Here is an example found in the RefSeq database: 

<img src="./data/PD-T7-3.svg">

PD-T7-3 system in the genome of *Alistipes sp.* (GCF\_009557455.1) is composed of 1 protein: PD-T7-3 (WP\_153498146.1).

## Distribution of the system among prokaryotes

The PD-T7-3 system is present in a total of 127 different species.

Among the 22k complete genomes of RefSeq, this system is present in 215 genomes (0.9 %).

<img src="./data/Distribution_PD-T7-3.svg" width=800px>

*Proportion of genome encoding the PD-T7-3 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

PD-T7-3 systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T2, T4, T6, T5, SECphi17, T3, T7 (Vassallo et al., 2022)

## Relevant abstracts

**Vassallo, C. N., Doering, C. R., Littlehale, M. L., Teodoro, G. I. C. & Laub, M. T. A functional selection reveals previously undetected anti-phage defence systems in the E. coli pangenome. Nat Microbiol 7, 1568-1579 (2022).**
The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.


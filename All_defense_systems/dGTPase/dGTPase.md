# dGTPase

## Example of genomic structure

The dGTPase system is composed of one protein: Sp_dGTPase.

Here is an example found in the RefSeq database: 

<img src="./data/dGTPase.svg">

dGTPase system in the genome of *Acinetobacter pittii* (GCF\_002012285.1) is composed of 1 protein: Sp\_dGTPase (WP\_213033921.1).

## Distribution of the system among prokaryotes

The dGTPase system is present in a total of 353 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1532 genomes (6.7 %).

<img src="./data/Distribution_dGTPase.svg" width=800px>

*Proportion of genome encoding the dGTPase system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

dGTPase systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T5, SECphi4, SECphi6, SECphi18, SECphi27, T7 (Tal et al., 2022)

A system from *Mesorhizobium ssp.* in *Escherichia coli* has an anti-phage effect against SECphi4, SECphi6, SECphi18, SECphi27, T7 (Tal et al., 2022)

A system from *Pseudoalteromonas luteoviolacea* in *Escherichia coli* has an anti-phage effect against T5, SECphi4, SECphi6, SECphi18, SECphi27, T2 (Tal et al., 2022)

A system from *Shewanella putrefaciens* in *Escherichia coli* has an anti-phage effect against T5, SECphi4, SECphi6, SECphi18, SECphi27, T2, T6, T7, SECphi17 (Tal et al., 2022)

## Relevant abstracts

**Hsueh, B. Y. et al. Phage defence by deaminase-mediated depletion of deoxynucleotides in bacteria. Nat Microbiol 7, 1210-1220 (2022).**
Vibrio cholerae biotype El Tor is perpetuating the longest cholera pandemic in recorded history. The genomic islands VSP-1 and VSP-2 distinguish El Tor from previous pandemic V. cholerae strains. Using a co-occurrence analysis of VSP genes in >200,000 bacterial genomes we built gene networks to infer biological functions encoded in these islands. This revealed that dncV, a component of the cyclic-oligonucleotide-based anti-phage signalling system (CBASS) anti-phage defence system, co-occurs with an uncharacterized gene vc0175 that we rename avcD for anti-viral cytodine deaminase. We show that AvcD is a deoxycytidylate deaminase and that its activity is post-translationally inhibited by a non-coding RNA named AvcI. AvcID and bacterial homologues protect bacterial populations against phage invasion by depleting free deoxycytidine nucleotides during infection, thereby decreasing phage replication. Homologues of avcD exist in all three domains of life, and bacterial AvcID defends against phage infection by combining traits of two eukaryotic innate viral immunity proteins, APOBEC and SAMHD1.


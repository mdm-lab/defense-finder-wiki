# Mokosh

## Example of genomic structure

The Mokosh system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Mokosh_TypeI.svg">

Mokosh\_TypeI subsystem in the genome of *Vibrio alginolyticus* (GCF\_022343125.1) is composed of 2 proteins: MkoB2 (WP\_238970063.1)and, MkoA2 (WP\_238970065.1).

<img src="./data/Mokosh_TypeII.svg">

Mokosh\_TypeII subsystem in the genome of *Shigella flexneri* (GCF\_022354205.1) is composed of 1 protein: MkoC (WP\_000344091.1).

## Distribution of the system among prokaryotes

The Mokosh system is present in a total of 605 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2540 genomes (11.1 %).

<img src="./data/Distribution_Mokosh.svg" width=800px>

*Proportion of genome encoding the Mokosh system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Mokosh systems were experimentally validated using:

Subsystem Type I with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T6, LambdaVir, T5, SECphi27 (Millman et al., 2022)

Subsystem Type II with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  SECphi17 (Millman et al., 2022)

## Relevant abstracts

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.


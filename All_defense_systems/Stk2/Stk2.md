# Stk2

## Description

Eukaryotic-like serine/threonine kinases have a variety of functions in prokaryotes. Recently, a single-gene system (Stk2) encoding for a Serine/threonine kinase from Staphylococcus epidermidis has been found to have anti-phage activity both in its native host and in a heterologous S.aureus host (Depardieu et al., 2016). 

## Molecular mechanism

Stk2 is an Abortive infection system, which triggers cell death upon phage infection, probably through phosphorylation of diverse essential cellular pathways (Depardieu et al., 2016). Stk2 was shown to detect a phage protein named Pack, which was proposed to be involved in phage genome packaging (Depardieu et al., 2016)


## Example of genomic structure

The Stk2 system is composed of one protein: Stk2.

Here is an example found in the RefSeq database: 

<img src="./data/Stk2.svg">

Stk2 system in the genome of *Staphylococcus aureus* (GCF\_009739755.1) is composed of 1 protein: Stk2 (WP\_001001347.1).

## Distribution of the system among prokaryotes

The Stk2 system is present in a total of 29 different species.

Among the 22k complete genomes of RefSeq, this system is present in 141 genomes (0.6 %).

<img src="./data/Distribution_Stk2.svg" width=800px>

*Proportion of genome encoding the Stk2 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Stk2 systems were experimentally validated using:

A system from *Staphylococcus epidermidis* in *Staphylococcus epidermidis* has an anti-phage effect against CNPx (Depardieu et al., 2016)

A system from *Staphylococcus epidermidis* in *Staphylococcus aureus* has an anti-phage effect against phage 80alpha, phage 85, phiNM1, phiNM2, phiNM4 (Depardieu et al., 2016)

## Relevant abstracts

**Depardieu, F. et al. A Eukaryotic-like Serine/Threonine Kinase Protects Staphylococci against Phages. Cell Host Microbe 20, 471-481 (2016).**
Organisms from all domains of life are infected by viruses. In eukaryotes, serine/threonine kinases play a central role in antiviral response. Bacteria, however, are not commonly known to use protein phosphorylation as part of their defense against phages. Here we identify Stk2, a staphylococcal serine/threonine kinase that provides efficient immunity against bacteriophages by inducing abortive infection. A phage protein of unknown function activates the Stk2 kinase. This leads to the Stk2-dependent phosphorylation of several proteins involved in translation, global transcription control, cell-cycle control, stress response, DNA topology, DNA repair, and central metabolism. Bacterial host cells die as a consequence of Stk2 activation, thereby preventing propagation of the phage to the rest of the bacterial population. Our work shows that mechanisms of viral defense that rely on protein phosphorylation constitute a conserved antiviral strategy across multiple domains of life.


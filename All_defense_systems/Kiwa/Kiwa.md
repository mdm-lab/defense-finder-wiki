# Kiwa

## Example of genomic structure

The Kiwa system is composed of 2 proteins: KwaA and, KwaB.

Here is an example found in the RefSeq database: 

<img src="./data/Kiwa.svg">

Kiwa system in the genome of *Aggregatibacter actinomycetemcomitans* (GCF\_001690155.1) is composed of 2 proteins: KwaB (WP\_005553122.1)and, KwaA (WP\_005540311.1).

## Distribution of the system among prokaryotes

The Kiwa system is present in a total of 355 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1104 genomes (4.8 %).

<img src="./data/Distribution_Kiwa.svg" width=800px>

*Proportion of genome encoding the Kiwa system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Kiwa systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against LambdaVir, SECphi18 (Doron et al., 2018)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.


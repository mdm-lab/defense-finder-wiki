# pAgo

## Example of genomic structure

The pAgo system is composed of one protein: pAgo_Short.

Here is an example found in the RefSeq database: 

<img src="./data/pAgo.svg">

pAgo system in the genome of *Ensifer adhaerens* (GCF\_020405145.1) is composed of 1 protein: pAgo\_LongB (WP\_218685258.1).

## Distribution of the system among prokaryotes

The pAgo system is present in a total of 435 different species.

Among the 22k complete genomes of RefSeq, this system is present in 598 genomes (2.6 %).

<img src="./data/Distribution_pAgo.svg" width=800px>

*Proportion of genome encoding the pAgo system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

pAgo systems were experimentally validated using:

Subsystem Ago with a system from *Clostridium butyricum* in *Escherichia coli*  has an anti-phage effect against  M13, P1vir (Kuzmenko et al., 2020)

A system from *Natronobacterium gregoryi* in *Escherichia coli* has an anti-phage effect against T7 (Xing et al., 2022)

Subsystem GsSir2/Ago with a system from *Geobacter sulfurreducens* in *Escherichia coli*  has an anti-phage effect against  LambdaVir, SECphi27 (Zaremba et al., 2022)

Subsystem GsSir2/Ago with a system from *Geobacter sulfurreducens* in *Escherichia coli*  has an anti-phage effect against  LambdaVir, SECphi27 (Zaremba et al., 2022)

Subsystem CcSir2/Ago with a system from *Caballeronia cordobensis* in *Escherichia coli*  has an anti-phage effect against  LambdaVir (Zaremba et al., 2022)

Subsystem PgSir2/Ago with a system from *araburkholderia graminis* in *Escherichia coli*  has an anti-phage effect against  LambdaVir, SECphi27 (Zaremba et al., 2022)

Subsystem Ago with a system from *Exiguobacterium marinum* in *Escherichia coli*  has an anti-phage effect against  P1vir (Lisitskaya et al., 2022)

Subsystem Sir2/Ago with a system from *Geobacter sulfurreducens* in *Escherichia coli*  has an anti-phage effect against  LambdaVir (Garb et al., 2022)

Subsystem SiAgo/Aga1/Aga2 with a system from *Sulfolobus islandicus* in *Sulfolobus islandicus*  has an anti-phage effect against  SMV1 (Zeng et al., 2021)

## Relevant abstracts

**Koopal, B. et al. Short prokaryotic Argonaute systems trigger cell death upon detection of invading DNA. Cell 185, 1471-1486.e19 (2022).**
Argonaute proteins use single-stranded RNA or DNA guides to target complementary nucleic acids. This allows eukaryotic Argonaute proteins to mediate RNA interference and long prokaryotic Argonaute proteins to interfere with invading nucleic acids. The function and mechanisms of the phylogenetically distinct short prokaryotic Argonaute proteins remain poorly understood. We demonstrate that short prokaryotic Argonaute and the associated TIR-APAZ (SPARTA) proteins form heterodimeric complexes. Upon guide RNA-mediated target DNA binding, four SPARTA heterodimers form oligomers in which TIR domain-mediated NAD(P)ase activity is unleashed. When expressed in Escherichia coli, SPARTA is activated in the presence of highly transcribed multicopy plasmid DNA, which causes cell death through NAD(P)+ depletion. This results in the removal of plasmid-invaded cells from bacterial cultures. Furthermore, we show that SPARTA can be repurposed for the programmable detection of DNA sequences. In conclusion, our work identifies SPARTA as a prokaryotic immune system that reduces cell viability upon RNA-guided detection of invading DNA.

**Kuzmenko, A. et al. DNA targeting and interference by a bacterial Argonaute nuclease. Nature 587, 632-637 (2020).**
Members of the conserved Argonaute protein family use small RNA guides to locate their mRNA targets and regulate gene expression and suppress mobile genetic elements in eukaryotes1,2. Argonautes are also present in many bacterial and archaeal species3-5. Unlike eukaryotic proteins, several prokaryotic Argonaute proteins use small DNA guides to cleave DNA, a process known as DNA interference6-10. However, the natural functions and targets of DNA interference are poorly understood, and the mechanisms of DNA guide generation and target discrimination remain unknown. Here we analyse the activity of a bacterial Argonaute nuclease from Clostridium butyricum (CbAgo) in vivo. We show that CbAgo targets multicopy genetic elements and suppresses the propagation of plasmids and infection by phages. CbAgo induces DNA interference between homologous sequences and triggers DNA degradation at double-strand breaks in the target DNA. The loading of CbAgo with locus-specific small DNA guides depends on both its intrinsic endonuclease activity and the cellular double-strand break repair machinery. A similar interaction was reported for the acquisition of new spacers during CRISPR adaptation, and prokaryotic genomes that encode Ago nucleases are enriched in CRISPR-Cas systems. These results identify molecular mechanisms that generate guides for DNA interference and suggest that the recognition of foreign nucleic acids by prokaryotic defence systems involves common principles.

**Makarova, K. S., Wolf, Y. I., van der Oost, J. & Koonin, E. V. Prokaryotic homologs of Argonaute proteins are predicted to function as key components of a novel system of defense against mobile genetic elements. Biol Direct 4, 29 (2009).**
BACKGROUND: In eukaryotes, RNA interference (RNAi) is a major mechanism of defense against viruses and transposable elements as well of regulating translation of endogenous mRNAs. The RNAi systems recognize the target RNA molecules via small guide RNAs that are completely or partially complementary to a region of the target. Key components of the RNAi systems are proteins of the Argonaute-PIWI family some of which function as slicers, the nucleases that cleave the target RNA that is base-paired to a guide RNA. Numerous prokaryotes possess the CRISPR-associated system (CASS) of defense against phages and plasmids that is, in part, mechanistically analogous but not homologous to eukaryotic RNAi systems. Many prokaryotes also encode homologs of Argonaute-PIWI proteins but their functions remain unknown. RESULTS: We present a detailed analysis of Argonaute-PIWI protein sequences and the genomic neighborhoods of the respective genes in prokaryotes. Whereas eukaryotic Ago/PIWI proteins always contain PAZ (oligonucleotide binding) and PIWI (active or inactivated nuclease) domains, the prokaryotic Argonaute homologs (pAgos) fall into two major groups in which the PAZ domain is either present or absent. The monophyly of each group is supported by a phylogenetic analysis of the conserved PIWI-domains. Almost all pAgos that lack a PAZ domain appear to be inactivated, and the respective genes are associated with a variety of predicted nucleases in putative operons. An additional, uncharacterized domain that is fused to various nucleases appears to be a unique signature of operons encoding the short (lacking PAZ) pAgo form. By contrast, almost all PAZ-domain containing pAgos are predicted to be active nucleases. Some proteins of this group (e.g., that from Aquifex aeolicus) have been experimentally shown to possess nuclease activity, and are not typically associated with genes for other (putative) nucleases. Given these observations, the apparent extensive horizontal transfer of pAgo genes, and their common, statistically significant over-representation in genomic neighborhoods enriched in genes encoding proteins involved in the defense against phages and/or plasmids, we hypothesize that pAgos are key components of a novel class of defense systems. The PAZ-domain containing pAgos are predicted to directly destroy virus or plasmid nucleic acids via their nuclease activity, whereas the apparently inactivated, PAZ-lacking pAgos could be structural subunits of protein complexes that contain, as active moieties, the putative nucleases that we predict to be co-expressed with these pAgos. All these nucleases are predicted to be DNA endonucleases, so it seems most probable that the putative novel phage/plasmid-defense system targets phage DNA rather than mRNAs. Given that in eukaryotic RNAi systems, the PAZ domain binds a guide RNA and positions it on the complementary region of the target, we further speculate that pAgos function on a similar principle (the guide being either DNA or RNA), and that the uncharacterized domain found in putative operons with the short forms of pAgos is a functional substitute for the PAZ domain. CONCLUSION: The hypothesis that pAgos are key components of a novel prokaryotic immune system that employs guide RNA or DNA molecules to degrade nucleic acids of invading mobile elements implies a functional analogy with the prokaryotic CASS and a direct evolutionary connection with eukaryotic RNAi. The predictions of the hypothesis including both the activities of pAgos and those of the associated endonucleases are readily amenable to experimental tests.

**Zeng, Z. et al. A short prokaryotic Argonaute activates membrane effector to confer antiviral defense. Cell Host Microbe 30, 930-943.e6 (2022).**
Argonaute (Ago) proteins are widespread nucleic-acid-guided enzymes that recognize targets through complementary base pairing. Although, in eukaryotes, Agos are involved in RNA silencing, the functions of prokaryotic Agos (pAgos) remain largely unknown. In particular, a clade of truncated and catalytically inactive pAgos (short pAgos) lacks characterization. Here, we reveal that a short pAgo protein in the archaeon Sulfolobus islandicus, together with its two genetically associated proteins, Aga1 and Aga2, provide robust antiviral protection via abortive infection. Aga2 is a toxic transmembrane effector that binds anionic phospholipids via a basic pocket, resulting in membrane depolarization and cell killing. Ago and Aga1 form a stable complex that exhibits nucleic-acid-directed nucleic-acid-recognition ability and directly interacts with Aga2, pointing to an immune sensing mechanism. Together, our results highlight the cooperation between pAgos and their widespread associated proteins, suggesting an uncharted diversity of pAgo-derived immune systems.

**.Garb, J. et al. Multiple phage resistance systems inhibit infection via SIR2-dependent NAD+ depletion. Nat Microbiol 7, 1849-1856 (2022).**
Defence-associated sirtuins (DSRs) comprise a family of proteins that defend bacteria from phage infection via an unknown mechanism. These proteins are common in bacteria and harbour an N-terminal sirtuin (SIR2) domain. In this study we report that DSR proteins degrade nicotinamide adenine dinucleotide (NAD+) during infection, depleting the cell of this essential molecule and aborting phage propagation. Our data show that one of these proteins, DSR2, directly identifies phage tail tube proteins and then becomes an active NADase in Bacillus subtilis. Using a phage mating methodology that promotes genetic exchange between pairs of DSR2-sensitive and DSR2-resistant phages, we further show that some phages express anti-DSR2 proteins that bind and repress DSR2. Finally, we demonstrate that the SIR2 domain serves as an effector NADase in a diverse set of phage defence systems outside the DSR family. Our results establish the general role of SIR2 domains in bacterial immunity against phages.

**Zaremba, M. et al. Short prokaryotic Argonautes provide defence against incoming mobile genetic elements through NAD+ depletion. Nat Microbiol 7, 1857-1869 (2022).**
Argonaute (Ago) proteins are found in all three domains of life. The so-called long Agos are composed of four major domains (N, PAZ, MID and PIWI) and contribute to RNA silencing in eukaryotes (eAgos) or defence against invading mobile genetic elements in prokaryotes (pAgos). The majority (~60%) of pAgos identified bioinformatically are shorter (comprising only MID and PIWI domains) and are typically associated with Sir2, Mrr or TIR domain-containing proteins. The cellular function and mechanism of short pAgos remain enigmatic. Here we show that Geobacter sulfurreducens short pAgo and the NAD+-bound Sir2 protein form a stable heterodimeric complex. The GsSir2/Ago complex presumably recognizes invading plasmid or phage DNA and activates the Sir2 subunit, which triggers endogenous NAD+ depletion and cell death, and prevents the propagation of invading DNA. We reconstituted NAD+ depletion activity in vitro and showed that activated GsSir2/Ago complex functions as a NADase that hydrolyses NAD+ to ADPR. Thus, short Sir2-associated pAgos provide defence against phages and plasmids, underscoring the diversity of mechanisms of prokaryotic Agos.


# Pycsar

## Example of genomic structure

The Pycsar system is composed of 2 proteins: AG_cyclase and, Effector_TIR.

Here is an example found in the RefSeq database: 

<img src="./data/Pycsar.svg">

Pycsar system in the genome of *Staphylococcus aureus* (GCF\_003186105.1) is composed of 2 proteins: AG\_cyclase (WP\_065316016.1)and, 2TM\_5 (WP\_000073144.1).

## Distribution of the system among prokaryotes

The Pycsar system is present in a total of 276 different species.

Among the 22k complete genomes of RefSeq, this system is present in 559 genomes (2.5 %).

<img src="./data/Distribution_Pycsar.svg" width=800px>

*Proportion of genome encoding the Pycsar system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Pycsar systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T5, P1, LambdaVir, SECphi27 (Tal et al., 2021)

A system from *Xanthomonas perforans* in *Escherichia coli* has an anti-phage effect against T7 (Tal et al., 2021)

## Relevant abstracts

**Tal, N. et al. Cyclic CMP and cyclic UMP mediate bacterial immunity against phages. Cell 184, 5728-5739.e16 (2021).**
The cyclic pyrimidines 3',5'-cyclic cytidine monophosphate (cCMP) and 3',5'-cyclic uridine monophosphate (cUMP) have been reported in multiple organisms and cell types. As opposed to the cyclic nucleotides 3',5'-cyclic adenosine monophosphate (cAMP) and 3',5'-cyclic guanosine monophosphate (cGMP), which are second messenger molecules with well-established regulatory roles across all domains of life, the biological role of cyclic pyrimidines has remained unclear. Here we report that cCMP and cUMP are second messengers functioning in bacterial immunity against viruses. We discovered a family of bacterial pyrimidine cyclase enzymes that specifically synthesize cCMP and cUMP following phage infection and demonstrate that these molecules activate immune effectors that execute an antiviral response. A crystal structure of a uridylate cyclase enzyme from this family explains the molecular mechanism of selectivity for pyrimidines as cyclization substrates. Defense systems encoding pyrimidine cyclases, denoted here Pycsar (pyrimidine cyclase system for antiphage resistance), are widespread in prokaryotes. Our results assign clear biological function to cCMP and cUMP as immunity signaling molecules in bacteria.


# Hachiman

## Description

Hachiman Type 1 systems were the first discovered and can be found in 3.4% of microbial genomes (1). Hachiman Type 1 systems are encoded by two genes, *hamA* (annotated as a Domain of Unknown Function, DUF) and *hamB* (annotated as a helicase) (1). 

More recently, Hachiman Type 2 systems were discovered and appeared to include a third gene, encoded for a DUF protein (HamC) (2).

## Example of genomic structure

The Hachiman type I system is composed of 2 proteins: HamB and, HamA.

Here is an example found in the RefSeq database: 

<img src="./data/Hachiman.svg">

Hachiman system in the genome of *Mesorhizobium terrae* (GCF\_008727715.1) is composed of 2 proteins: HamA\_1 (WP\_245317480.1)and, HamB (WP\_065997554.1).

## Distribution of the system among prokaryotes

The Hachiman system is present in a total of 518 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1361 genomes (6.0 %).

<img src="./data/Distribution_Hachiman.svg" width=800px>

*Proportion of genome encoding the Hachiman system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Hachiman systems were experimentally validated using:

A system from *Bacillus cereus* in *Bacillus subtilis* has an anti-phage effect against SBSphiJ, phi3T, SPbeta, SPR, phi105, rho14, phi29 (Doron et al., 2018)

Subsystem Hachiman Type II with a system from *Sphingopyxis witflariensis* in *Escherichia coli*  has an anti-phage effect against  T3, PVP-SE1 (Payne et al., 2021)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.

## References

1\. Doron S, Melamed S, Ofir G, et al. Systematic discovery of antiphage defense systems in the microbial pangenome. *Science*. 2018;359(6379):eaar4120. doi:10.1126/science.aar4120

2\. Payne LJ, Todeschini TC, Wu Y, Perry BJ, Ronson CW, Fineran PC, Nobrega FL, Jackson SA. Identification and classification of antiviral defence systems in bacteria and archaea with PADLOC reveals new system types. Nucleic Acids Res. 2021 Nov 8;49(19):10868-10878. doi: 10.1093/nar/gkab883. PMID: 34606606; PMCID: PMC8565338.

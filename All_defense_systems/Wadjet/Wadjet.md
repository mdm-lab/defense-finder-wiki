# Wadjet

## Example of genomic structure

The Wadjet system have been describe in a total of 4 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Wadjet_I.svg">

Wadjet\_I subsystem in the genome of *Bifidobacterium pseudocatenulatum* (GCF\_021484885.1) is composed of 4 proteins: JetA\_I (WP\_195524168.1), JetB\_I (WP\_195523897.1), JetC\_I (WP\_195523898.1)and, JetD\_I (WP\_229067172.1).

<img src="./data/Wadjet_II.svg">

Wadjet\_II subsystem in the genome of *Streptomyces sp.* (GCF\_023273835.1) is composed of 4 proteins: JetD\_II (WP\_248777007.1), JetC\_II (WP\_248777008.1), JetB\_II (WP\_248777009.1)and, JetA\_II (WP\_248777010.1).

<img src="./data/Wadjet_III.svg">

Wadjet\_III subsystem in the genome of *Caldibacillus thermoamylovorans* (GCF\_003096215.1) is composed of 4 proteins: JetD\_III (WP\_108897743.1), JetA\_III (WP\_108897744.1), JetB\_III (WP\_034768879.1)and, JetC\_III (WP\_108897745.1).

## Distribution of the system among prokaryotes

The Wadjet system is present in a total of 1151 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2380 genomes (10.4 %).

<img src="./data/Distribution_Wadjet.svg" width=800px>

*Proportion of genome encoding the Wadjet system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.


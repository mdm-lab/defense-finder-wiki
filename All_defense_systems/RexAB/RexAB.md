# RexAB

## Example of genomic structure

The RexAB system is composed of 2 proteins: RexA and, RexB.

Here is an example found in the RefSeq database: 

<img src="./data/RexAB.svg">

RexAB system in the genome of *Escherichia coli* (GCF\_008033315.1) is composed of 2 proteins: RexA (WP\_000788349.1)and, RexB (WP\_001245922.1).

## Distribution of the system among prokaryotes

The RexAB system is present in a total of 17 different species.

Among the 22k complete genomes of RefSeq, this system is present in 73 genomes (0.3 %).

<img src="./data/Distribution_RexAB.svg" width=800px>

*Proportion of genome encoding the RexAB system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

RexAB systems were experimentally validated using:

A system from *Escherichia coli lambda prophage* in *Escherichia coli* has an anti-phage effect against T4, Lamboid phages (Parma et al., 1992)

## Relevant abstracts

**Parma, D. H. et al. The Rex system of bacteriophage lambda: tolerance and altruistic cell death. Genes Dev 6, 497-510 (1992).**
The rexA and rexB genes of bacteriophage lambda encode a two-component system that aborts lytic growth of bacterial viruses. Rex exclusion is characterized by termination of macromolecular synthesis, loss of active transport, the hydrolysis of ATP, and cell death. By analogy to colicins E1 and K, these results can be explained by depolarization of the cytoplasmic membrane. We have fractionated cells to determine the intracellular location of the RexB protein and made RexB-alkaline phosphatase fusions to analyze its membrane topology. The RexB protein appears to be a polytopic transmembrane protein. We suggest that RexB proteins form ion channels that, in response to lytic growth of bacteriophages, depolarize the cytoplasmic membrane. The Rex system requires a mechanism to prevent lambda itself from being excluded during lytic growth. We have determined that overexpression of RexB in lambda lysogens prevents the exclusion of both T4 rII mutants and lambda ren mutants. We suspect that overexpression of RexB is the basis for preventing self-exclusion following the induction of a lambda lysogen and that RexB overexpression is accomplished through transcriptional regulation.


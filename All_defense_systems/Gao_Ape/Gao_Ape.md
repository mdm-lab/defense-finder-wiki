# Gao_Ape

## Example of genomic structure

The Gao_Ape system is composed of one protein: ApeA.

Here is an example found in the RefSeq database: 

<img src="./data/Gao_Ape.svg">

Gao\_Ape system in the genome of *Klebsiella sp.* (GCF\_018388785.1) is composed of 1 protein: ApeA (WP\_213292831.1).

## Distribution of the system among prokaryotes

The Gao_Ape system is present in a total of 76 different species.

Among the 22k complete genomes of RefSeq, this system is present in 199 genomes (0.9 %).

<img src="./data/Distribution_Gao_Ape.svg" width=800px>

*Proportion of genome encoding the Gao_Ape system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Gao_Ape systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T2, T4, T5, T3, T7, PhiV-1 (Gao et al., 2020)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.


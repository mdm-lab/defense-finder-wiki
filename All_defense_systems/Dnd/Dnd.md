# Dnd

## Example of genomic structure

The Dnd system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Dnd_ABCDE.svg">

Dnd\_ABCDE subsystem in the genome of *Vibrio tritonius* (GCF\_001547935.1) is composed of 6 proteins: DndA (WP\_068714508.1), DndB (WP\_068714510.1), DndC (WP\_068714512.1), DndD (WP\_068714514.1), DndE (WP\_068714516.1)and, DndD (WP\_068714526.1).

<img src="./data/Dnd_ABCDEFGH.svg">

Dnd\_ABCDEFGH subsystem in the genome of *Vibrio sp.* (GCF\_023716625.1) is composed of 8 proteins: DptF (WP\_252041715.1), DptG (WP\_252041716.1), DptH (WP\_252041717.1), DndE (WP\_252041720.1), DndD (WP\_252041722.1), DndC (WP\_252041723.1), DndB (WP\_252041724.1)and, DndA (WP\_252041725.1).

## Distribution of the system among prokaryotes

The Dnd system is present in a total of 218 different species.

Among the 22k complete genomes of RefSeq, this system is present in 388 genomes (1.7 %).

<img src="./data/Distribution_Dnd.svg" width=800px>

*Proportion of genome encoding the Dnd system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Dnd systems were experimentally validated using:

Subsystem DndCDEA-PbeABCD with a system from *Halalkalicoccus jeotgali* in *Natrinema sp. CJ7-F*  has an anti-phage effect against  SNJ1 (Xiong et al., 2019)

## Relevant abstracts

**Wang, L. et al. Phosphorothioation of DNA in bacteria by dnd genes. Nat Chem Biol 3, 709-710 (2007).**
Modifications of the canonical structures of DNA and RNA play critical roles in cell physiology, DNA replication, transcription and translation in all organisms. We now report that bacterial dnd gene clusters incorporate sulfur into the DNA backbone as a sequence-selective, stereospecific phosphorothioate modification. To our knowledge, unlike any other DNA or RNA modification systems, DNA phosphorothioation by dnd gene clusters is the first physiological modification described on the DNA backbone.

**Xiong, L. et al. A new type of DNA phosphorothioation-based antiviral system in archaea. Nat Commun 10, 1688 (2019).**
Archaea and Bacteria have evolved different defence strategies that target virtually all steps of the viral life cycle. The diversified virion morphotypes and genome contents of archaeal viruses result in a highly complex array of archaea-virus interactions. However, our understanding of archaeal antiviral activities lags far behind our knowledges of those in bacteria. Here we report a new archaeal defence system that involves DndCDEA-specific DNA phosphorothioate (PT) modification and the PbeABCD-mediated halt of virus propagation via inhibition of DNA replication. In contrast to the breakage of invasive DNA by DndFGH in bacteria, DndCDEA-PbeABCD does not degrade or cleave viral DNA. The PbeABCD-mediated PT defence system is widespread and exhibits extensive interdomain and intradomain gene transfer events. Our results suggest that DndCDEA-PbeABCD is a new type of PT-based virus resistance system, expanding the known arsenal of defence systems as well as our understanding of host-virus interactions.


# Septu

## Example of genomic structure

The Septu system is composed of 2 proteins: PtuA and, PtuB.

Here is an example found in the RefSeq database: 

<img src="./data/Septu.svg">

Septu system in the genome of *Arcobacter porcinus* (GCF\_004299785.2) is composed of 2 proteins: PtuA (WP\_066386194.1)and, PtuB\_2 (WP\_066386193.1).

## Distribution of the system among prokaryotes

The Septu system is present in a total of 911 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2193 genomes (9.6 %).

<img src="./data/Distribution_Septu.svg" width=800px>

*Proportion of genome encoding the Septu system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Septu systems were experimentally validated using:

A system from *Bacillus thuringiensis* in *Bacillus subtilis* has an anti-phage effect against SBSphiJ, SBSphiC (Doron et al., 2018)

A system from *Bacillus weihenstephanensis* in *Bacillus subtilis* has an anti-phage effect against SBSphiC, SpBeta (Doron et al., 2018)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.

**Millman, A. et al. Bacterial Retrons Function In Anti-Phage Defense. Cell 183, 1551-1561.e12 (2020).**
Retrons are bacterial genetic elements comprised of a reverse transcriptase (RT) and a non-coding RNA (ncRNA). The RT uses the ncRNA as template, generating a chimeric RNA/DNA molecule in which the RNA and DNA components are covalently linked. Although retrons were discovered three decades ago, their function remained unknown. We report that retrons function as anti-phage defense systems. The defensive unit is composed of three components: the RT, the ncRNA, and an effector protein. We examined multiple retron systems and show that they confer defense against a broad range of phages via abortive infection. Focusing on retron Ec48, we show evidence that it "guards" RecBCD, a complex with central anti-phage functions in bacteria. Inhibition of RecBCD by phage proteins activates the retron, leading to abortive infection and cell death. Thus, the Ec48 retron forms a second line of defense that is triggered if the first lines of defense have collapsed.

**Payne, L. J. et al. Identification and classification of antiviral defence systems in bacteria and archaea with PADLOC reveals new system types. Nucleic Acids Research 49, 10868-10878 (2021).**
To provide protection against viral infection and limit the uptake of mobile genetic elements, bacteria and archaea have evolved many diverse defence systems. The discovery and application of CRISPR-Cas adaptive immune systems has spurred recent interest in the identification and classification of new types of defence systems. Many new defence systems have recently been reported but there is a lack of accessible tools available to identify homologs of these systems in different genomes. Here, we report the Prokaryotic Antiviral Defence LOCator (PADLOC), a flexible and scalable open-source tool for defence system identification. With PADLOC, defence system genes are identified using HMM-based homologue searches, followed by validation of system completeness using gene presence/absence and synteny criteria specified by customisable system classifications. We show that PADLOC identifies defence systems with high accuracy and sensitivity. Our modular approach to organising the HMMs and system classifications allows additional defence systems to be easily integrated into the PADLOC database. To demonstrate application of PADLOC to biological questions, we used PADLOC to identify six new subtypes of known defence systems and a putative novel defence system comprised of a helicase, methylase and ATPase. PADLOC is available as a standalone package (https://github.com/padlocbio/padloc) and as a webserver (https://padloc.otago.ac.nz).


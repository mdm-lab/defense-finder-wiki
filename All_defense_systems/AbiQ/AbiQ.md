# AbiQ

## Example of genomic structure

The AbiQ system is composed of one protein: AbiQ.

Here is an example found in the RefSeq database: 

<img src="./data/AbiQ.svg">

AbiQ system in the genome of *Enterococcus sp.* (GCF\_003812305.1) is composed of 1 protein: AbiQ (WP\_123866849.1).

## Distribution of the system among prokaryotes

The AbiQ system is present in a total of 110 different species.

Among the 22k complete genomes of RefSeq, this system is present in 262 genomes (1.1 %).

<img src="./data/Distribution_AbiQ.svg" width=800px>

*Proportion of genome encoding the AbiQ system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

AbiQ systems were experimentally validated using:

A system from *lactococcal plasmid* in *lactococci* has an anti-phage effect against 936, c2 (Chopin et al., 2005)

## Relevant abstracts

**Chopin, M.-C., Chopin, A. & Bidnenko, E. Phage abortive infection in lactococci: variations on a theme. Curr Opin Microbiol 8, 473-479 (2005).**
Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.

**Emond, E. et al. AbiQ, an abortive infection mechanism from Lactococcus lactis. Appl Environ Microbiol 64, 4748-4756 (1998).**
Lactococcus lactis W-37 is highly resistant to phage infection. The cryptic plasmids from this strain were coelectroporated, along with the shuttle vector pSA3, into the plasmid-free host L. lactis LM0230. In addition to pSA3, erythromycin- and phage-resistant isolates carried pSRQ900, an 11-kb plasmid from L. lactis W-37. This plasmid made the host bacteria highly resistant (efficiency of plaquing <10(-8)) to c2- and 936-like phages. pSRQ900 did not confer any resistance to phages of the P335 species. Adsorption, cell survival, and endonucleolytic activity assays showed that pSRQ900 encodes an abortive infection mechanism. The phage resistance mechanism is limited to a 2.2-kb EcoRV/BclI fragment. Sequence analysis of this fragment revealed a complete open reading frame (abiQ), which encodes a putative protein of 183 amino acids. A frameshift mutation within abiQ completely abolished the resistant phenotype. The predicted peptide has a high content of positively charged residues (pI = 10.5) and is, in all likelihood, a cytosolic protein. AbiQ has no homology to known or deduced proteins in the databases. DNA replication assays showed that phage c21 (c2-like) and phage p2 (936-like) can still replicate in cells harboring AbiQ. However, phage DNA accumulated in its concatenated form in the infected AbiQ+ cells, whereas the AbiQ- cells contained processed (mature) phage DNA in addition to the concatenated form. The production of the major capsid protein of phage c21 was not hindered in the cells harboring AbiQ.

**Forde, A. & Fitzgerald, G. F. Bacteriophage defence systems in lactic acid bacteria. Antonie Van Leeuwenhoek 76, 89-113 (1999).**
The study of the interactions between lactic acid bacteria and their bacteriophages has been a vibrant and rewarding research activity for a considerable number of years. In the more recent past, the application of molecular genetics for the analysis of phage-host relationships has contributed enormously to the unravelling of specific events which dictate insensitivity to bacteriophage infection and has revealed that while they are complex and intricate in nature, they are also extremely effective. In addition, the strategy has laid solid foundations for the construction of phage resistant strains for use in commercial applications and has provided a sound basis for continued investigations into existing, naturally-derived and novel, genetically-engineered defence systems. Of course, it has also become clear that phage particles are highly dynamic in their response to those defence systems which they do encounter and that they can readily adapt to them as a consequence of their genetic flexibility and plasticity. This paper reviews the exciting developments that have been described in the literature regarding the study of phage-host interactions in lactic acid bacteria and the innovative approaches that can be taken to exploit this basic information for curtailing phage infection.


# Thoeris

## Example of genomic structure

The Thoeris system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Thoeris_I.svg">

Thoeris\_I subsystem in the genome of *Bacillus thuringiensis* (GCF\_020809205.1) is composed of 2 proteins: ThsA\_new\_grand (WP\_021728720.1)and, ThsB\_Global (WP\_021728719.1).

<img src="./data/Thoeris_II.svg">

Thoeris\_II subsystem in the genome of *Acinetobacter baumannii* (GCF\_014672775.1) is composed of 2 proteins: ThsB\_Global (WP\_000120680.1)and, ThsA\_new\_petit (WP\_005134880.1).

## Distribution of the system among prokaryotes

The Thoeris system is present in a total of 286 different species.

Among the 22k complete genomes of RefSeq, this system is present in 812 genomes (3.6 %).

<img src="./data/Distribution_Thoeris.svg" width=800px>

*Proportion of genome encoding the Thoeris system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Thoeris systems were experimentally validated using:

A system from *Bacillus amyloliquefaciens* in *Bacillus subtilis* has an anti-phage effect against SPO1, SBSphiJ, SBSphiC (Doron et al., 2018)

A system from *Bacillus cereus* in *Bacillus subtilis* has an anti-phage effect against phi29, SBSphiC, SPO1, SBSphiJ (Doron et al., 2018; Ofir et al., 2021)

A system from *Bacillus dafuensis* in *Bacillus subtilis* has an anti-phage effect against phi3T, SPBeta, SPR, SBSphi11, SBSphi13, phi29, SBSphiJ, SPO1 (Ofir et al., 2021)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.

**Ofir, G. et al. Antiviral activity of bacterial TIR domains via immune signalling molecules. Nature 600, 116-120 (2021).**
The Toll/interleukin-1 receptor (TIR) domain is a canonical component of animal and plant immune systems1,2. In plants, intracellular pathogen sensing by immune receptors triggers their TIR domains to generate a molecule that is a variant of cyclic ADP-ribose3,4. This molecule is hypothesized to mediate plant cell death through a pathway that has yet to be resolved5. TIR domains have also been shown to be involved in a bacterial anti-phage defence system called Thoeris6, but the mechanism of Thoeris defence remained unknown. Here we show that phage infection triggers Thoeris TIR-domain proteins to produce an isomer of cyclic ADP-ribose. This molecular signal activates a second protein, ThsA, which then depletes the cell of the essential molecule nicotinamide adenine dinucleotide (NAD) and leads to abortive infection and cell death. We also show that, similar to eukaryotic innate immune systems, bacterial TIR-domain proteins determine the immunological specificity to the invading pathogen. Our results describe an antiviral signalling pathway in bacteria, and suggest that the generation of intracellular signalling molecules is an ancient immunological function of TIR domains that is conserved in both plant and bacterial immunity.


# SpbK

## Example of genomic structure

The SpbK system is composed of one protein: SpbK.

Here is an example found in the RefSeq database: 

<img src="./data/SpbK.svg">

SpbK system in the genome of *Clostridium beijerinckii* (GCF\_900010805.1) is composed of 1 protein: SpbK (WP\_077841417.1).

## Distribution of the system among prokaryotes

The SpbK system is present in a total of 88 different species.

Among the 22k complete genomes of RefSeq, this system is present in 219 genomes (1.0 %).

<img src="./data/Distribution_SpbK.svg" width=800px>

*Proportion of genome encoding the SpbK system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

SpbK systems were experimentally validated using:

A system from *Bacillus subtilis* in *Bacillus subtilis* has an anti-phage effect against SPbeta (Johnson et al., 2022)

## Relevant abstracts

**Johnson, C. M., Harden, M. M. & Grossman, A. D. Interactions between mobile genetic elements: An anti-phage gene in an integrative and conjugative element protects host cells from predation by a temperate bacteriophage. PLOS Genetics 18, e1010065 (2022).**
Most bacterial genomes contain horizontally acquired and transmissible mobile genetic elements, including temperate bacteriophages and integrative and conjugative elements. Little is known about how these elements interact and co-evolved as parts of their host genomes. In many cases, it is not known what advantages, if any, these elements provide to their bacterial hosts. Most strains of Bacillus subtilis contain the temperate phage SPß and the integrative and conjugative element ICEBs1. Here we show that the presence of ICEBs1 in cells protects populations of B. subtilis from predation by SPß, likely providing selective pressure for the maintenance of ICEBs1 in B. subtilis. A single gene in ICEBs1 (yddK, now called spbK for SPß killing) was both necessary and sufficient for this protection. spbK inhibited production of SPß, during both activation of a lysogen and following de novo infection. We found that expression spbK, together with the SPß gene yonE constitutes an abortive infection system that leads to cell death. spbK encodes a TIR (Toll-interleukin-1 receptor)-domain protein with similarity to some plant antiviral proteins and animal innate immune signaling proteins. We postulate that many uncharacterized cargo genes in ICEs may confer selective advantage to cells by protecting against other mobile elements.


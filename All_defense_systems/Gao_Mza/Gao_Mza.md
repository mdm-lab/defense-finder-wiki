# Gao_Mza

## Example of genomic structure

The Gao_Mza system is composed of 5 proteins: MzaB, MzaC, MzaA, MzaD and, MzaE.

Here is an example found in the RefSeq database: 

<img src="./data/Gao_Mza.svg">

Gao\_Mza system in the genome of *Enterobacter roggenkampii* (GCF\_023023065.1) is composed of 5 proteins: MzaE (WP\_045418899.1), MzaD (WP\_045418897.1), MzaC (WP\_025912266.1), MzaB (WP\_045418895.1)and, MzaA (WP\_045418893.1).

## Distribution of the system among prokaryotes

The Gao_Mza system is present in a total of 57 different species.

Among the 22k complete genomes of RefSeq, this system is present in 99 genomes (0.4 %).

<img src="./data/Distribution_Gao_Mza.svg" width=800px>

*Proportion of genome encoding the Gao_Mza system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Gao_Mza systems were experimentally validated using:

A system from *Salmonella enterica* in *Escherichia coli* has an anti-phage effect against T2, T4, T5, Lambda, M13 (Gao et al., 2020)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.


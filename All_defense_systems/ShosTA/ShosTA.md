# ShosTA

## Example of genomic structure

The ShosTA system is composed of 2 proteins: ShosA and, ShosT.

Here is an example found in the RefSeq database: 

<img src="./data/ShosTA.svg">

ShosTA system in the genome of *Escherichia coli* (GCF\_011404895.1) is composed of 2 proteins: ShosA (WP\_001567470.1)and, ShosT (WP\_001567471.1).

## Distribution of the system among prokaryotes

The ShosTA system is present in a total of 299 different species.

Among the 22k complete genomes of RefSeq, this system is present in 668 genomes (2.9 %).

<img src="./data/Distribution_ShosTA.svg" width=800px>

*Proportion of genome encoding the ShosTA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

ShosTA systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against LambdaVir, SECphi4, SECphi6, SECphi18, T7 (Millman et al., 2022)

A system from *Escherichia coli (P2 loci)* in *Escherichia coli* has an anti-phage effect against Lambda, T7 (Rousset et al., 2022)

## Relevant abstracts

**Kimelman, A. et al. A vast collection of microbial genes that are toxic to bacteria. Genome research 22, 802-809 (2012).**
In the process of clone-based genome sequencing, initial assemblies frequently contain cloning gaps that can be resolved using cloning-independent methods, but the reason for their occurrence is largely unknown. By analyzing 9,328,693 sequencing clones from 393 microbial genomes, we systematically mapped more than 15,000 genes residing in cloning gaps and experimentally showed that their expression products are toxic to the Escherichia coli host. A subset of these toxic sequences was further evaluated through a series of functional assays exploring the mechanisms of their toxicity. Among these genes, our assays revealed novel toxins and restriction enzymes, and new classes of small, non-coding toxic RNAs that reproducibly inhibit E. coli growth. Further analyses also revealed abundant, short, toxic DNA fragments that were predicted to suppress E. coli growth by interacting with the replication initiator DnaA. Our results show that cloning gaps, once considered the result of technical problems, actually serve as a rich source for the discovery of biotechnologically valuable functions, and suggest new modes of antimicrobial interventions.

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.


# Shango


## Description
Shango is a three genes defense system which was discovered in parallel in two works in both *E.coli* and *P.aeruginosa* and was shown to have antiphage activity against the Lambda-phage in *E.coli* [1] and against diverse podo- and siphoviridae in *P.aeruginosa* [2].

Shango is composed of (i) a TerB-like domain, (ii) an Helicase and (iii) an ATPase. The TerB domain was previously shown to be associated to the perisplasmic membrane of bacteria [3]. 

## Molecular mechanism

The exact mechanism of action of the Shango defense has not yet been characterized, but it was shown that the TerB domain and the catalytic activity of the ATPase and the Helicase are required to provide antiviral defense. The fact that TerB domains are known to be associated to the periplasmic membrane could indicate that Shango might be involved in membrane surveillance [1].


## Example of genomic structure

The Shango system is composed of 3 proteins: SngC, SngB and, SngA.

Here is an example found in the RefSeq database: 

<img src="./data/Shango.svg">

Shango system in the genome of *Paenibacillus sp.* (GCF\_022637315.1) is composed of 3 proteins: SngA (WP\_241931534.1), SngB (WP\_241931535.1)and, SngC (WP\_241931536.1).

## Distribution of the system among prokaryotes

The Shango system is present in a total of 385 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1112 genomes (4.9 %).

<img src="./data/Distribution_Shango.svg" width=800px>

*Proportion of genome encoding the Shango system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Shango systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against LambdaVir, SECphi18 (Millman et al., 2022)

## Relevant abstracts

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.

## References
Shango was discovered in parallel by Adi Millman (Sorek group) and the team of J. Bondy-Denomy (UCSF). 

[1] Millman, A., Melamed, S., Leavitt, A., Doron, S., Bernheim, A., Hör, J., Garb, J., Bechon, N., Brandis, A., Lopatina, A., Ofir, G., Hochhauser, D., Stokar-Avihail, A., Tal, N., Sharir, S., Voichek, M., Erez, Z., Ferrer, J. L. M., Dar, D., … Sorek, R. (2022). An expanded arsenal of immune systems that protect bacteria from phages. _Cell Host & Microbe_, _30_(11), 1556-1569.e5. [https://doi.org/10.1016/j.chom.2022.09.017](https://doi.org/10.1016/j.chom.2022.09.017)

[2] Johnson, Matthew, Laderman, Eric, Huiting, Erin, Zhang, Charles, Davidson, Alan, & Bondy-Denomy, Joseph. (2022). _Core Defense Hotspots within Pseudomonas aeruginosa are a consistent and rich source of anti-phage defense systems_. [https://doi.org/10.5281/ZENODO.7254690](https://doi.org/10.5281/ZENODO.7254690)

[3] Alekhina, O., Valkovicova, L., & Turna, J. (2011). Study of membrane attachment and in vivo co-localization of TerB protein from uropathogenic Escherichia coli KL53. _General physiology and biophysics_, _30_(3), 286-292.
# SoFIC

## Example of genomic structure

The SoFIC system is composed of one protein: SoFic.

Here is an example found in the RefSeq database: 

<img src="./data/SoFic.svg">

SoFic subsystem in the genome of *Fusobacterium nucleatum* (GCF\_019552125.1) is composed of 1 protein: SoFic (WP\_220308819.1).

## Distribution of the system among prokaryotes

The SoFIC system is present in a total of 1014 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2265 genomes (9.9 %).

<img src="./data/Distribution_SoFIC.svg" width=800px>

*Proportion of genome encoding the SoFIC system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

SoFIC systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T5 (Millman et al., 2022)

## Relevant abstracts

**Millman, A. et al. An expanded arsenal of immune systems that protect bacteria from phages. Cell Host Microbe 30, 1556-1569.e5 (2022).**
Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.


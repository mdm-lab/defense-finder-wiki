# Rst_PARIS

## Description

PARIS (for Phage Anti-Restriction-Induced System) is a novel anti-phage system. PARIS is found in 4% of prokaryotic genomes. It comprises an ATPase associated with a DUF4435 protein, which can be found either as a two-gene cassette or a single-gene fusion (1).

This system relies on an unknown [Abortive infection](/general_concepts/Abi) mechanism to trigger growth arrest upon sensing a phage-encoded protein (Ocr). Interestingly, the Ocr protein has been found to inhibit R-M systems and BREX systems, making PARIS a suitable defense mechanism against RM resistant and/or BREX resistant phages (1, 2, 3). 

## Example of genomic structure

The Rst_PARIS system have been describe in a total of 4 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/PARIS_I.svg">

PARIS\_I subsystem in the genome of *Salmonella enterica* (GCF\_020715485.1) is composed of 2 proteins: AAA\_15 (WP\_001520831.1)and, DUF4435 (WP\_010989064.1).

<img src="./data/PARIS_II.svg">

PARIS\_II subsystem in the genome of *Enterobacter cloacae* (GCF\_023238665.1) is composed of 2 proteins: DUF4435 (WP\_071830092.1)and, AAA\_21 (WP\_061772587.1).

<img src="./data/PARIS_II_merge.svg">

PARIS\_II\_merge subsystem in the genome of *Desulfovibrio desulfuricans* (GCF\_017815575.1) is composed of 1 protein: AAA\_21\_DUF4435 (WP\_209818471.1).

<img src="./data/PARIS_I_merge.svg">

PARIS\_I\_merge subsystem in the genome of *Sideroxydans lithotrophicus* (GCF\_000025705.1) is composed of 1 protein: AAA\_15\_DUF4435 (WP\_013030315.1).

## Distribution of the system among prokaryotes

The Rst_PARIS system is present in a total of 463 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1145 genomes (5.0 %).

<img src="./data/Distribution_Rst_PARIS.svg" width=800px>

*Proportion of genome encoding the Rst_PARIS system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Rst_PARIS systems were experimentally validated using:

Subsystem Paris 1 with a system from *Escherichia coli (P4 loci)* in *Escherichia coli*  has an anti-phage effect against  Lambda, T4, CLB_P2, LF82_P8, Al505_P2, T7 (Rousset et al., 2022)

Subsystem Paris 2 with a system from *Escherichia coli (P4 loci)* in *Escherichia coli*  has an anti-phage effect against  Lambda, T4, CLB_P2, LF82_P8, T7 (Rousset et al., 2022)

## Relevant abstracts

**Rousset, F. et al. Phages and their satellites encode hotspots of antiviral systems. Cell Host & Microbe 30, 740-753.e5 (2022).**
Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.


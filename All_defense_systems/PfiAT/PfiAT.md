# PfiAT

## Example of genomic structure

The PfiAT system is composed of 2 proteins: PfiA and, PfiT.

Here is an example found in the RefSeq database: 

<img src="./data/PfiAT.svg">

PfiAT system in the genome of *Pseudomonas amygdali* (GCF\_023207855.1) is composed of 2 proteins: PfiT (WP\_096134620.1)and, PfiA (WP\_057431469.1).

## Distribution of the system among prokaryotes

The PfiAT system is present in a total of 261 different species.

Among the 22k complete genomes of RefSeq, this system is present in 819 genomes (3.6 %).

<img src="./data/Distribution_PfiAT.svg" width=800px>

*Proportion of genome encoding the PfiAT system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Relevant abstracts

**Li, Y. et al. Prophage encoding toxin/antitoxin system PfiT/PfiA inhibits Pf4 production in Pseudomonas aeruginosa. Microb Biotechnol 13, 1132-1144 (2020).**
Pf prophages are ssDNA filamentous prophages that are prevalent among various Pseudomonas aeruginosa strains. The genomes of Pf prophages contain not only core genes encoding functions involved in phage replication, structure and assembly but also accessory genes. By studying the accessory genes in the Pf4 prophage in P. aeruginosa PAO1, we provided experimental evidence to demonstrate that PA0729 and the upstream ORF Rorf0727 near the right attachment site of Pf4 form a type II toxin/antitoxin (TA) pair. Importantly, we found that the deletion of the toxin gene PA0729 greatly increased Pf4 phage production. We thus suggest the toxin PA0729 be named PfiT for Pf4 inhibition toxin and Rorf0727 be named PfiA for PfiT antitoxin. The PfiT toxin directly binds to PfiA and functions as a corepressor of PfiA for the TA operon. The PfiAT complex exhibited autoregulation by binding to a palindrome (5'-AATTCN5 GTTAA-3') overlapping the -35 region of the TA operon. The deletion of pfiT disrupted TA autoregulation and activated pfiA expression. Additionally, the deletion of pfiT also activated the expression of the replication initiation factor gene PA0727. Moreover, the Pf4 phage released from the pfiT deletion mutant overcame the immunity provided by the phage repressor Pf4r. Therefore, this study reveals that the TA systems in Pf prophages can regulate phage production and phage immunity, providing new insights into the function of TAs in mobile genetic elements.


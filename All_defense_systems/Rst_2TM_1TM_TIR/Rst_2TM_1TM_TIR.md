# Rst_2TM_1TM_TIR

## Example of genomic structure

The Rst_2TM_1TM_TIR system is composed of 3 proteins: Rst_TIR_tm, Rst_1TM_TIR and, Rst_2TM_TIR.

Here is an example found in the RefSeq database: 

<img src="./data/Rst_2TM_1TM_TIR.svg">

Rst\_2TM\_1TM\_TIR system in the genome of *Escherichia coli* (GCF\_001900375.1) is composed of 3 proteins: Rst\_TIR\_tm (WP\_023140578.1), Rst\_1TM\_TIR (WP\_001534953.1)and, Rst\_2TM\_TIR (WP\_023140577.1).

## Distribution of the system among prokaryotes

The Rst_2TM_1TM_TIR system is present in a total of 1 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2 genomes (0.0 %).

<img src="./data/Distribution_Rst_2TM_1TM_TIR.svg" width=800px>

*Proportion of genome encoding the Rst_2TM_1TM_TIR system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Relevant abstracts

**Rousset, F. et al. Phages and their satellites encode hotspots of antiviral systems. Cell Host & Microbe 30, 740-753.e5 (2022).**
Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.


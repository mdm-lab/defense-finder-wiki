# AbiP2

## Example of genomic structure

The AbiP2 system is composed of one protein: AbiP2.

Here is an example found in the RefSeq database: 

<img src="./data/AbiP2.svg">

AbiP2 system in the genome of *Casimicrobium huifangae* (GCF\_009746125.1) is composed of 1 protein: AbiP2 (WP\_156862066.1).

## Distribution of the system among prokaryotes

The AbiP2 system is present in a total of 98 different species.

Among the 22k complete genomes of RefSeq, this system is present in 299 genomes (1.3 %).

<img src="./data/Distribution_AbiP2.svg" width=800px>

*Proportion of genome encoding the AbiP2 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

AbiP2 systems were experimentally validated using:

Subsystem RT-Abi-P2 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T5 (Gao et al., 2020)

A system from *lactococcal plasmid* in *lactococci* has an anti-phage effect against 936 (Chopin et al., 2005)

## Relevant abstracts

**Chopin, M.-C., Chopin, A. & Bidnenko, E. Phage abortive infection in lactococci: variations on a theme. Curr Opin Microbiol 8, 473-479 (2005).**
Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.

**Forde, A. & Fitzgerald, G. F. Bacteriophage defence systems in lactic acid bacteria. Antonie Van Leeuwenhoek 76, 89-113 (1999).**
The study of the interactions between lactic acid bacteria and their bacteriophages has been a vibrant and rewarding research activity for a considerable number of years. In the more recent past, the application of molecular genetics for the analysis of phage-host relationships has contributed enormously to the unravelling of specific events which dictate insensitivity to bacteriophage infection and has revealed that while they are complex and intricate in nature, they are also extremely effective. In addition, the strategy has laid solid foundations for the construction of phage resistant strains for use in commercial applications and has provided a sound basis for continued investigations into existing, naturally-derived and novel, genetically-engineered defence systems. Of course, it has also become clear that phage particles are highly dynamic in their response to those defence systems which they do encounter and that they can readily adapt to them as a consequence of their genetic flexibility and plasticity. This paper reviews the exciting developments that have been described in the literature regarding the study of phage-host interactions in lactic acid bacteria and the innovative approaches that can be taken to exploit this basic information for curtailing phage infection.

**Mestre, M. R. et al. UG/Abi: a highly diverse family of prokaryotic reverse transcriptases associated with defense functions. Nucleic Acids Research 50, 6084-6101 (2022).**
Reverse transcriptases (RTs) are enzymes capable of synthesizing DNA using RNA as a template. Within the last few years, a burst of research has led to the discovery of novel prokaryotic RTs with diverse antiviral properties, such as DRTs (Defense-associated RTs), which belong to the so-called group of unknown RTs (UG) and are closely related to the Abortive Infection system (Abi) RTs. In this work, we performed a systematic analysis of UG and Abi RTs, increasing the number of UG/Abi members up to 42 highly diverse groups, most of which are predicted to be functionally associated with other gene(s) or domain(s). Based on this information, we classified these systems into three major classes. In addition, we reveal that most of these groups are associated with defense functions and/or mobile genetic elements, and demonstrate the antiphage role of four novel groups. Besides, we highlight the presence of one of these systems in novel families of human gut viruses infecting members of the Bacteroidetes and Firmicutes phyla. This work lays the foundation for a comprehensive and unified understanding of these highly diverse RTs with enormous biotechnological potential.


# SanaTA

## Example of genomic structure

The SanaTA system is composed of 2 proteins: SanaT and, SanaA.

Here is an example found in the RefSeq database: 

<img src="./data/SanaTA.svg">

SanaTA system in the genome of *Methylovorus glucosetrophus* (GCF\_000023745.1) is composed of 2 proteins: SanaT (WP\_015830669.1)and, SanaA (WP\_015830670.1).

## Distribution of the system among prokaryotes

The SanaTA system is present in a total of 504 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1071 genomes (4.7 %).

<img src="./data/Distribution_SanaTA.svg" width=800px>

*Proportion of genome encoding the SanaTA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

SanaTA systems were experimentally validated using:

A system from *Shewanella sp. ANA-3* in *Escherichia coli* has an anti-phage effect against T7 (Sberro et al., 2013)

## Relevant abstracts

**Sberro, H. et al. Discovery of functional toxin/antitoxin systems in bacteria by shotgun cloning. Mol Cell 50, 136-148 (2013).**
Toxin-antitoxin (TA) modules, composed of a toxic protein and a counteracting antitoxin, play important roles in bacterial physiology. We examined the experimental insertion of 1.5 million genes from 388 microbial genomes into an Escherichia coli host using more than 8.5 million random clones. This revealed hundreds of genes (toxins) that could only be cloned when the neighboring gene (antitoxin) was present on the same clone. Clustering of these genes revealed TA families widespread in bacterial genomes, some of which deviate from the classical characteristics previously described for such modules. Introduction of these genes into E. coli validated that the toxin toxicity is mitigated by the antitoxin. Infection experiments with T7 phage showed that two of the new modules can provide resistance against phage. Moreover, our experiments revealed an "antidefense" protein in phage T7 that neutralizes phage resistance. Our results expose active fronts in the arms race between bacteria and phage.


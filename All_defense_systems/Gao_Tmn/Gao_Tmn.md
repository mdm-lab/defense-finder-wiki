# Gao_Tmn

## Example of genomic structure

The Gao_Tmn system is composed of one protein: TmnA.

Here is an example found in the RefSeq database: 

<img src="./data/Gao_Tmn.svg">

Gao\_Tmn system in the genome of *Salmonella enterica* (GCF\_006384195.1) is composed of 1 protein: TmnA (WP\_130525902.1).

## Distribution of the system among prokaryotes

The Gao_Tmn system is present in a total of 82 different species.

Among the 22k complete genomes of RefSeq, this system is present in 414 genomes (1.8 %).

<img src="./data/Distribution_Gao_Tmn.svg" width=800px>

*Proportion of genome encoding the Gao_Tmn system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Gao_Tmn systems were experimentally validated using:

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T2, P1, PhiV-1, PhiX (Gao et al., 2020)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.


# Retrons

## Description

Retrons are genetic elements constituted of a non-coding RNA (ncRNA) associated with a reverse-transcriptase (RT). The RT reverse-transcribes part of the ncRNA to generate an RNA-DNA hybrid molecule. Although the existence of retrons have been known for decades, their biological functions were unknown. Recent studies revealed that most retrons could in fact be anti-phage systems (1,2). 

<img src="./data/Retron_mestre_et_al_fig_1.jpg" width="400px">

_Fig 1. (Mestre et al., 2020) Structure and organisation of a retron_ . The two non-coding contiguous inverted sequences (named msr and msd) are transcribed as a single RNA. The RT recognizes its specific structure and reverse-transcribes it, generating an RNA-DNA hybrid


The majority of retrons are encoded on a gene cassette that encodes the retron and one or two additional proteins, which act as the retrons effectors. Bioinformatic prediction reveals that these effectors are very diverse and include transmembrane proteins, proteases, Cold-shock proteins, TIR domains proteins, ATPase, endonucleases, etc. Interestingly, several of these effector domains have already been described in other defense systems, including CBASS and Septu. Most retrons appear to act through an Abortive infection strategy (1).

## Molecular mechanisms

The *E.coli* retron system Ec48 mediates growth arrest upon sensing the inactivation of the bacterial RecBCD complex, a key element of the bacterial DNA repair system and immunity (1). Another study demonstrates that several retrons are part of Toxin-Antitoxin systems, where the RT-msDNA complex acts as an antitoxin that binds to and inhibits its cognate toxin. The tempering of the RT-msDNA, possibly by phage-encoded anti-RM systems, abolishes the antitoxin properties of the retron element, resulting in cell death mediated by the toxin activity (2). 


## Example of genomic structure

The Retron system have been describe in a total of 16 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Retron_II.svg">

Retron\_II subsystem in the genome of *Klebsiella pneumoniae* (GCF\_904866255.1) is composed of 2 proteins: NDT2 (WP\_057222224.1)and, RT\_Tot (WP\_048289034.1).

<img src="./data/Retron_IV.svg">

Retron\_IV subsystem in the genome of *Aliivibrio fischeri* (GCF\_000011805.1) is composed of 2 proteins: RT\_Tot (WP\_011261677.1)and, 2TM (WP\_236727775.1).

<img src="./data/Retron_I_A.svg">

Retron\_I\_A subsystem in the genome of *Vibrio harveyi* (GCF\_009184745.1) is composed of 3 proteins: RT\_Tot (WP\_152163686.1), ATPase\_TypeIA (WP\_152163687.1)and, HNH\_TIGR02646 (WP\_152163688.1).

<img src="./data/Retron_I_B.svg">

Retron\_I\_B subsystem in the genome of *Vibrio vulnificus* (GCF\_009665475.1) is composed of 2 proteins: ATPase\_TOPRIM\_COG3593 (WP\_103277404.1)and, RT\_Tot (WP\_043877188.1).

<img src="./data/Retron_I_C.svg">

Retron\_I\_C subsystem in the genome of *Listeria monocytogenes* (GCF\_905219385.1) is composed of 1 protein: RT\_1\_C2 (WP\_003726410.1).

<img src="./data/Retron_V.svg">

Retron\_V subsystem in the genome of *Proteus terrae* (GCF\_013171285.1) is composed of 2 proteins: CSD (WP\_004244726.1)and, RT\_Tot (WP\_109418979.1).

<img src="./data/Retron_VI.svg">

Retron\_VI subsystem in the genome of *Pseudomonas eucalypticola* (GCF\_013374995.1) is composed of 2 proteins: HTH (WP\_245217789.1)and, RT\_Tot (WP\_176571652.1).

<img src="./data/Retron_VII_1.svg">

Retron\_VII\_1 subsystem in the genome of *Pseudoxanthomonas mexicana* (GCF\_014397415.1) is composed of 1 protein: RT\_7\_A1 (WP\_187572543.1).

<img src="./data/Retron_VII_2.svg">

Retron\_VII\_2 subsystem in the genome of *Bacillus mycoides* (GCF\_018742105.1) is composed of 2 proteins: DUF3800 (WP\_215564565.1)and, RT\_Tot (WP\_215564572.1).

<img src="./data/Retron_XI.svg">

Retron\_XI subsystem in the genome of *Planococcus kocurii* (GCF\_001465835.2) is composed of 1 protein: RT\_11 (WP\_058386256.1).

<img src="./data/Retron_XII.svg">

Retron\_XII subsystem in the genome of *Stenotrophomonas acidaminiphila* (GCF\_014109845.1) is composed of 1 protein: RT\_12 (WP\_182333825.1).

<img src="./data/Retron_XIII.svg">

Retron\_XIII subsystem in the genome of *Delftia acidovorans* (GCF\_016026535.1) is composed of 3 proteins: ARM (WP\_197944577.1), WHSWIM (WP\_197944578.1)and, RT\_Tot (WP\_065344905.1).

## Distribution of the system among prokaryotes

The Retron system is present in a total of 731 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2601 genomes (11.4 %).

<img src="./data/Distribution_Retron.svg" width=800px>

*Proportion of genome encoding the Retron system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Retron systems were experimentally validated using:

Subsystem SLATT + RT_G2_intron with a system from *Klebsiella pneumoniae's PICI (KpCIUCICRE 8)* in *Escherichia coli*  has an anti-phage effect against  T5, HK97, HK544, HK578, T7 (Fillol-Salom et al., 2022)

Subsystem SLATT + RT_G2_intron with a system from *Klebsiella pneumoniae's PICI (KpCIUCICRE 8)* in *Samonella enterica*  has an anti-phage effect against  P22, BTP1, ES18 (Fillol-Salom et al., 2022)

Subsystem SLATT + RT_G2_intron with a system from *Klebsiella pneumoniae's PICI (KpCIUCICRE 8)* in *Klebsiella pneumoniae*  has an anti-phage effect against  Pokey, Raw, Eggy, KaID (Fillol-Salom et al., 2022)

Subsystem RT Ec67 + TOPRIM with a system from *Klebsiella pneumoniae's PICI (KpCIB28906)* in *Escherichia coli*  has an anti-phage effect against  T4, T5, HK578, T7 (Fillol-Salom et al., 2022)

Subsystem RT Ec67 + TOPRIM with a system from *Klebsiella pneumoniae's PICI (KpCIB28906)* in *Samonella enterica*  has an anti-phage effect against  det7 (Fillol-Salom et al., 2022)

Subsystem RT Ec67 + TOPRIM with a system from *Klebsiella pneumoniae's PICI (KpCIB28906)* in *Samonella enterica*  has an anti-phage effect against  Pokey, KalD (Fillol-Salom et al., 2022)

Subsystem Retron-TIR with a system from *Shigella dysenteriae* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T3, T7, PhiV-1 (Gao et al., 2020)

Subsystem Retron Ec67 + TOPRIM with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T5 (Gao et al., 2020)

Subsystem Retron Ec86 + Nuc_deoxy with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T4 (Gao et al., 2020)

Subsystem Retron Ec78 + ATPase + HNH with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T5 (Gao et al., 2020)

Subsystem Ec73 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  SECphi4, SECphi6, SECphi27, P1, T7 (Millman et al., 2020)

Subsystem Ec86 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T5 (Millman et al., 2020)

Subsystem Ec48 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  Lambda-Vir, T5, T2, T4, T7 (Millman et al., 2020)

Subsystem Ec67 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T5 (Millman et al., 2020)

Subsystem Se72 with a system from *Salmonella enterica* in *Escherichia coli*  has an anti-phage effect against  Lambda-Vir (Millman et al., 2020)

Subsystem Ec78 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T5 (Millman et al., 2020)

Subsystem Ec83 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T6 (Millman et al., 2020)

Subsystem Vc95 with a system from *Vibrio cholerae* in *Escherichia coli*  has an anti-phage effect against  T2, T4, T6 (Millman et al., 2020)

Subsystem Retron-Eco8 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  SECphi4, SECphi6, SECphi18, T4, T6, T7 (Millman et al., 2020)

Subsystem Retron-Sen2 with a system from *Salmonella enterica serovar Typhimurium* in *Escherichia coli*  has an anti-phage effect against  T5 (Bobonis et al., 2022)

Subsystem Retron-Eco9 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  P1vir, T2, T3, T5, T7, Ffm, Br60 (Bobonis et al., 2022)

Subsystem Retron-Eco1 with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T5 (Bobonis et al., 2022)

## Relevant abstracts

**Mestre, M. R., González-Delgado, A., Gutiérrez-Rus, L. I., Martínez-Abarca, F. & Toro, N. Systematic prediction of genes functionally associated with bacterial retrons and classification of the encoded tripartite systems. Nucleic Acids Research 48, 12632-12647 (2020).**
Bacterial retrons consist of a reverse transcriptase (RT) and a contiguous non-coding RNA (ncRNA) gene. One third of annotated retrons carry additional open reading frames (ORFs), the contribution and significance of which in retron biology remains to be determined. In this study we developed a computational pipeline for the systematic prediction of genes specifically associated with retron RTs based on a previously reported large dataset representative of the diversity of prokaryotic RTs. We found that retrons generally comprise a tripartite system composed of the ncRNA, the RT and an additional protein or RT-fused domain with diverse enzymatic functions. These retron systems are highly modular, and their components have coevolved to different extents. Based on the additional module, we classified retrons into 13 types, some of which include additional variants. Our findings provide a basis for future studies on the biological function of retrons and for expanding their biotechnological applications.

**Millman, A. et al. Bacterial Retrons Function In Anti-Phage Defense. Cell 183, 1551-1561.e12 (2020).**
Retrons are bacterial genetic elements comprised of a reverse transcriptase (RT) and a non-coding RNA (ncRNA). The RT uses the ncRNA as template, generating a chimeric RNA/DNA molecule in which the RNA and DNA components are covalently linked. Although retrons were discovered three decades ago, their function remained unknown. We report that retrons function as anti-phage defense systems. The defensive unit is composed of three components: the RT, the ncRNA, and an effector protein. We examined multiple retron systems and show that they confer defense against a broad range of phages via abortive infection. Focusing on retron Ec48, we show evidence that it "guards" RecBCD, a complex with central anti-phage functions in bacteria. Inhibition of RecBCD by phage proteins activates the retron, leading to abortive infection and cell death. Thus, the Ec48 retron forms a second line of defense that is triggered if the first lines of defense have collapsed.


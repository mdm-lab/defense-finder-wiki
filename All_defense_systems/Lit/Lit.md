# Lit

## Example of genomic structure

The Lit system is composed of one protein: Lit.

Here is an example found in the RefSeq database: 

<img src="./data/Lit.svg">

Lit system in the genome of *Stenotrophomonas maltophilia* (GCF\_012647025.1) is composed of 1 protein: Lit (WP\_061201506.1).

## Distribution of the system among prokaryotes

The Lit system is present in a total of 193 different species.

Among the 22k complete genomes of RefSeq, this system is present in 455 genomes (2.0 %).

<img src="./data/Distribution_Lit.svg" width=800px>

*Proportion of genome encoding the Lit system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

Lit systems were experimentally validated using:

A system from *Escherichia coli defective prophage e14* in *Escherichia coli* has an anti-phage effect against T4 (Yu and Snyder, 1994)

## Relevant abstracts

**Bingham, R., Ekunwe, S. I., Falk, S., Snyder, L. & Kleanthous, C. The major head protein of bacteriophage T4 binds specifically to elongation factor Tu. J Biol Chem 275, 23219-23226 (2000).**
The Lit protease in Escherichia coli K-12 strains induces cell death in response to bacteriophage T4 infection by cleaving translation elongation factor (EF) Tu and shutting down translation. Suicide of the cell is timed to the appearance late in the maturation of the phage of a short peptide sequence in the major head protein, the Gol peptide, which activates proteolysis. In the present work we demonstrate that the Gol peptide binds specifically to domains II and III of EF-Tu, creating the unique substrate for the Lit protease, which then cleaves domain I, the guanine nucleotide binding domain. The conformation of EF-Tu is important for binding and Lit cleavage, because both are sensitive to the identity of the bound nucleotide, with GDP being preferred over GTP. We propose that association of the T4 coat protein with EF-Tu plays a role in phage head assembly but that this association marks infected cells for suicide when Lit is present. Based on these data and recent observations on human immunodeficiency virus type 1 maturation, we speculate that associations between host translation factors and coat proteins may be integral to viral assembly in both prokaryotes and eukaryotes.

**Uzan, M. & Miller, E. S. Post-transcriptional control by bacteriophage T4: mRNA decay and inhibition of translation initiation. Virology Journal 7, 360 (2010).**
Over 50 years of biological research with bacteriophage T4 includes notable discoveries in post-transcriptional control, including the genetic code, mRNA, and tRNA; the very foundations of molecular biology. In this review we compile the past 10 - 15 year literature on RNA-protein interactions with T4 and some of its related phages, with particular focus on advances in mRNA decay and processing, and on translational repression. Binding of T4 proteins RegB, RegA, gp32 and gp43 to their cognate target RNAs has been characterized. For several of these, further study is needed for an atomic-level perspective, where resolved structures of RNA-protein complexes are awaiting investigation. Other features of post-transcriptional control are also summarized. These include: RNA structure at translation initiation regions that either inhibit or promote translation initiation; programmed translational bypassing, where T4 orchestrates ribosome bypass of a 50 nucleotide mRNA sequence; phage exclusion systems that involve T4-mediated activation of a latent endoribonuclease (PrrC) and cofactor-assisted activation of EF-Tu proteolysis (Gol-Lit); and potentially important findings on ADP-ribosylation (by Alt and Mod enzymes) of ribosome-associated proteins that might broadly impact protein synthesis in the infected cell. Many of these problems can continue to be addressed with T4, whereas the growing database of T4-related phage genome sequences provides new resources and potentially new phage-host systems to extend the work into a broader biological, evolutionary context.

**Yu, Y. T. & Snyder, L. Translation elongation factor Tu cleaved by a phage-exclusion system. Proc Natl Acad Sci U S A 91, 802-806 (1994).**
Bacteriophage T4 multiples poorly in Escherichia coli strains carrying the defective prophage, e14; the e14 prophage contains the lit gene for late inhibitor of T4 in E. coli. The exclusion is caused by the interaction of the e14-encoded protein, Lit, with a short RNA or polypeptide sequence encoded by gol from within the major head protein gene of T4. The interaction between Lit and the gol product causes a severe inhibition of all translation and prevents the transcription of genes downstream of the gol site in the same transcription unit. However, it does not inhibit most transcription, nor does it inhibit replication or affect intracellular levels of ATP. Here we show that the interaction of gol with Lit causes the cleavage of translation elongation factor Tu (EF-Tu) in a region highly conserved from bacteria to humans. The depletion of EF-Tu is at least partly responsible for the inhibition of translation and the phage exclusion. The only other phage-exclusion system to be understood in any detail also attacks a highly conserved cellular component, suggesting that phage-exclusion systems may yield important reagents for studying cellular processes.


# SspBCDE

## Example of genomic structure

The SspBCDE system is composed of 7 proteins: SspB, SspC, SspD, SspE, SspH, SspG and, SspF.

Here is an example found in the RefSeq database: 

<img src="./data/SspBCDE.svg">

SspBCDE system in the genome of *Bordetella hinzii* (GCF\_006770405.1) is composed of 7 proteins: SspF (WP\_221886990.1), SspG (WP\_142096192.1), SspH (WP\_142096195.1), SspE (WP\_142096198.1), SspD (WP\_142096201.1), SspC (WP\_142096204.1)and, SspB (WP\_142096207.1).

## Distribution of the system among prokaryotes

The SspBCDE system is present in a total of 276 different species.

Among the 22k complete genomes of RefSeq, this system is present in 579 genomes (2.5 %).

<img src="./data/Distribution_SspBCDE.svg" width=800px>

*Proportion of genome encoding the SspBCDE system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation

SspBCDE systems were experimentally validated using:

Subsystem SspABCD+SspE with a system from *Vibrio cyclitrophicus* in *Escherichia coli*  has an anti-phage effect against  T4, T1, JMPW1, JMPW2, EEP, T7 (Xiong et al., 2020) 

Subsystem SspBCD+SspE with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T4, T1, JMPW1, JMPW2, EEP, T7, PhiX174 (Xiong et al., 2020)

Subsystem SspBCD+SspE with a system from *Streptomyces yokosukanensis* in *Streptomyces lividans*  has an anti-phage effect against  JXY1 (Xiong et al., 2020)

Subsystem SspBCD+SspFGH with a system from *Vibrio anguillarum* in *Escherichia coli*  has an anti-phage effect against  T1, JMPW2, T4, EEP (Wang et al., 2021)

## Relevant abstracts

**Wang, S. et al. SspABCD-SspFGH Constitutes a New Type of DNA Phosphorothioate-Based Bacterial Defense System. mBio 12, e00613-21 (2021).**
Unlike nucleobase modifications in canonical restriction-modification systems, DNA phosphorothioate (PT) epigenetic modification occurs in the DNA sugar-phosphate backbone when the nonbridging oxygen is replaced by sulfur in a double-stranded (ds) or single-stranded (ss) manner governed by DndABCDE or SspABCD, respectively. SspABCD coupled with SspE constitutes a defense barrier in which SspE depends on sequence-specific PT modifications to exert its antiphage activity. Here, we identified a new type of ssDNA PT-based SspABCD-SspFGH defense system capable of providing protection against phages through a mode of action different from that of SspABCD-SspE. We provide further evidence that SspFGH damages non-PT-modified DNA and exerts antiphage activity by suppressing phage DNA replication. Despite their different defense mechanisms, SspFGH and SspE are compatible and pair simultaneously with one SspABCD module, greatly enhancing the protection against phages. Together with the observation that the sspBCD-sspFGH cassette is widely distributed in bacterial genomes, this study highlights the diversity of PT-based defense barriers and expands our knowledge of the arsenal of phage defense mechanisms.IMPORTANCE We recently found that SspABCD, catalyzing single-stranded (ss) DNA phosphorothioate (PT) modification, coupled with SspE provides protection against phage infection. SspE performs both PT-simulated NTPase and DNA-nicking nuclease activities to damage phage DNA, rendering SspA-E a PT-sensing defense system. To our surprise, ssDNA PT modification can also pair with a newly identified 3-gene sspFGH cassette to fend off phage infection with a different mode of action from that of SspE. Interestingly, both SspFGH and SspE can pair with the same SspABCD module for antiphage defense, and their combination provides Escherichia coli JM109 with additive phage resistance up to 105-fold compared to that for either barrier alone. This agrees with our observation that SspFGH and SspE coexist in 36 bacterial genomes, highlighting the diversity of the gene contents and molecular mechanisms of PT-based defense systems.

**Wang, S. et al. SspABCD-SspFGH Constitutes a New Type of DNA Phosphorothioate-Based Bacterial Defense System. mBio 12, e00613-21 (2021).**
Unlike nucleobase modifications in canonical restriction-modification systems, DNA phosphorothioate (PT) epigenetic modification occurs in the DNA sugar-phosphate backbone when the nonbridging oxygen is replaced by sulfur in a double-stranded (ds) or single-stranded (ss) manner governed by DndABCDE or SspABCD, respectively. SspABCD coupled with SspE constitutes a defense barrier in which SspE depends on sequence-specific PT modifications to exert its antiphage activity. Here, we identified a new type of ssDNA PT-based SspABCD-SspFGH defense system capable of providing protection against phages through a mode of action different from that of SspABCD-SspE. We provide further evidence that SspFGH damages non-PT-modified DNA and exerts antiphage activity by suppressing phage DNA replication. Despite their different defense mechanisms, SspFGH and SspE are compatible and pair simultaneously with one SspABCD module, greatly enhancing the protection against phages. Together with the observation that the sspBCD-sspFGH cassette is widely distributed in bacterial genomes, this study highlights the diversity of PT-based defense barriers and expands our knowledge of the arsenal of phage defense mechanisms.IMPORTANCE We recently found that SspABCD, catalyzing single-stranded (ss) DNA phosphorothioate (PT) modification, coupled with SspE provides protection against phage infection. SspE performs both PT-simulated NTPase and DNA-nicking nuclease activities to damage phage DNA, rendering SspA-E a PT-sensing defense system. To our surprise, ssDNA PT modification can also pair with a newly identified 3-gene sspFGH cassette to fend off phage infection with a different mode of action from that of SspE. Interestingly, both SspFGH and SspE can pair with the same SspABCD module for antiphage defense, and their combination provides Escherichia coli JM109 with additive phage resistance up to 105-fold compared to that for either barrier alone. This agrees with our observation that SspFGH and SspE coexist in 36 bacterial genomes, highlighting the diversity of the gene contents and molecular mechanisms of PT-based defense systems.


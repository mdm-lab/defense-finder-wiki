# RADAR

## Description

RADAR is comprised of two genes, encoding respectively for an adenosine triphosphatase (RdrA) and  a divergent adenosine deaminase (RdrB) (1), which are in some cases associated with a small membrane protein (RdrC or D) (1). 

RADAR was found to perform RNA editing of adenosine to inosine during phage infection. Editing sites are broadly distributed on the host transcriptome, which could prove deleterious to the host and explain the observed growth arrest of RADAR upon phage infection.  

## Molecular mechanism
RADAR mediates growth arrest upon infection and is therefore considered to be an Abortive infection system.

## Example of genomic structure

The RADAR system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/radar_I.svg">

radar\_I subsystem in the genome of *Dickeya dianthicola* (GCF\_014893095.1) is composed of 2 proteins: rdrB\_I (WP\_192988590.1)and, rdrA\_I (WP\_192988591.1).

<img src="./data/radar_II.svg">

radar\_II subsystem in the genome of *Klebsiella aerogenes* (GCF\_008693885.1) is composed of 3 proteins: rdrD\_II (WP\_015705078.1), rdrB\_II (WP\_015705077.1)and, rdrA\_II (WP\_015705076.1).

## Distribution of the system among prokaryotes

The RADAR system is present in a total of 39 different species.

Among the 22k complete genomes of RefSeq, this system is present in 135 genomes (0.6 %).

<img src="./data/Distribution_RADAR.svg" width=800px>

*Proportion of genome encoding the RADAR system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

RADAR systems were experimentally validated using:

A system from *Citrobacter rodentium* in *Escherichia coli* has an anti-phage effect against T2, T4, T5, T3, T6 (Gao et al., 2020; Duncan-Lowey et al., 2022)

A system from *Escherichia coli* in *Escherichia coli* has an anti-phage effect against T2, T4, T6 (Duncan-Lowey et al., 2022)

A system from *Streptococcus suis* in *Escherichia coli* has an anti-phage effect against T2, T4, T5, T6 (Duncan-Lowey et al., 2022)

## Relevant abstracts

**Gao, L. et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. Science 369, 1077-1084 (2020).**
Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.

## References

1.  Gao L, Altae-Tran H, Böhning F, et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. *Science*. 2020;369(6507):1077-1084. doi:10.1126/science.aba0372

# Zorya

## Example of genomic structure

The Zorya system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

<img src="./data/Zorya_TypeI.svg">

Zorya\_TypeI subsystem in the genome of *Pseudomonas aeruginosa* (GCF\_002085605.1) is composed of 4 proteins: ZorD (WP\_015649020.1), ZorC (WP\_015649021.1), ZorB (WP\_015649022.1)and, ZorA (WP\_025297974.1).

<img src="./data/Zorya_TypeII.svg">

Zorya\_TypeII subsystem in the genome of *Legionella longbeachae* (GCF\_011465255.1) is composed of 3 proteins: ZorA2 (WP\_050777601.1), ZorB (WP\_003632756.1)and, ZorE (WP\_050777600.1).

## Distribution of the system among prokaryotes

The Zorya system is present in a total of 304 different species.

Among the 22k complete genomes of RefSeq, this system is present in 840 genomes (3.7 %).

<img src="./data/Distribution_Zorya.svg" width=800px>

*Proportion of genome encoding the Zorya system for the 14 phyla with more than 50 genomes in the RefSeq database.*  *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Experimental validation

Zorya systems were experimentally validated using:

Subsystem Type I with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  LambdaVir, SECphi27, T7 (Doron et al., 2018)

Subsystem Type II with a system from *Escherichia coli* in *Escherichia coli*  has an anti-phage effect against  T7, SECphi17 (Doron et al., 2018)

Subsystem Type III with a system from *Stenotrophomonas nitritireducens* in *Escherichia coli*  has an anti-phage effect against  T1, T4, T7, LambdaVir, PVP-SE1 (Payne et al., 2021)

## Relevant abstracts

**Doron, S. et al. Systematic discovery of antiphage defense systems in the microbial pangenome. Science 359, eaar4120 (2018).**
The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.

**Payne, L. J. et al. Identification and classification of antiviral defence systems in bacteria and archaea with PADLOC reveals new system types. Nucleic Acids Research 49, 10868-10878 (2021).**
To provide protection against viral infection and limit the uptake of mobile genetic elements, bacteria and archaea have evolved many diverse defence systems. The discovery and application of CRISPR-Cas adaptive immune systems has spurred recent interest in the identification and classification of new types of defence systems. Many new defence systems have recently been reported but there is a lack of accessible tools available to identify homologs of these systems in different genomes. Here, we report the Prokaryotic Antiviral Defence LOCator (PADLOC), a flexible and scalable open-source tool for defence system identification. With PADLOC, defence system genes are identified using HMM-based homologue searches, followed by validation of system completeness using gene presence/absence and synteny criteria specified by customisable system classifications. We show that PADLOC identifies defence systems with high accuracy and sensitivity. Our modular approach to organising the HMMs and system classifications allows additional defence systems to be easily integrated into the PADLOC database. To demonstrate application of PADLOC to biological questions, we used PADLOC to identify six new subtypes of known defence systems and a putative novel defence system comprised of a helicase, methylase and ATPase. PADLOC is available as a standalone package (https://github.com/padlocbio/padloc) and as a webserver (https://padloc.otago.ac.nz).

